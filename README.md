# Quasi-static phase-field fracture of laminated glass

This git repository provides support for article *On quasi-static bending of laminated glass andphase-field modelling of glass fracture*.