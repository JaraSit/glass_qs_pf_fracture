import formulations as f
import solver as s
import fenics as fe
import numpy as np
import matplotlib.pyplot as plt


# --------------------
# Functions and classes
# --------------------
def left_point(x):
	return fe.near(x[0], l_off)


def right_point(x):
	return fe.near(x[0], l_x - l_off)


def left_load_point(x):
	return fe.near(x[0], l_x / 2 - l_0 / 2)


def right_load_point(x):
	return fe.near(x[0], l_x / 2 + l_0 / 2)


def symmetry_axis(x, on_boundary):
	return fe.near(x[0], l_x/2) and on_boundary


def init_bc(model):
	model.init_spaces()
	V = model.V

	u_d = fe.Expression("-t/1000.0", t=0.0, degree=0)  # Prescribed displacement
	BC_u_1 = fe.DirichletBC(V.sub(1), 0.0, left_point, method="pointwise")
	BC_u_2 = fe.DirichletBC(V.sub(1), u_d, left_load_point, method="pointwise")
	BC_u_3 = fe.DirichletBC(V.sub(0), 0.0, symmetry_axis)
	BC_u_4 = fe.DirichletBC(V.sub(2), 0.0, symmetry_axis)
	BC_u = [BC_u_1, BC_u_2, BC_u_3, BC_u_4]
	BC_d = []

	model.set_bc(BC_u, BC_d, u_d)


def solve(file, model):
	solution = f.PostProcess(file)
	ss = s.StaggeredScheme(model, time_space, solution)

	mon1 = f.Monitor(0.5*l_x, 0.0)
	ss.set_monitor(mon1)

	ss.set_d_init(d_init)
	ss.solve()

	mon1.plot_monitor()
	solution.save_monitor(mon1)


def calc_uniforms():
	model_pham = f.BeamDamageModel(glass_p, cross, mesh_unif, "el", "pham", hs)
	model_bourdin = f.BeamDamageModel(glass_b, cross, mesh_unif, "el", "bourdin", hs)
	model_stress = f.BeamDamageModel(glass_p, cross, mesh_unif, "el", "stress", hs)

	init_bc(model_pham)
	init_bc(model_bourdin)
	init_bc(model_stress)

	solve("Solution_pham_beam_Unif", model_pham)
	solve("Solution_bourdin_beam_Unif", model_bourdin)
	solve("Solution_stress_beam_Unif", model_stress)


def calc_refined():
	model_pham = f.BeamDamageModel(glass_p_ref, cross, mesh_ref, "el", "pham", hs)
	model_bourdin = f.BeamDamageModel(glass_b_ref, cross, mesh_ref, "el", "bourdin", hs)
	model_stress = f.BeamDamageModel(glass_p_ref, cross, mesh_ref, "el", "stress", hs)

	init_bc(model_pham)
	init_bc(model_bourdin)
	init_bc(model_stress)

	#solve("Solution_pham_beam_Ref_anal", model_pham)
	solve("Solution_bourdin_beam_Ref_anal_imp", model_bourdin)
	#solve("Solution_stress_beam_Ref", model_stress)


def calc_imper():
	model_pham = f.BeamDamageModel(glass_p_ref, cross, mesh_ref, "el", "pham", hs)
	init_bc(model_pham)
	solve("Solution_pham_beam_Ref_imper", model_pham)


# Material parameters
E = 76.6e9  # Young's modulus
#E = fe.Expression("76.6e9 - 0.01*near(x[0], 0.5, 0.0001)", degree=1)
nu = 0.22  # Poisson ratio
lc = 0.005
lc_ref = 0.0005
ft = 60.0e6

d_init = fe.Expression("0.0 + 0.001*near(x[0], 0.5, 0.0001)", degree=1)

Gc_b = lc * 256.0 / 27.0 * ft ** 2 / E
Gc_p = lc * 8.0 / 3.0 * ft ** 2 / E

Gc_b_ref = lc_ref * 256.0 / 27.0 * ft ** 2 / E
Gc_p_ref = lc_ref * 8.0 / 3.0 * ft ** 2 / E

glass_b = f.ElasticMaterial(E, nu, Gc_b, lc, ft)
glass_p = f.ElasticMaterial(E, nu, Gc_p, lc, ft)

glass_b_ref = f.ElasticMaterial(E, nu, Gc_b_ref, lc_ref, ft)
glass_p_ref = f.ElasticMaterial(E, nu, Gc_p_ref, lc_ref, ft)

# Geometry parameters
l_x, l_y = 1.1, 0.02  # Length and thickness of glass beam
n_x, n_y = 275, 10  # Number of elements for uniform mesh
l_off = 0.05  # Support offset
l_0 = 0.2  # Pitch of load points

b = 1.0
h = 0.02

cross = f.CrossSection(b, h)

n_ni = 20
hs = np.linspace(-0.5*h, 0.5*h, n_ni)

# Time parameters - TODO: change time space
t_start = 0.1
t_end = 25.0
dt_1 = 0.1
dt_2 = 0.1
#t_steps_1 = int((t_middle-t_start)/dt_1)
#t_steps_2 = int((t_end-t_middle)/dt_2)
t_steps = int((t_end-t_start)/dt_2)

#time_space_1 = np.linspace(t_start, t_middle, t_steps_1)
#time_space_2 = np.linspace(t_middle + dt_1, t_end, t_steps_2)

#time_space = np.concatenate((time_space_1, time_space_2))
time_space = np.linspace(t_start, t_end, t_steps)

# --------------------
# Define geometry
# --------------------
mesh_unif = fe.Mesh("Meshes/QS_4PB_1D.xml")
mesh_ref = fe.Mesh("Meshes/QS_4PB_1D_ref.xml")

hmin = mesh_unif.hmin()
hmin_ref = mesh_ref.hmin()
print(hmin, hmin_ref)

fe.plot(mesh_ref, color="black")
plt.show()

#calc_uniforms()
calc_refined()
#calc_imper()





# Create model with pham/bourdin/stress formulation with material glass_p
# 3rd argument: type of displacement equation
# 4th argument: type of damage model
# 5th argument: type of decomposition for damage calculation
#model_pham = f.PSDamageModel(glass_p, mesh, "el", "pham", "vd")
#model_bourdin = f.PSDamageModel(glass_p, mesh, "el", "bourdin", "vd")
#model_stress = f.PSDamageModel(glass_p, mesh, "el", "stress", "vd")

# Init spaces - for BC definition
#model_pham.init_spaces()
#V = model_pham.V

# --------------------
# Boundary conditions
# --------------------
#u_d = fe.Expression("-t/1000.0", t=0.0, degree=0)  # Prescribed displacement
#BC_u_1 = fe.DirichletBC(V.sub(1), 0.0, left_point, method="pointwise")
#BC_u_2 = fe.DirichletBC(V.sub(1), u_d, left_load_point, method="pointwise")
#BC_u_3 = fe.DirichletBC(V.sub(0), 0.0, symmetry_axis)
#BC_u = [BC_u_1, BC_u_2, BC_u_3]
#BC_d = []
#model_pham.set_bc(BC_u, BC_d, u_d)

# 1st argument of PostProcess object: Folder for solution
#solution_1 = f.PostProcess("Solution_pham_PS_Ref")
#ss = s.StaggeredScheme(model_pham, time_space, solution_1)
#solution_2 = f.PostProcess("Solution_bourdin_PS_Ref")
#ss = s.StaggeredScheme(model_pham, time_space, solution_2)

# One-point monitoring of sigma_x
#mon1 = f.Monitor(l_x/2, 0.0)
#ss.set_monitor(mon1)

# Staggered solver
#ss.solve()

# Plotting of monitor data
#mon1.plot_monitor()
#solution.save_monitor(mon1)
