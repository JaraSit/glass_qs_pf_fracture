import numpy as np
import matplotlib.pyplot as plt


def plot_ps(data, pham):
    plt.plot(data[:, 0], data[:, 3], label="pham")


pham_unif_ps = np.loadtxt("../Solution_pham_PS_Unif/stress_data.txt")
#bourdin_unif_ps = np.loadtxt("Solution_bourdin_PS_Unif")
#stress_unif_ps = np.loadtxt("Solution_stress_PS_Unif")

plot_ps(pham_unif_ps)
plt.show()