import formulations as f
import solver as s
import fenics as fe
import numpy as np
import matplotlib.pyplot as plt


# --------------------
# Functions and classes
# --------------------
def left_point(x):
	return fe.near(x[0], l_off)


def right_point(x):
	return fe.near(x[0], l_x - l_off)


def left_load_point(x):
	return fe.near(x[0], l_x / 2 - l_0 / 2)


def right_load_point(x):
	return fe.near(x[0], l_x / 2 + l_0 / 2)


def symmetry_axis(x, on_boundary):
	return fe.near(x[0], l_x/2) and on_boundary


def corner(x):
	return fe.near(x[0], l_off) and fe.near(x[1], 0.0)


def prepare_mesh():
	nx = 11
	ny = 10

	l_sym = 0.5 * l_x
	mesh = fe.RectangleMesh(fe.Point(0., 0.), fe.Point(0.5*l_x, l_y), nx, ny, diagonal="left/right")

	# fe.plot(mesh, "Mesh")
	# plt.show()

	# Refine
	markers = fe.MeshFunction("bool", mesh, 2)
	markers.set_all(False)
	for c in fe.cells(mesh):
		# Mark cells with facet midpoints near x == L/2
		for f in fe.facets(c):
			if fe.near(f.midpoint()[0], l_x/2., 0.13):
				markers[c] = True
	mesh = fe.refine(mesh, markers, redistribute=False)

	# fe.plot(mesh, "Mesh")
	# plt.show()

	# Refine again
	markers = fe.MeshFunction("bool", mesh, 2)
	markers.set_all(False)
	for c in fe.cells(mesh):
		# Mark cells with facet midpoints near x == L/2
		for f in fe.facets(c):
			if fe.near(f.midpoint()[0], l_x / 2., 0.13):
				markers[c] = True
	mesh = fe.refine(mesh, markers, redistribute=False)

	# Refine again
	markers = fe.MeshFunction("bool", mesh, 2)
	markers.set_all(False)
	for c in fe.cells(mesh):
		# Mark cells with facet midpoints near x == L/2
		for f in fe.facets(c):
			if fe.near(f.midpoint()[0], l_x / 2. - 0.1, 0.05):
				markers[c] = True
	mesh = fe.refine(mesh, markers, redistribute=False)

	# Refine again
	markers = fe.MeshFunction("bool", mesh, 2)
	markers.set_all(False)
	for c in fe.cells(mesh):
		# Mark cells with facet midpoints near x == L/2
		for f in fe.facets(c):
			if fe.near(f.midpoint()[0], l_x / 2. - 0.1, 0.05):
				markers[c] = True
	mesh = fe.refine(mesh, markers, redistribute=False)

	return mesh


def init_bc(model):
	model.init_spaces()
	V = model.V

	u_d = fe.Expression("-t/1000.0", t=0.0, degree=0)  # Prescribed displacement
	BC_u_1 = fe.DirichletBC(V.sub(1), 0.0, left_point)
	BC_u_1a = fe.DirichletBC(V.sub(1), 0.0, right_point)
	BC_u_1b = fe.DirichletBC(V.sub(0), (0.0, 0.0), right_point)
	BC_u_2 = fe.DirichletBC(V.sub(1), u_d, left_load_point)
	BC_u_2a = fe.DirichletBC(V.sub(1), u_d, right_load_point)
	BC_u_3 = fe.DirichletBC(V.sub(0).sub(0), 0.0, symmetry_axis)
	BC_u_3a = fe.DirichletBC(V.sub(0).sub(1), 0.0, corner, method="pointwise")
	BC_u_4 = fe.DirichletBC(V.sub(2).sub(1), 0.0, symmetry_axis)
	BC_u = [BC_u_1, BC_u_2, BC_u_3, BC_u_3a, BC_u_4]
	BC_d = []

	model.set_bc(BC_u, BC_d, u_d)


def solve(file, model):
	solution = f.PostProcess(file)
	ss = s.StaggeredScheme(model, time_space, solution)

	mon1 = f.Monitor(0.5*l_x, 0.5*l_y)
	ss.set_monitor(mon1)

	ss.solve()

	#mon1.plot_monitor()
	solution.save_monitor(mon1)


def calc_uniforms():
	model_pham = f.BeamDamageModel(glass_p, cross, mesh_unif, "el", "pham", hs)
	model_bourdin = f.BeamDamageModel(glass_b, cross, mesh_unif, "el", "bourdin", hs)
	model_stress = f.BeamDamageModel(glass_p, cross, mesh_unif, "el", "stress", hs)

	init_bc(model_pham)
	init_bc(model_bourdin)
	init_bc(model_stress)

	solve("Solution_pham_plate_Unif", model_pham)
	solve("Solution_bourdin_plate_Unif", model_bourdin)
	solve("Solution_stress_plate_Unif", model_stress)


def calc_refined():
	model_pham = f.PlateDamageModel(glass_p_ref, cross, mesh_ref, "el", "pham", hs)
	model_bourdin = f.PlateDamageModel(glass_b_ref, cross, mesh_ref, "el", "bourdin", hs)
	model_stress = f.PlateDamageModel(glass_p_ref, cross, mesh_ref, "el", "stress", hs)

	init_bc(model_pham)
	init_bc(model_bourdin)
	init_bc(model_stress)

	solve("Solution_pham_plate_Ref_3", model_pham)
	#solve("Solution_bourdin_plate_Ref", model_bourdin)
	#solve("Solution_stress_plate_Ref", model_stress)


# Material parameters
E = 76.6e9  # Young's modulus
nu = 0.22  # Poisson ratio
lc = 0.02
lc_ref = 0.006
#ft = 60.0e6
ft = 60.0e6

Gc_b = lc * 256.0 / 27.0 * ft ** 2 / E
Gc_p = lc * 8.0 / 3.0 * ft ** 2 / E

Gc_b_ref = lc_ref * 256.0 / 27.0 * ft ** 2 / E
Gc_p_ref = lc_ref * 8.0 / 3.0 * ft ** 2 / E

glass_b = f.ElasticMaterial(E, nu, Gc_b, lc, ft)
glass_p = f.ElasticMaterial(E, nu, Gc_p, lc, ft)

glass_b_ref = f.ElasticMaterial(E, nu, Gc_b_ref, lc_ref, ft)
glass_p_ref = f.ElasticMaterial(E, nu, Gc_p_ref, lc_ref, ft, k_res=1.0e-3)

# Geometry parameters
l_x, l_y = 1.1, 0.36  # Length and thickness of glass beam
n_x, n_y = 275, 10  # Number of elements for uniform mesh
l_off = 0.05  # Support offset
l_0 = 0.2  # Pitch of load points

b = 1.0
h = 0.02

cross = f.CrossSection(b, h)

n_ni = 20
hs = np.linspace(-0.5*h, 0.5*h, n_ni)

# Time parameters - TODO: change time space
t_start = 0.1
t_middle = 6.0
t_end = 7.0
dt_1 = 0.5
dt_2 = 0.02
t_steps_1 = int((t_middle-t_start)/dt_1)
t_steps_2 = int((t_end-t_middle)/dt_2)
#t_steps = int((t_end-t_start)/dt_2)

time_space_1 = np.linspace(t_start, t_middle, t_steps_1)
time_space_2 = np.linspace(t_middle + dt_1, t_end, t_steps_2)

time_space = np.concatenate((time_space_1, time_space_2))
#time_space = np.linspace(t_start, t_end, t_steps)

# --------------------
# Define geometry
# --------------------
#mesh_unif = fe.Mesh("Meshes/QS_4PB_1D.xml")
#mesh_ref = fe.Mesh("Meshes/QS_4PB_1D_ref.xml")
mesh_ref = prepare_mesh()

#hmin = mesh_unif.hmin()
hmin_ref = mesh_ref.hmin()
print(hmin_ref)

fe.plot(mesh_ref)
plt.show()

#calc_uniforms()
calc_refined()





# Create model with pham/bourdin/stress formulation with material glass_p
# 3rd argument: type of displacement equation
# 4th argument: type of damage model
# 5th argument: type of decomposition for damage calculation
#model_pham = f.PSDamageModel(glass_p, mesh, "el", "pham", "vd")
#model_bourdin = f.PSDamageModel(glass_p, mesh, "el", "bourdin", "vd")
#model_stress = f.PSDamageModel(glass_p, mesh, "el", "stress", "vd")

# Init spaces - for BC definition
#model_pham.init_spaces()
#V = model_pham.V

# --------------------
# Boundary conditions
# --------------------
#u_d = fe.Expression("-t/1000.0", t=0.0, degree=0)  # Prescribed displacement
#BC_u_1 = fe.DirichletBC(V.sub(1), 0.0, left_point, method="pointwise")
#BC_u_2 = fe.DirichletBC(V.sub(1), u_d, left_load_point, method="pointwise")
#BC_u_3 = fe.DirichletBC(V.sub(0), 0.0, symmetry_axis)
#BC_u = [BC_u_1, BC_u_2, BC_u_3]
#BC_d = []
#model_pham.set_bc(BC_u, BC_d, u_d)

# 1st argument of PostProcess object: Folder for solution
#solution_1 = f.PostProcess("Solution_pham_PS_Ref")
#ss = s.StaggeredScheme(model_pham, time_space, solution_1)
#solution_2 = f.PostProcess("Solution_bourdin_PS_Ref")
#ss = s.StaggeredScheme(model_pham, time_space, solution_2)

# One-point monitoring of sigma_x
#mon1 = f.Monitor(l_x/2, 0.0)
#ss.set_monitor(mon1)

# Staggered solver
#ss.solve()

# Plotting of monitor data
#mon1.plot_monitor()
#solution.save_monitor(mon1)
