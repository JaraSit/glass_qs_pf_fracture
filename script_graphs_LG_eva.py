import numpy as np
import matplotlib.pyplot as plt
import fenics as fe
import dolfin as d
import mshr


def plot_ps(data, label):
    plt.plot(abs(-data[:, 0])*1.0e3, data[:, 1]*70.0e3, label=label)


# Experimentalni data
# 0-time, 1-disp1, 2-disp2, 3-force, 4-sigma1, 5-sigma2, 6-sigma3, 7-sigma4, 8-sigma5, 9-sigma6, 10-sigma7, 11-sigma8
# 2 - Tazena strana uprostred
# 5 - Tlacena strana uprostred
exp_01_1 = np.loadtxt("exps/VSG_EVA_01-1.txt")
exp_01_2 = np.loadtxt("exps/VSG_EVA_01-2.txt")
exp_02_1 = np.loadtxt("exps/VSG_EVA_02-1.txt")
exp_02_2 = np.loadtxt("exps/VSG_EVA_02-2.txt")
exp_02_3 = np.loadtxt("exps/VSG_EVA_02-3.txt")
exp_03_1 = np.loadtxt("exps/VSG_EVA_03-1.txt")
exp_03_2 = np.loadtxt("exps/VSG_EVA_03-2.txt")
exp_04_1 = np.loadtxt("exps/VSG_EVA_04-1.txt")
exp_05_1 = np.loadtxt("exps/VSG_EVA_05-1.txt")
exp_05_2 = np.loadtxt("exps/VSG_EVA_05-2.txt")

# Data z numeriky
PS_stress_min = np.loadtxt("Solutions/Solution_LG3_PS_el_pham_vd_eva_hyb_min/stress_data.txt")
PS_stress_max = np.loadtxt("Solutions/Solution_LG3_PS_el_pham_vd_eva_hyb_max/stress_data.txt")
PS_force_min = np.loadtxt("Solutions/Solution_LG3_PS_el_pham_vd_eva_hyb_min/reaction_data.txt")
PS_force_max = np.loadtxt("Solutions/Solution_LG3_PS_el_pham_vd_eva_hyb_max/reaction_data.txt")
B_stress_min = np.loadtxt("Solutions/Solution_LG3_B_eva_min/stress_data.txt")
B_stress_max = np.loadtxt("Solutions/Solution_LG3_B_eva_max/stress_data.txt")
B_force_min = np.loadtxt("Solutions/Solution_LG3_B_eva_min/reaction_data.txt")
B_force_max = np.loadtxt("Solutions/Solution_LG3_B_eva_max/reaction_data.txt")
P_stress_min = np.loadtxt("Solutions/Solution_LG3_P_eva_min/stress_data.txt")
P_stress_max = np.loadtxt("Solutions/Solution_LG3_P_eva_max/stress_data.txt")
P_force_min = np.loadtxt("Solutions/Solution_LG3_P_eva_min/reaction_data.txt")
P_force_max = np.loadtxt("Solutions/Solution_LG3_P_eva_max/reaction_data.txt")

# Dalsi parametry
b = 0.36

# Plot 1 - Reaction
plt.plot(abs(PS_stress_max[:, 0])*1.0e3, -PS_stress_max[:, 3]/1.0e6, "-", label="PS-lower")
plt.plot(abs(PS_stress_max[:, 0])*1.0e3, PS_stress_max[:, 7]/1.0e6, "-", label="PS-upper")
plt.xlabel("Prescribed displacement [mm]")
plt.ylabel("Reaction [kN]")
plt.legend()
plt.show()

plt.plot(exp_01_1[:, 1], exp_01_1[:, 5], "-", color="lightgray")
plt.plot(exp_01_1[:, 1], -exp_01_1[:, 8], "-", color="lightgray")
plt.xlabel("Prescribed displacement [mm]")
plt.ylabel("Reaction [kN]")
plt.legend()
plt.show()

# Plot 1 - Reaction
plt.plot(exp_01_1[:, 1], exp_01_1[:, 3], "-", color="lightgray")
plt.plot(exp_02_2[:, 1], exp_02_2[:, 3], "-", color="lightgray")
plt.plot(exp_03_1[:, 1], exp_03_1[:, 3], "-", color="lightgray")
plt.plot(exp_04_1[:, 1], exp_04_1[:, 3], "-", color="lightgray")
plt.plot(exp_05_1[:, 1], exp_05_1[:, 3], "-", color="lightgray", label="Experiments")
plt.plot(abs(PS_force_min[:, 0])*1.0e3, 2*b*PS_force_min[:, 1]/1.0e3, "-", label="PS-lower")
plt.plot(abs(PS_force_max[:, 0])*1.0e3, 2*b*PS_force_max[:, 1]/1.0e3, "-", label="PS-upper")
plt.plot(abs(B_force_min[:, 0])*1.0e3, 2*B_force_min[:, 1]/1.0e3, "-", label="B-lower")
plt.plot(abs(B_force_max[:, 0])*1.0e3, 2*B_force_max[:, 1]/1.0e3, "-", label="B-upper")
plt.plot(abs(P_force_min[:, 0])*1.0e3, 2*2*P_force_min[:, 1]/1.0e3, "-", label="P-lower")
plt.xlabel("Prescribed displacement [mm]")
plt.ylabel("Reaction [kN]")
plt.legend()
plt.show()

# Plot 2 - Stress, tension
plt.plot(exp_01_1[:, 1], exp_01_1[:, 5], "-", color="lightgray")
plt.plot(exp_02_2[:, 1], exp_02_2[:, 5], "-", color="lightgray")
plt.plot(exp_03_1[:, 1], exp_03_1[:, 5], "-", color="lightgray")
plt.plot(exp_04_1[:, 1], exp_04_1[:, 5], "-", color="lightgray", label="Experiments")
#plt.plot(exp_05_1[:, 1], exp_05_1[:, 5], "-", color="lightgray")
plt.plot(abs(PS_stress_min[:, 0])*1.0e3, PS_stress_min[:, 3]/1.0e6, "-", label="PS-lower")
plt.plot(abs(PS_stress_max[:, 0])*1.0e3, PS_stress_max[:, 3]/1.0e6, "-", label="PS-upper")
plt.plot(abs(B_stress_min[:, 0])*1.0e3, B_stress_min[:, 3]/1.0e6, "-", label="B-lower")
plt.plot(abs(B_stress_max[:, 0])*1.0e3, B_stress_max[:, 3]/1.0e6, "-", label="B-upper")
plt.plot(abs(P_stress_min[:, 0])*1.0e3, P_stress_min[:, 3]/1.0e6, "-", label="P-lower")
plt.xlabel("Prescribed displacement [mm]")
plt.ylabel("Mid-point stress [MPa]")
plt.legend()
plt.show()

# Plot 3 - Stress, compression
plt.plot(exp_01_1[:, 1], exp_01_1[:, 8], "-", color="lightgray")
plt.plot(exp_02_2[:, 1], exp_02_2[:, 8], "-", color="lightgray")
plt.plot(exp_03_1[:, 1], exp_03_1[:, 8], "-", color="lightgray")
plt.plot(exp_04_1[:, 1], exp_04_1[:, 8], "-", color="lightgray")
plt.plot(exp_05_1[:, 1], exp_05_1[:, 8], "-", color="lightgray", label="Experiments")
plt.plot(abs(PS_stress_min[:, 0])*1.0e3, PS_stress_min[:, 7]/1.0e6, "-", label="PS-lower")
plt.plot(abs(PS_stress_max[:, 0])*1.0e3, PS_stress_max[:, 7]/1.0e6, "-", label="PS-upper")
# plt.plot(abs(B_stress_min[:, 0])*1.0e3, B_stress_min[:, 7]/1.0e6, "-", label="B-lower")
# plt.plot(abs(B_stress_max[:, 0])*1.0e3, B_stress_max[:, 7]/1.0e6, "-", label="B-upper")
plt.xlabel("Prescribed displacement [mm]")
plt.ylabel("Mid-point stress [MPa]")
plt.legend()
plt.show()

# Comparison of models - reactions
plt.plot(abs(PS_force_max[:, 0])*1.0e3, 2*b*PS_force_max[:, 1]/1.0e3, "-", label="PS-upper")
plt.plot(abs(B_force_max[:, 0])*1.0e3, 2*B_force_max[:, 1]/1.0e3, "-", label="B-upper")
plt.plot(abs(P_force_max[:, 0])*1.0e3, 2*2*P_force_max[:, 1]/1.0e3, "-", label="P-upper")
plt.xlabel("Prescribed displacement [mm]")
plt.ylabel("Reaction [kN]")
plt.legend()
plt.show()

# Comparison of models
plt.plot(abs(PS_stress_max[:, 0])*1.0e3, PS_stress_max[:, 3]/1.0e6, "-", label="PS-upper")
plt.plot(abs(B_stress_max[:, 0])*1.0e3, B_stress_max[:, 3]/1.0e6, "-", label="B-upper")
plt.plot(abs(P_stress_max[:, 0])*1.0e3, P_stress_max[:, 3]/1.0e6, "-", label="P-upper")
plt.xlabel("Prescribed displacement [mm]")
plt.ylabel("Mid-point stress [MPa]")
plt.legend()
plt.show()

# data1 = np.loadtxt("Solution_pham_PS_Ref/reaction_data.txt")
# data2 = np.loadtxt("Solution_pham_PS_Ref2/reaction_data.txt")
# # Plot 1 - Stress, compression
# plt.plot(data1[:, 0], data1[:, 1], "-", color="lightgray", label="l=0.0015")
# plt.plot(data2[:, 0], data2[:, 1], "-", color="lightgray", label="l=0.0008")
# plt.xlabel("Prescribed displacement [mm]")
# plt.ylabel("Mid-point stress [MPa]")
# plt.legend()
# plt.show()
