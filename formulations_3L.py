import fenics as fe
import matplotlib.pyplot as plt
import numpy as np
import Functions as fu
import math


# --------------------------------------
# Layered Mindlin beam model with damage
# --------------------------------------
class BeamDamageModel:
	# PSDamageModel Constructor
	def __init__(self, material, cross, mesh, u_type, d_type, num_int, lays_num, glass_num):
		self.mat = material
		self.cross = cross
		self.mesh = mesh
		self.V = None
		self.V0 = None
		self.W = None
		self.bc_u = []
		self.bc_d = []
		self.u_d = None
		self.u_type = u_type  # Type of form for displacement calculation
		self.d_type = d_type  # Type of form for damage calculation
		self.num_int = num_int
		self.n_ni = len(num_int)
		self.dh = abs(num_int[1] - num_int[0])
		self.lays_num = lays_num
		self.glass_num = glass_num

	# Define spaces
	def init_spaces(self):
		p1 = fe.FiniteElement('P', fe.interval, 1)  # Space of linear polynomial functions
		elems_num = 5*self.lays_num - 2
		element = fe.MixedElement(elems_num*[p1])  # Element of mixed space
		self.V = fe.FunctionSpace(self.mesh, element)  # Mixed space
		self.V0 = fe.FunctionSpace(self.mesh, 'DG', 0)
		element_d = fe.MixedElement(self.glass_num*[p1])
		self.W = fe.FunctionSpace(self.mesh, element_d)
		#self.W = fe.FunctionSpace(self.mesh, 'P', 1)  # Space of linear polynomial functions

	# Set Dirichlet boundary conditions and parameter function u_d
	def set_bc(self, bc_u, bc_d, u_d):
		self.bc_u = bc_u
		self.bc_d = bc_d
		self.u_d = u_d

	# Update time-like parameter in u_d function
	def time_step_update(self, t):
		self.u_d.t = t

	# Test version of analytical u_form (without damage)
	def get_u_form_a(self, x, d):
		# Selective integration for shearlock reduction
		dx_shear = fe.dx(metadata={"quadrature_degree": 0})

		lg_trial = fe.TrialFunctions(self.V)
		lg_test = fe.TestFunctions(self.V)

		u_form = 0.0

		E = self.mat.get_Es()
		G = self.mat.get_Gs()
		h = self.mat.get_hs()
		b = self.cross.b
		A = [b * hi for hi in h]
		I = [1.0/12.0*b*hi**3 for hi in h]

		for i in range(self.lays_num):

			ind = 3*i
			ind_max = 3*self.lays_num

			u_tr, w_tr, phi_tr = lg_trial[ind], lg_trial[ind + 1], lg_trial[ind + 2]
			u_test, w_test, phi_test = lg_test[ind], lg_test[ind + 1], lg_test[ind + 2]

			u_form += u_test.dx(0)*E[i]*A[i]*u_tr.dx(0)*fe.dx
			u_form += phi_test.dx(0)*E[i]*I[i]*phi_tr.dx(0)*fe.dx

			u_form += (w_test.dx(0) + phi_test)*G[i]*A[i]*(w_tr.dx(0) + phi_tr)*dx_shear
			u_form -= fe.Constant(0.0)*w_test*fe.dx

			if i < (self.lays_num - 1):
				lmbd_1_tr, lmbd_2_tr = lg_trial[ind_max + 2*i], lg_trial[ind_max + 2*i + 1]
				lmbd_1_test, lmbd_2_test = lg_test[ind_max + 2*i], lg_test[ind_max + 2*i + 1]
				ind_next = 3*(i + 1)
				u_tr_next, w_tr_next, phi_tr_next = lg_trial[ind_next], lg_trial[ind_next + 1], lg_trial[ind_next + 2]
				u_test_next, w_test_next, phi_test_next = lg_test[ind_next], lg_test[ind_next + 1], lg_test[ind_next + 2]

				u_form += lmbd_1_tr*(u_test + 0.5*h[i]*phi_test - u_test_next + 0.5*h[i + 1]*phi_test_next)*fe.dx
				u_form += lmbd_1_test*(u_tr + 0.5*h[i]*phi_tr - u_tr_next + 0.5*h[i + 1]*phi_tr_next)*fe.dx
				u_form += lmbd_2_tr*(w_test - w_test_next)*fe.dx + lmbd_2_test*(w_tr - w_tr_next)*fe.dx

		return u_form, "lin"

	# u_form with numerical integration
	def get_u_form(self, x, d):
		# Selective integration for shearlock reduction
		dx_shear = fe.dx(metadata={"quadrature_degree": 0})

		lg_trial = fe.split(x)
		lg_test = fe.TestFunctions(self.V)

		u_form = 0.0

		E = self.mat.get_Es()
		G = self.mat.get_Gs()
		h = self.mat.get_hs()
		b = self.cross.b
		A = [b*hi for hi in h]
		I = [1.0/12.0*b*hi**3 for hi in h]

		glass_ind = 0

		for i in range(self.lays_num):

			ind = 3 * i
			ind_max = 3 * self.lays_num

			u_tr, w_tr, phi_tr = lg_trial[ind], lg_trial[ind + 1], lg_trial[ind + 2]
			u_test, w_test, phi_test = lg_test[ind], lg_test[ind + 1], lg_test[ind + 2]

			if i%2 == 0:
				hs = np.linspace(-0.5*h[i], 0.5*h[i], self.n_ni)
				dh = abs(hs[1] - hs[0])
				for j in range(len(hs) - 1):
					gauss = 0.5*(hs[j] + hs[j + 1])
					eps_z = u_tr.dx(0) + phi_tr.dx(0)*gauss
					d_eps_z = u_test.dx(0) + phi_test.dx(0)*gauss
					eps_z_p = fu.mc_bracket(eps_z)
					eps_z_n = -fu.mc_bracket(-eps_z)
					u_form += fe.inner((1.0-d[glass_ind])**2*E[i]*eps_z_p + E[i]*eps_z_n, d_eps_z)*dh*b*fe.dx
				glass_ind += 1
			else:
				u_form += u_test.dx(0)*E[i]*A[i]*u_tr.dx(0)*fe.dx
				u_form += phi_test.dx(0)*E[i]*I[i]*phi_tr.dx(0)*fe.dx

			u_form += (w_test.dx(0) + phi_test)*G[i]*A[i]*(w_tr.dx(0) + phi_tr)*dx_shear
			u_form -= fe.Constant(0.0)*w_test*fe.dx

			if i < (self.lays_num - 1):
				lmbd_1_tr, lmbd_2_tr = lg_trial[ind_max + 2*i], lg_trial[ind_max + 2*i + 1]
				lmbd_1_test, lmbd_2_test = lg_test[ind_max + 2*i], lg_test[ind_max + 2*i + 1]
				ind_next = 3*(i + 1)
				u_tr_next, w_tr_next, phi_tr_next = lg_trial[ind_next], lg_trial[ind_next + 1], lg_trial[ind_next + 2]
				u_test_next, w_test_next, phi_test_next = lg_test[ind_next], lg_test[ind_next + 1], lg_test[ind_next + 2]

				u_form += lmbd_1_tr*(u_test + 0.5*h[i]*phi_test - u_test_next + 0.5*h[i + 1]*phi_test_next)*fe.dx
				u_form += lmbd_1_test*(u_tr + 0.5*h[i]*phi_tr - u_tr_next + 0.5*h[i + 1]*phi_tr_next)*fe.dx
				u_form += lmbd_2_tr*(w_test - w_test_next)*fe.dx + lmbd_2_test*(w_tr - w_tr_next)*fe.dx

		return u_form, "nlin"

	# Return formulation for damage
	def get_d_form(self, u, d):
		d_i = fe.split(d)
		#d_tr = fe.TrialFunctions(self.W)
		d_test = fe.TestFunctions(self.W)
		gc = self.mat.glass_mat.Gc
		lc = self.mat.glass_mat.lc
		ft = self.mat.glass_mat.ft
		# TODO: make it better
		h = [self.mat.get_hs()[0], self.mat.get_hs()[2]]
		b = self.cross.b

		d_form = 0.0
		for i in range(self.glass_num):
			d_form += -2*self.get_energy_active(u, d, 2*i)*fe.inner(1.0 - d_i[i], d_test[i])*fe.dx
			d_form += b*h[i]*3.0/8.0*gc*(1.0/lc*d_test[i] + 2*lc*fe.inner(fe.grad(d_i[i]), fe.grad(d_test[i])))*fe.dx

		return d_form

	def get_energy_active(self, x, d, l_num):
		lg_fce = fe.split(x)
		u_, w_, phi_ = lg_fce[3*l_num], lg_fce[3*l_num + 1], lg_fce[3*l_num + 2]
		h = self.mat.get_hs()[l_num]
		E = self.mat.get_Es()[l_num]
		b = self.cross.b
		eps_i_1 = u_.dx(0) + phi_.dx(0)*0.5*h
		eps_i_2 = u_.dx(0) - phi_.dx(0)*0.5*h
		eps_max = fu.max_fce(eps_i_1, eps_i_2)
		en = 0.5*fu.mc_bracket(eps_max)**2*E*h*b
		return en

	def init_files(self, files):
		files_i = []
		for i in range(self.lays_num):
			files_i.append("displ_u_" + str(i) + ".xdmf")
			files_i.append("displ_w_" + str(i) + ".xdmf")
			files_i.append("displ_phi_" + str(i) + ".xdmf")
			files_i.append("damage_" + str(i) + ".xdmf")
		files.init_files(files_i)

	def save_u(self, files, x, t):
		lg_displ = x.split(deepcopy=True)
		for i in range(self.lays_num):
			ind = 3*i
			ind_file = 4*i
			files.save_to(lg_displ[ind], ind_file, t)
			files.save_to(lg_displ[ind + 1], ind_file + 1, t)
			files.save_to(lg_displ[ind + 2], ind_file + 2, t)

	def save_d(self, files, d, t):
		lg_d = d.split(deepcopy=True)
		for i in range(self.glass_num):
			ind_file = 3 + 4*i
			files.save_to(lg_d[i], ind_file, t)

	# TODO: make it better. Temporary!!
	def get_strain_and_stress(self, m_x, m_y, u_i, d_i):
		E = self.mat.get_Es()
		hs = self.mat.get_hs()
		lg_displ = u_i.split(deepcopy=True)
		u_ = lg_displ[0]
		phi_ = lg_displ[2]
		eps_i = u_.dx(0) - phi_.dx(0)*0.5*hs[0]
		strain = fu.local_project(eps_i, self.V0)
		stress = fu.local_project(E[0]*eps_i, self.V0)
		#stress = fu.local_project((1.0 - d_i)**2*E[0]*fu.mc_bracket(eps_i) - E[0]*fu.mc_bracket(-eps_i), self.V0)
		return strain(m_x), 0.0, stress(m_x), 0.0

	def get_d_max(self):
		# TODO: make it better
		return fe.interpolate(fe.Constant((1.0, 1.0)), self.W)
