import formulations as f
import formulations_3L as f3l
import solvers.solver as s
import fenics as fe
import numpy as np
import matplotlib.pyplot as plt


# --------------------
# Functions and classes
# --------------------
def left_point(x):
	return fe.near(x[0], l_off)


def left_load_point(x):
	return fe.near(x[0], l_x / 2 - l_0 / 2)


def symmetry_axis(x, on_boundary):
	return fe.near(x[0], l_x/2) and on_boundary


def init_bc(model):
	model.init_spaces()
	V = model.V

	u_d = fe.Expression("-t/1000.0", t=0.0, degree=0)  # Prescribed displacement
	BC_u_1 = fe.DirichletBC(V.sub(1), 0.0, left_point, method="pointwise")
	BC_u_2 = fe.DirichletBC(V.sub(7), u_d, left_load_point, method="pointwise")
	BC_u_3 = fe.DirichletBC(V.sub(0), 0.0, symmetry_axis)
	BC_u_4 = fe.DirichletBC(V.sub(2), 0.0, symmetry_axis)
	BC_u_7 = fe.DirichletBC(V.sub(6), 0.0, symmetry_axis)
	BC_u_8 = fe.DirichletBC(V.sub(8), 0.0, symmetry_axis)
	BC_u = [BC_u_1, BC_u_2, BC_u_3, BC_u_4, BC_u_7, BC_u_8]
	BC_d = []

	model.set_bc(BC_u, BC_d, u_d)


def solve(file, model, type, time_space_i):
	solution = f.PostProcess(file)
	if type == "displacement":
		ss = s.TimeDisplacementScheme(model, time_space_i, solution)
	elif type == "staggered":
		ss = s.StaggeredScheme(model, time_space_i, solution)
		ss.max_iter = max_iter
	else:
		ss = s.StaggeredScheme(model, time_space_i, solution)
		ss.max_iter = max_iter

	mon1 = f.Monitor(0.5*l_x, 0.0)
	ss.set_monitor([mon1])

	p = fe.Point(0.5*l_x - 0.5*l_0)
	index = find_dof(p, 7, model.V)
	mon2 = f.MonitorReaction(index)
	ss.set_monitor_react(mon2)

	ss.solve(False)

	mon1.plot_monitor()
	solution.save_monitor(mon1)
	solution.save_monitor_r(mon2)


def calc_LG():
	model_pham_min = f3l.BeamDamageModel(LG_mat_EVA_min, cross, mesh_ref, "el", "pham", hs, 3, 2)
	model_pham_max = f3l.BeamDamageModel(LG_mat_EVA_max, cross, mesh_ref, "el", "pham", hs, 3, 2)
	init_bc(model_pham_min)
	init_bc(model_pham_max)
	solve("Solutions/Solution_LG3_B_eva_min", model_pham_min, "staggered", time_space_min)
	solve("Solutions/Solution_LG3_B_eva_max", model_pham_max, "staggered", time_space_max)


def find_dof(p, d, V):
	found_dof = -1
	V_dofs = V.tabulate_dof_coordinates()
	V0_dofs = V.sub(d).dofmap().dofs()
	for i in range(0, len(V0_dofs)):
		v_x = V_dofs[V0_dofs[i]]
		if fe.near(v_x, p.x()):
			found_dof = V0_dofs[i]
	print("Found_dof = ", found_dof)
	return found_dof


def make_time_space(t_array, t_dens_array):
	t_space = np.array([])
	for i in range(len(t_array) - 1):
		t_i_end = t_array[i+1] - t_dens_array[i]
		t_i_steps = int((t_i_end - t_array[i])/t_dens_array[i])
		t_i_space = np.linspace(t_array[i], t_i_end, t_i_steps)
		t_space = np.concatenate((t_space, t_i_space))
	return t_space


# Material parameters
E_glass = 70.0e9  # Young's modulus
nu_glass = 0.22  # Poisson ratio
G_EVA = 1.0e3*np.array([6933.9, 3898.6, 2289.2, 1672.7, 761.6, 2401.0, 65.2, 248.0, 575.6, 56.3, 188.6, 445.1, 300.1, 401.6, 348.1, 111.6, 127.2, 137.8, 50.5, 322.9, 100.0, 199.9])
theta_EVA = np.array([1.0e-9, 1.0e-8, 1.0e-7, 1.0e-6, 1.0e-5, 1.0e-4, 1.0e-3, 1.0e-2, 1.0e-1, 1.0, 1.0e1, 1.0e2, 1.0e3, 1.0e4, 1.0e5, 1.0e6, 1.0e7, 1.0e8, 1.0e9, 1.0e10, 1.0e11, 1.0e12])
nu_eva = 0.49
G_inf_eva = 682.18e3
c1_eva = 339.102
c2_eva = 1185.816
T_ref = 20.0
T_act = 25.0

lc = 0.0005
ft_glass_min = 29.0e6
ft_glass_max = 85.0e6

Gc_min = lc * 8.0 / 3.0 * ft_glass_min ** 2 / E_glass
Gc_max = lc * 8.0 / 3.0 * ft_glass_max ** 2 / E_glass

glass_mat_min = f.ElasticMaterial(E_glass, nu_glass, Gc_min, lc, ft_glass_min)
glass_mat_max = f.ElasticMaterial(E_glass, nu_glass, Gc_max, lc, ft_glass_max)
EVA_foil_mat = f.ViscoElasticMaterial(G_EVA, theta_EVA, G_inf_eva, nu_eva, c1_eva, c2_eva, T_ref, T_act)

H1, H2, H3 = 0.00996, 0.00076, 0.00996

LG_mat_EVA_min = f.LG3LMaterial(glass_mat_min, EVA_foil_mat, [H1, H2, H3])
LG_mat_EVA_max = f.LG3LMaterial(glass_mat_max, EVA_foil_mat, [H1, H2, H3])

# Geometry parameters
l_x, l_y = 1.1, H1+H2+H3  # Length and thickness of glass beam
l_off = 0.05  # Support offset
l_0 = 0.2  # Pitch of load points

b = 0.36
h = 0.0

cross = f.CrossSection(b, h)

n_ni = 20
hs = np.linspace(-0.5*h, 0.5*h, n_ni)

# Numerical parameters
max_iter = 50

time_space_min = make_time_space([0.1, 4.5, 4.9], [0.5, 0.1])
time_space_max = make_time_space([0.1, 14.5], [0.5])

# --------------------
# Define geometry
# --------------------
mesh_ref = fe.Mesh("Meshes/QS_4PB_1D_ref.xml")

hmin_ref = mesh_ref.hmin()
print(hmin_ref)

fe.plot(mesh_ref, color="black")
plt.show()

calc_LG()
