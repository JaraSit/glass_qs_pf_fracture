# -------------------------
# 1D PF pre-tests
# -------------------------

# -------------------------
# Description:
# - Quasi-static regime
#
# Last edit: 13.11.2019
# -------------------------

import fenics as fe
import numpy as np
import matplotlib.pyplot as plt
import math


def solver(form):

	# --------------------
	# Functions and classes
	# --------------------
	def left_end(x):
		return fe.near(x[0], 0.0)

	def right_end(x):
		return fe.near(x[0], l)

	def middle_point(x):
		return fe.near(x[0], l / 2)

	# Macauley bracket
	def mc_bracket(v):
		return 0.5*(v+abs(v))

	# --------------------
	# Solvers
	# --------------------
	def solve_damage_pham(d_min):
		lower = d_min
		upper = fe.interpolate(fe.Constant(1.00), V)

		# Pham formulation
		E_ds = -E*A*fe.inner(fe.grad(u), fe.grad(u))*fe.inner(1.0 - d, d_test)*fe.dx
		E_ds += 3.0/8.0*Gc*(1.0/lc*d_test + 2*lc*fe.inner(fe.grad(d), fe.grad(d_test)))*fe.dx

		# Solution of damage formulation
		H = fe.derivative(E_ds, d, fe.TrialFunction(V))

		snes_solver_parameters = {"nonlinear_solver": "snes",
								  "snes_solver": {"linear_solver": "lu",
												  "relative_tolerance": 1e-4,
												  "maximum_iterations": 20,
												  "report": True,
												  "error_on_nonconvergence": False}}

		problem = fe.NonlinearVariationalProblem(E_ds, d, [], H)
		problem.set_bounds(lower, upper)

		solver = fe.NonlinearVariationalSolver(problem)
		solver.parameters.update(snes_solver_parameters)
		solver.solve()
		return d

	def solve_damage_bourdin():
		E_ds = -E*A*fe.inner(fe.grad(u), fe.grad(u))*fe.inner(1.0 - d_tr, d_test)*fe.dx + Gc*(
					1.0 / lc * fe.inner(d_tr, d_test) +
					lc * fe.inner(fe.grad(d_tr), fe.grad(d_test))) * fe.dx
		fe.solve(fe.lhs(E_ds) == fe.rhs(E_ds), d, BC_s)
		return d

	def solve_damage_stress():
		E_ds = -mc_bracket(E**2*fe.inner(fe.grad(u), fe.grad(u))/f_t**2 - 1.0)*fe.inner(1.0 - d_tr, d_test) * fe.dx + (
						   fe.inner(d_tr, d_test) +
						   lc ** 2 * fe.inner(fe.grad(d_tr), fe.grad(d_test))) * fe.dx
		fe.solve(fe.lhs(E_ds) == fe.rhs(E_ds), d, BC_s)
		return d

	# --------------------
	# Parameters
	# --------------------
	n = 200
	l = 1.0
	A = 1.0
	lc = 0.01
	# Gc = 1.0
	f_t = 6.0
	E = 1.0

	if form == "bourdin":
		Gc = lc*256.0/27.0*f_t**2/E
	elif form == "pham":
		Gc = lc*8.0/3.0*f_t**2/E
	else:
		Gc = 1.0

	# Time parameters
	t = 0.0
	t_end = 14.0
	dt = 0.1

	# Iteration parameters
	iterr = 1
	error = 1.0
	toll = 1.0e-4
	maxiter = 30

	# --------------------
	# Define geometry
	# --------------------
	mesh = fe.IntervalMesh(n, 0.0, l)

	fe.plot(mesh, "Mesh")
	plt.show()

	# --------------------
	# Define spaces
	# --------------------
	V = fe.FunctionSpace(mesh, "CG", 1)
	W = fe.FunctionSpace(mesh, "DG", 0)
	u_tr = fe.TrialFunction(V)
	u_test = fe.TestFunction(V)
	d_tr = fe.TrialFunction(V)
	d_test = fe.TestFunction(V)

	# --------------------
	# Boundary conditions
	# --------------------
	u_D = fe.Expression("t", t=0.0, degree=0)
	BC_u_1 = fe.DirichletBC(V, fe.Constant(0.0), left_end)
	BC_u_2 = fe.DirichletBC(V, u_D, right_end)
	BC_u = [BC_u_1, BC_u_2]
	BC_s = []

	# --------------------
	# Initialization
	# --------------------
	u = fe.Function(V)
	u_old = fe.Function(V)
	d = fe.Function(V)
	d_old = fe.Function(V)
	d_min = fe.Function(V)

	# E = fe.Expression("1.0 - 0.1*near(x[0], 0.5, 0.001)", degree=1)
	fe.plot(fe.project(E, V))
	plt.show()

	# --------------------
	# Variational problem
	# --------------------
	E_du = (1-d)**2*E*A*fe.inner(fe.grad(u_test), fe.grad(u_tr))*fe.dx

	# --------------------
	# Staggered loop
	# --------------------

	force = []
	time = []
	sigma_exact = []
	temp = []

	while t < t_end:

		u_D.t = t
		error = 1.0
		iterr = 1
		d.assign(fe.interpolate(fe.Constant(0.0), V))

		while error > toll and iterr < maxiter:
			# Displacement solution
			fe.solve(fe.lhs(E_du) == fe.rhs(E_du), u_old, BC_u)
			error1 = fe.norm(fe.project(u-u_old, V))
			u.assign(u_old)

			# Damage solution
			if form == "pham":
				solve_damage_pham(d_min)
			elif form == "bourdin":
				solve_damage_bourdin()
			elif form == "stress":
				solve_damage_stress()
			else:
				print("You must choose damage formulation!")
			# fe.solve(fe.lhs(E_ds) == fe.rhs(E_ds), d_old, BC_s)
			error2 = fe.norm(fe.project(d-d_old, V))
			d_old.assign(d)

			# While condition
			iterr += 1
			error = max(error1, error2)

			if iterr == maxiter:
				print("max iteration reached")

		d_min.assign(d)

		temp.append(fe.assemble(E**2*fe.inner(fe.grad(u), fe.grad(u))/f_t**2*fe.dx))
		time.append(t)
		sigma = fe.project((1.0-d)**2*E*A*u.dx(0), W)
		force.append(sigma.vector()[n-1])
		E_0 = 1.0
		sigma_exact.append(Gc**2*l**3*E_0*t/(E_0*lc*t**2 + Gc*l**2)**2)

		t += dt

	return time, force


time_p, force_p = solver("pham")
time_b, force_b = solver("bourdin")
time_s, force_s = solver("stress")

plt.plot(time_b, force_b, label="bourdin")
plt.plot(time_s, force_s, label="stress")
plt.plot(time_p, force_p, "--", label="pham")
# plt.plot(time, sigma_exact, "r")
plt.xlabel("t")
plt.ylabel("reaction")
plt.legend()
# plt.figure()
# fe.plot(u)
# plt.xlabel("x")
# plt.ylabel("u(x)")
# plt.figure()
# fe.plot(d)
# plt.xlabel("x")
# plt.ylabel("d(x)")
plt.show()
