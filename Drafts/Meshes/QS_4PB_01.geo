//Inputs
L = 1.1;
L0 = 0.2;
L1 = 0.24;
Loff = 0.05;
H = 0.02;

gridsize = 0.009;
gridsize2 = 0.001;

//Points
Point(1) = {0, 0, 0, gridsize};
Point(2) = {Loff, 0, 0, gridsize};
Point(3) = {L/2-0.6*L1, 0, 0, gridsize};
Point(4) = {L/2-L1/2, 0, 0, gridsize2};
Point(5) = {L/2, 0, 0, gridsize2};
Point(6) = {L/2+L1/2, 0, 0, gridsize2};
Point(7) = {L/2+0.6*L1, 0, 0, gridsize};
Point(8) = {L-Loff, 0, 0, gridsize};
Point(9) = {L, 0, 0, gridsize};
Point(10) = {L, H, 0, gridsize};
Point(11) = {L/2+0.6*L1, H, 0, gridsize};
Point(12) = {L/2+L1/2, H, 0, gridsize2};
Point(13) = {L/2+L0/2, H, 0, gridsize2};
Point(14) = {L/2-L0/2, H, 0, gridsize2};
Point(15) = {L/2-L1/2, H, 0, gridsize2};
Point(16) = {L/2-0.6*L1, H, 0, gridsize};
Point(17) = {0, H, 0, gridsize};

//Lines
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,5};
Line(5) = {5,6};
Line(6) = {6,7};
Line(7) = {7,8};
Line(8) = {8,9};
Line(9) = {9,10};
Line(10) = {10,11};
Line(11) = {11,12};
Line(12) = {12,13};
Line(13) = {13,14};
Line(14) = {14,15};
Line(15) = {15,16};
Line(16) = {16,17};
Line(17) = {17,1};

//Line loops
Line Loop(1) = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17};

//Surfaces
Plane Surface(1) = 1;

