from fenics import *
import matplotlib.pyplot as plt
import numpy as np

plt.autoscale()

# --------------------
# Methods
# --------------------
def get_zn(du_loc, dphi_loc, s_i, h, t1, t2, t3, t1_t, t2_t, t3_t):
	du_local = du_loc.vector().get_local()
	dphi_local = dphi_loc.vector().get_local()
	s_local = s_i.compute_vertex_values()
	# plot(dphi_loc)
	# plot(du_loc)
	# plt.show()
	t1_loc = []
	t2_loc = []
	t3_loc = []
	t1_loc_t = []
	t2_loc_t = []
	t3_loc_t = []
	znn = []
	for j in range(0, len(du_local)):
		z_n_local, g_m, g_p, g_m_t, g_p_t = get_params(du_local[j], dphi_local[j], s_local[j], h)
		t1_loc.append(g_m*(z_n_local + 0.5*h) + g_p*(0.5*h - z_n_local))
		t2_loc.append(g_m*(z_n_local*z_n_local - 0.125*h*h) + g_p*(0.125*h*h - z_n_local*z_n_local))
		t3_loc.append(g_m*(z_n_local*z_n_local*z_n_local + h*h*h/24.0) + g_p*(h*h*h/24.0 - z_n_local*z_n_local*z_n_local))
		t1_loc_t.append(g_m_t * (z_n_local + 0.5 * h) + g_p_t * (0.5 * h - z_n_local))
		t2_loc_t.append(g_m_t * (z_n_local * z_n_local - 0.125 * h * h) + g_p_t * (0.125 * h * h - z_n_local * z_n_local))
		t3_loc_t.append(g_m_t * (z_n_local * z_n_local * z_n_local + h * h * h / 24.0) + g_p_t * (h * h * h / 24.0 - z_n_local * z_n_local * z_n_local))
		znn.append(z_n_local)

	# plt.plot(t3_loc)
	# plt.plot(znn)
	# plt.show()

	t1.vector().set_local(t1_loc)
	t2.vector().set_local(t2_loc)
	t3.vector().set_local(t3_loc)
	t1_t.vector().set_local(t1_loc_t)
	t2_t.vector().set_local(t2_loc_t)
	t3_t.vector().set_local(t3_loc_t)

# def get_zn_2(du_loc, dphi_loc, s_t, s_c, h):
#     du_local = du_loc.vector().get_local()
#     dphi_local = dphi_loc.vector().get_local()
#     s_t_local = s_t.vector().get_local()
#     s_c_local = s_c.vector().get_local()
#
#     t1_loc = []
#     t2_loc = []
#     t3_loc = []
#     t1_loc_t = []
#     t2_loc_t = []
#     t3_loc_t = []
#     znn = []
#
#     for j in range(0, len(du_local)):
#         z_n_local, g_m, g_p, g_m_t, g_p_t = get_params(du_local[j], dphi_local[j], s_local[j], h)
#         t1_loc.append(g_m*(z_n_local + 0.5*h) + g_p*(0.5*h - z_n_local))
#         t2_loc.append(g_m*(z_n_local*z_n_local - 0.125*h*h) + g_p*(0.125*h*h - z_n_local*z_n_local))
#         t3_loc.append(g_m*(z_n_local*z_n_local*z_n_local + h*h*h/24.0) + g_p*(h*h*h/24.0 - z_n_local*z_n_local*z_n_local))
#         t1_loc_t.append(g_m_t * (z_n_local + 0.5 * h) + g_p_t * (0.5 * h - z_n_local))
#         t2_loc_t.append(g_m_t * (z_n_local * z_n_local - 0.125 * h * h) + g_p_t * (0.125 * h * h - z_n_local * z_n_local))
#         t3_loc_t.append(g_m_t * (z_n_local * z_n_local * z_n_local + h * h * h / 24.0) + g_p_t * (h * h * h / 24.0 - z_n_local * z_n_local * z_n_local))
#         znn.append(z_n_local)
#
#     # plt.plot(t3_loc)
#     # plt.plot(znn)
#     # plt.show()
#
#     t1.vector().set_local(t1_loc)
#     t2.vector().set_local(t2_loc)
#     t3.vector().set_local(t3_loc)
#     t1_t.vector().set_local(t1_loc_t)
#     t2_t.vector().set_local(t2_loc_t)
#     t3_t.vector().set_local(t3_loc_t)


def get_params(d_u_local, d_phi_local, s_i, h):
	if abs(d_phi_local) < 1.0e-10:
		# zn = 10.0
		zn = 0.0
	else:
		zn = -d_u_local / d_phi_local

	if d_u_local <= -0.5*h*abs(d_phi_local):
		# zn = 0.0
		gm = 1.0
		gp = 1.0
		gm_t = 0.0
		gp_t = 0.0
		# print("a")
	elif d_u_local >= 0.5*h*abs(d_phi_local):
		# zn = 0.0
		gm = s_i
		gp = s_i
		gm_t = 1.0
		gp_t = 1.0
		# print("b")
	else:
		if d_phi_local > 0.0:
			gm = 1.0
			gp = s_i
			gm_t = 0.0
			gp_t = 1.0
			# print("c")
		else:
			gm = s_i
			gp = 1.0
			gm_t = 1.0
			gp_t = 0.0
			# print("d")
	return zn, gm, gp, gm_t, gp_t


def max_f(a, b, W):
	maxim = np.maximum(a.vector().get_local(), b.vector().get_local())
	bla = Function(W)
	bla.vector().set_local(maxim.flatten())
	return bla


def solver():

	# --------------------
	# Parameters
	# --------------------
	# Material parameters
	E = 3.61e6  # Young's modulus
	G = 1.28e6  # Shear modulus
	Gc = 0.5  # Fracture toughness
	lc = 0.01  # Width of localization zone

	# Geometry parameters
	b = 0.1  # Width
	h = 0.001  # Height
	Ar = b * h  # Area
	Iy = (1.0 / 12.0) * b * h * h * h  # Moment of inertia of cross section
	le = 0.8  # Length of beam

	# Other parameters
	n = 200  # Number of elements
	f_lin = 0.0  # Value of constant continuous load
	k = 0.001
	hist = 0.0
	u_pr = 0.20268
	# u_pr = 0.9
	Fg = 0.0006
	# Fg = 0.0009

	# --------------------
	# Define geometry
	# --------------------
	mesh = IntervalMesh(n, 0.0, le)

	# --------------------
	# Spaces and functions
	# --------------------
	P1 = FiniteElement('P', interval, 2)  # Space of quadratic polynomial functions (to overcome locking)
	element = MixedElement([P1, P1, P1])  # Element of mixed space
	V = FunctionSpace(mesh, element)  # Mixed space
	W = FunctionSpace(mesh, 'P', 1)  # Space of linear polynomial functions

	# Test and trial functions for displacement variational problem
	d_u, d_w, d_phi = TestFunctions(V)
	u, w, phi = TrialFunctions(V)

	# Test and trial function for damage-evolution variational problem
	d_s = TestFunction(W)
	#dd_s = TestFunction(FunctionSpace(mesh, 'P', 2))
	s = TrialFunction(W)

	# Other functions
	X = Function(V)
	f = Function(V)
	s_ = Function(W)
	z_N = Function(W)
	#TODO: Make better solution
	t1 = Function(W)
	t2 = Function(W)
	t3 = Function(W)
	t1_t = Function(W)
	t2_t = Function(W)
	t3_t = Function(W)

	# --------------------
	# Boundary conditions
	# --------------------
	# Definition of beam ends
	def left_end(x):
		return near(x[0], 0.0)

	def right_end(x):
		return near(x[0], le)

	def middle_point(x):
		return near(x[0], le/2)

	# Application of boundary conditions
	bc1 = DirichletBC(V.sub(0), Constant(0.0), left_end)
	bc2 = DirichletBC(V.sub(1), Constant(0.0), left_end)
	bc3 = DirichletBC(V.sub(1), Constant(0.0), right_end)
	bc4 = DirichletBC(V.sub(0), Constant(0.0), right_end)
	bc6 = DirichletBC(V.sub(2), Constant(0.0), left_end)
	bc7 = DirichletBC(V.sub(2), Constant(0.0), right_end)
	bc5 = DirichletBC(V.sub(1), Constant(u_pr), middle_point)
	bc = [bc1, bc2, bc3, bc4]

	# Boundary conditions for phase field
	bc1_s = DirichletBC(W, Constant(1.0), left_end)
	bc2_s = DirichletBC(W, Constant(1.0), right_end)
	# bc_s = [bc1_s, bc2_s]
	bc_s = [bc1_s, bc2_s]

	# --------------------
	# Initialization
	# --------------------
	u_, w_, phi_ = split(X)
	f.interpolate(Constant((0.0, f_lin, 0.0)))

	s_.interpolate(Constant(1.0))

	t1.interpolate(Constant(h))
	t2.interpolate(Constant(0.0))
	t3.interpolate(Constant(h * h * h / 12.0))
	t1_t.interpolate(Constant(h))
	t2_t.interpolate(Constant(0.0))
	t3_t.interpolate(Constant(h * h * h / 12.0))

	s_init = 1.0

	# --------------------
	# Solution
	# --------------------
	for i in range(20):
		# Delete?
		d_u, d_w, d_phi = TestFunctions(V)
		u, w, phi = TrialFunctions(V)

		# Calc of degradation function
		s_2 = project((1-k)*(s_**2) + k, FunctionSpace(mesh, 'P', 2))
		#bla = assemble(s_ ** 2 * d_s * dx)

		if i > 0:
			get_zn(project(u_.dx(0), W), project(phi_.dx(0), W), s_2, h, t1, t2, t3, t1_t, t2_t, t3_t)

		# Variational problem
		A = d_u.dx(0) * E * b * t1 * u.dx(0) * dx + d_u.dx(0) * E * b * t2 * phi.dx(0) * dx + d_w.dx(
			0) * G * b * t1 * w.dx(0) * dx + \
			d_w.dx(0) * G * b * t1 * phi * dx + d_phi.dx(0) * E * b * t3 * phi.dx(0) * dx + d_phi.dx(
			0) * E * b * t2 * u.dx(0) * dx + \
			d_phi * G * b * t1 * (phi + w.dx(0)) * dx
		l = Constant(f_lin) * d_w * dx

		# Solver for linear variational problems
		A_ass, b_ass = assemble_system(A, l, bc)
		pointForce = PointSource(V.sub(1), Point(le/2.0), Fg)
		pointForce.apply(b_ass)
		solve(A_ass, X.vector(), b_ass)

		u_, w_, phi_ = X.split(deepcopy=True)

		get_zn(project(u_.dx(0), W), project(phi_.dx(0), W), s_2, h, t1, t2, t3, t1_t, t2_t, t3_t)

		histo = 0.5 * phi_.dx(0) * E * b * t3_t * phi_.dx(0) + 0.5 * u_.dx(0) * E * b * t1_t * u_.dx(0) + u_.dx(
			0) * E * b * t2_t * phi_.dx(0) + \
				0.5 * phi_ * G * b * t1_t * phi_ + w_.dx(0) * G * b * t1_t * phi_ + 0.5 * w_.dx(
			0) * G * b * t1_t * w_.dx(0)

		hist = project(histo, W)

		E_phi = ((4 * lc * (1-k) * hist / (Gc*b*h) + 1) * inner(s, d_s) + 4 * lc * lc * inner(grad(s), grad(d_s)) - d_s) * dx
		solve(lhs(E_phi) == rhs(E_phi), s_, bc_s)

		plot(s_)
		plt.autoscale()
		# plt.show()

	# plot(w_, c="red")
	# plt.autoscale()
	# plt.show()
	plot(s_, c="red")
	plt.show()

	plot(w_)
	plt.show()

solver()
