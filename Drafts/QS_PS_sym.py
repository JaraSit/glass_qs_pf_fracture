# -------------------------
# 2D Plain strain
# -------------------------

# -------------------------
# Description:
# - Phase-field SYMMETRIC fracture for testing different formulations
# - Quasi-static formulation
# - reference:
#
# Last edit: 10.11.2019
# -------------------------


import fenics as fe
import numpy as np
import matplotlib.pyplot as plt
import math


# --------------------
# Functions and classes
# --------------------
def left_point(x):
	return fe.near(x[0], l_off) and fe.near(x[1], 0.0)


def right_point(x):
	return fe.near(x[0], l_x-l_off) and fe.near(x[1], 0.0)


def left_load_point(x):
	return fe.near(x[0], l_x/2-l_0/2) and fe.near(x[1], l_y)


def right_load_point(x):
	return fe.near(x[0], l_x/2+l_0/2) and fe.near(x[1], l_y)


# Symmetry gradient of displacements
def eps(v):
	return fe.sym(fe.grad(v))


# Elastic stress tensor
def sigma_el(u_i):
	return lmbda*(fe.tr(eps(u_i)))*fe.Identity(2) + 2.0*mu*(eps(u_i))


# Positive stress tensor
def sigma_p(u_i):
	return lmbda*mc_bracket(fe.tr(eps(u_i)))*fe.Identity(2) + 2*mu*eps_p(u_i)


# Negative stress tensor
def sigma_n(u_i):
	return lmbda*mc_bracket(-fe.tr(eps(u_i)))*fe.Identity(2) + 2*mu*eps_n(u_i)


# energy decomposition: spectral decomposition
# eigenvalues for 2x2 matrix
def eig_v(a):
	v00 = a[0, 0]/2 + a[1, 1]/2 - fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2
	v01 = 0.0
	v10 = 0.0
	v11 = a[0, 0]/2 + a[1, 1]/2 + fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2
	return v00, v01, v10, v11


# eigenvectors
def eig_w(a):
	w00 = -a[0, 1]/(a[0, 0]/2 - a[1, 1]/2 + fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2)
	w01 = -a[0, 1]/(a[0, 0]/2 - a[1, 1]/2 - fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2)
	w10 = 1.0
	w11 = 1.0
	return w00, w01, w10, w11


# positive strain tensor
def eps_p(v):
	v00, v01, v10, v11 = eig_v(eps(v))
	v00 = fe.conditional(fe.gt(v00, 0.0), v00, 0.0)
	v11 = fe.conditional(fe.gt(v11, 0.0), v11, 0.0)
	w00, w01, w10, w11 = eig_w(eps(v))
	wp = ([w00, w01], [w10, w11])
	wp = fe.as_tensor(wp)
	vp = ([v00, v01], [v10, v11])
	vp = fe.as_tensor(vp)
	return wp*vp*fe.inv(wp)


# negative strain tensor
def eps_n(v):
	v00, v01, v10, v11 = eig_v(eps(v))
	v00 = fe.conditional(fe.lt(v00, 0.0), v00, 0.0)
	v11 = fe.conditional(fe.lt(v11, 0.0), v11, 0.0)
	w00, w01, w10, w11 = eig_w(eps(v))
	wn = ([w00, w01], [w10, w11])
	wn = fe.as_tensor(wn)
	vn = ([v00, v01], [v10, v11])
	vn = fe.as_tensor(vn)
	return wn*vn*fe.inv(wn)


# Macauley bracket
def mc_bracket(v):
	return 0.5*(v+abs(v))


def solve_elastic_nl():
	A_form_nl = (1-d)**2*fe.inner(sigma_p(u), eps(u_test))*fe.dx + fe.inner(sigma_n(u), eps(u_test))*fe.dx
	# fe.solve(A_form_nl == 0, u, BC_u)
	J = fe.derivative(A_form_nl, u)
	problem = fe.NonlinearVariationalProblem(A_form_nl, u, BC_u, J)
	solver = fe.NonlinearVariationalSolver(problem)
	prm = solver.parameters
	prm['newton_solver']['absolute_tolerance'] = 1E-5
	prm['newton_solver']['relative_tolerance'] = 1E-5
	prm['newton_solver']['maximum_iterations'] = 25
	prm['newton_solver']['relaxation_parameter'] = 1.0
	solver.solve()
	return u


def solve_elastic():
	A_form = (1-d)**2*fe.inner(sigma_el(u_tr), eps(u_test))*fe.dx
	fe.solve(fe.lhs(A_form) == fe.rhs(A_form), u, BC_u)
	return u


def solve_damage_bourdin():
	E_ds = -(fe.inner(sigma_p(u), eps(u)))*fe.inner(1.0-d_tr, d_test)*fe.dx
	E_ds += Gc*(1.0/lc*fe.inner(d_tr, d_test) + lc*fe.inner(fe.grad(d_tr), fe.grad(d_test)))*fe.dx
	fe.solve(fe.lhs(E_ds) == fe.rhs(E_ds), d, BC_s)
	return d


def solve_damage_driving_force():
	stress = sigma_el(u)
	sg00, sg01, sg10, sg11 = eig_v(stress)
	D = mc_bracket((mc_bracket(sg00)/f_t)**2 + (mc_bracket(sg11)/f_t)**2 - 1)
	E_ds = -D*fe.inner(1.0-d_tr, d_test)*fe.dx
	E_ds += (fe.inner(d_tr, d_test) + lc**2*fe.inner(fe.grad(d_tr), fe.grad(d_test)))*fe.dx
	fe.solve(fe.lhs(E_ds) == fe.rhs(E_ds), d, BC_s)
	return d


def solve_damage_pham(d_min):
	lower = d_min
	upper = fe.interpolate(fe.Constant(1.00), W)

	# Pham formulation
	E_ds = -fe.inner(sigma_p(u), eps(u))*fe.inner(1.0 - d, d_test)*fe.dx
	E_ds += Gc*(1.0/lc*d_test + lc*fe.inner(fe.grad(d), fe.grad(d_test)))*fe.dx

	# Solution of damage formulation
	H = fe.derivative(E_ds, d, fe.TrialFunction(W))

	snes_solver_parameters = {"nonlinear_solver": "snes",
							  "snes_solver": {"linear_solver": "lu",
											  "relative_tolerance": 1e-4,
											  "maximum_iterations": 20,
											  "report": True,
											  "error_on_nonconvergence": False}}

	problem = fe.NonlinearVariationalProblem(E_ds, d, [], H)
	problem.set_bounds(lower, upper)

	solver = fe.NonlinearVariationalSolver(problem)
	solver.parameters.update(snes_solver_parameters)
	solver.solve()
	return d


def get_pos_eig(fce):
	mesh1 = fce.function_space().mesh()
	vert_vals = fce.vector().get_local()
	temp = vert_vals.reshape([mesh.num_vertices(),2,2])
	[eigL,eigR] = np.linalg.eig(temp)
	eigL1 = eigL[:, 0]
	eigL2 = eigL[:, 1]
	psiL = 0.5*lmbda*np.power((eigL1 + eigL2).clip(0), 2)+ mu*(np.power(eigL1.clip(0), 2)+ np.power(eigL2.clip(0), 2))
	eig = fe.Function(fe.FunctionSpace(mesh1,'CG',1))
	eig.vector().set_local(psiL.flatten())
	return eig


# --------------------
# Parameters
# --------------------
# Geometry parameters
l_x, l_y = 1.1, 0.02  # Length and thickness of glass beam
l_off = 0.05  # Support offset
l_0 = 0.2  # Pitch of load points

# Material parameters
lc = 0.005
f_t = 60.0e6
E = 76.6e9  # Young's modulus
Gc = lc*256.0/27.0*f_t**2/E

# lc = 27.0/256.0*E*Gc/f_t**2  # 1D relationship between f_t <-> Gc <-> lc
# Gc = 60.0

nu = 0.22  # Poisson ratio
mu, lmbda = E/2/(1+nu), E*nu/(1+nu)/(1-2*nu)  # Lame coefficients
lmbda = 2*mu*lmbda/(lmbda+2*mu)  # Correction for plane stress

# Time parameters
t_start = 0.1
# t_end = 8.3 for final
# t_end = 7.41005 for detail
t_end = 10.59
# t_end = 7.7001
dt1 = 0.05
dt2 = 0.00000005
t = t_start

# # Iteration parameters
# iterr = 1
# error = 1.0
# toll = 1.0e-4
# maxiter = 30

# --------------------
# Define geometry
# --------------------
mesh = fe.Mesh("Meshes/QS_4PB_testing.xml")

fe.plot(mesh, "Mesh")
plt.show()

# --------------------
# Define spaces
# --------------------
V = fe.VectorFunctionSpace(mesh, "CG", 1)  # Function space for displacements
print(V.dim())
W = fe.FunctionSpace(mesh, "CG", 1)  # Function space for damage variable
u_tr = fe.TrialFunction(V)
u_test = fe.TestFunction(V)
d_tr = fe.TrialFunction(W)
d_test = fe.TestFunction(W)


# --------------------
# Boundary conditions
# --------------------
u_D = fe.Expression("-t/1000.0", t=0.0, degree=0)  # Prescribed displacement
BC_u_1 = fe.DirichletBC(V, fe.Constant((0.0, 0.0)), left_point, method="pointwise")
BC_u_2 = fe.DirichletBC(V.sub(1), 0.0, right_point, method="pointwise")
BC_u_3 = fe.DirichletBC(V.sub(1), u_D, left_load_point, method="pointwise")
BC_u_4 = fe.DirichletBC(V.sub(1), u_D, right_load_point, method="pointwise")
BC_u = [BC_u_1, BC_u_2, BC_u_3, BC_u_4]
BC_s = []

# --------------------
# Initialization
# --------------------
u = fe.Function(V)
d = fe.Function(W)

du = fe.Function(V)
dd = fe.Function(W)
u_old = fe.Function(V)
d_old = fe.Function(W)
d_min = fe.Function(W)

file_u = fe.XDMFFile("Results/PS_QS_form_test_u_01.xdmf")  # XDMF file for displacements
file_d = fe.XDMFFile("Results/PS_QS_form_test_d_01.xdmf")  # XDMF file for damage variable

# t1 = np.linspace(t_start, t_end, steps)
# dt1 = np.asscalar(t1[1] - t1[0])
# t2_temp = np.linspace(t_end, t_end2, steps2)
# dt2 = np.asscalar(t2_temp[1] - t2_temp[0])
# t2 = t2_temp[2:]
# print(t2_temp)
# print(t2)

# --------------------
# Variational problem
# --------------------
# E_du = (1-d)**2*fe.inner(sigma_el(u_old), eps(u_test))*fe.dx + rho/(dt2*dt2)*fe.dot(u_tr - 2*u_old + u_old2, u_test)*fe.dx
# E_ds = -2*(0.5*fe.inner(sigma_el(u), eps(u)))*fe.inner(1.0-d_tr, d_test)*fe.dx
# E_ds += Gc*(1.0/lc*fe.inner(d_tr, d_test) + lc*fe.inner(fe.grad(d_tr), fe.grad(d_test)))*fe.dx
# A_form_implicit = (1-d)**2*fe.inner(sigma_el(u_tr), eps(u_test))*fe.dx + 4*rho/(dt1*dt1)*fe.dot(u_tr - u_bar, u_test)*fe.dx
# A_form_implicit2 = (1-d)**2*fe.inner(sigma_el(u_tr), eps(u_test))*fe.dx + 4*rho/(dt2*dt2)*fe.dot(u_tr - u_bar, u_test)*fe.dx

# M_form = fe.lhs(E_du)
# M_action = fe.action(M_form, fe.interpolate(fe.Constant((1.0, 1.0)), V))
# M_lumped = fe.assemble(M_form)
# M_lumped.zero()
# M_lumped_assembled = fe.assemble(M_action)
# M_lumped.set_diagonal(M_lumped_assembled)
# [BC_u_i.apply(M_lumped) for BC_u_i in BC_u]
# BC_u_1.apply(M_lumped)
# BC_u_2.apply(M_lumped)
# BC_u_3.apply(M_lumped)

u_D.t = -t_start/1000.0
u = solve_elastic()

max_iter = 20
tol = 1.0e-5
# --------------------
# Time loop
# --------------------
while t < t_end:
	print(t)
	u_D.t = t

	ite = 1
	err = 10.0

	while err > tol:
		u = solve_elastic()
		# d = solve_damage_pham(d_min)
		# d = solve_damage_bourdin()
		d = solve_damage_driving_force()

		du.assign(u - u_old)
		dd.assign(d - d_old)

		err_u = fe.norm(du) / fe.norm(u)
		err_d = fe.norm(dd)
		err = max(err_u, err_d)

		print('iter', ite, 'errors', err_u, err_d)

		u_old.assign(u)
		d_old.assign(d)

		ite += 1

		if ite > max_iter:
			print("max iterations reached")
			break

	d_min.assign(d)
	file_u.write(u, t)
	file_d.write(d, t)

	t += dt1

file_u.close()
file_d.close()

