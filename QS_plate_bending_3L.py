import formulations as f
import formulations_3L as f3l
import solvers.solver as s
import fenics as fe
import numpy as np
import matplotlib.pyplot as plt


# --------------------
# Functions and classes
# --------------------
def left_point(x):
	return fe.near(x[0], l_off)


def right_point(x):
	return fe.near(x[0], l_x - l_off)


def left_load_point(x):
	return fe.near(x[0], l_x / 2 - l_0 / 2)


def right_load_point(x):
	return fe.near(x[0], l_x / 2 + l_0 / 2)


def symmetry_axis(x, on_boundary):
	return fe.near(x[0], l_x/2) and on_boundary


def corner(x):
	return fe.near(x[0], l_off) and fe.near(x[1], 0.0)


def prepare_mesh():
	nx = 11
	ny = 10

	l_sym = 0.5 * l_x
	mesh = fe.RectangleMesh(fe.Point(0., 0.), fe.Point(0.5*l_x, l_y), nx, ny, diagonal="left/right")

	# fe.plot(mesh, "Mesh")
	# plt.show()

	# Refine
	markers = fe.MeshFunction("bool", mesh, 2)
	markers.set_all(False)
	for c in fe.cells(mesh):
		# Mark cells with facet midpoints near x == L/2
		for f in fe.facets(c):
			if fe.near(f.midpoint()[0], l_x/2., 0.13):
				markers[c] = True
	mesh = fe.refine(mesh, markers, redistribute=False)

	# fe.plot(mesh, "Mesh")
	# plt.show()

	# Refine again
	markers = fe.MeshFunction("bool", mesh, 2)
	markers.set_all(False)
	for c in fe.cells(mesh):
		# Mark cells with facet midpoints near x == L/2
		for f in fe.facets(c):
			if fe.near(f.midpoint()[0], l_x / 2., 0.13):
				markers[c] = True
	mesh = fe.refine(mesh, markers, redistribute=False)

	# Refine again
	markers = fe.MeshFunction("bool", mesh, 2)
	markers.set_all(False)
	for c in fe.cells(mesh):
		# Mark cells with facet midpoints near x == L/2
		for f in fe.facets(c):
			if fe.near(f.midpoint()[0], l_x / 2. - 0.1, 0.05):
				markers[c] = True
	mesh = fe.refine(mesh, markers, redistribute=False)

	# Refine again
	markers = fe.MeshFunction("bool", mesh, 2)
	markers.set_all(False)
	for c in fe.cells(mesh):
		# Mark cells with facet midpoints near x == L/2
		for f in fe.facets(c):
			if fe.near(f.midpoint()[0], l_x / 2. - 0.1, 0.05):
				markers[c] = True
	mesh = fe.refine(mesh, markers, redistribute=False)

	return mesh


def init_bc(model):
	model.init_spaces()
	V = model.V

	# w1, u1, phi1, u3, phi3
	u_d = fe.Expression("-t/1000.0", t=0.0, degree=0)  # Prescribed displacement
	BC_u_1 = fe.DirichletBC(V.sub(0), 0.0, left_point)
	BC_u_2 = fe.DirichletBC(V.sub(0), u_d, left_load_point)
	BC_u_3 = fe.DirichletBC(V.sub(1).sub(0), 0.0, symmetry_axis)
	BC_u_3a = fe.DirichletBC(V.sub(1).sub(1), 0.0, corner, method="pointwise")
	BC_u_4 = fe.DirichletBC(V.sub(2).sub(1), 0.0, symmetry_axis)
	BC_u_5 = fe.DirichletBC(V.sub(3).sub(0), 0.0, symmetry_axis)
	BC_u_6 = fe.DirichletBC(V.sub(4).sub(1), 0.0, symmetry_axis)
	BC_u = [BC_u_1, BC_u_2, BC_u_3, BC_u_3a, BC_u_4, BC_u_5, BC_u_6]
	BC_d = []

	model.set_bc(BC_u, BC_d, u_d)


def solve(file, model):
	solution = f.PostProcess(file)
	#ss = s.TimeDisplacementScheme(model, time_space, solution)
	ss = s.StaggeredScheme(model, time_space, solution)
	ss.max_iter = max_iter

	mon1 = f.Monitor(0.5*l_x, 0.5*l_y)
	ss.set_monitor(mon1)

	p = fe.Point((0.5 * l_x - 0.5 * l_0, l_y))
	index = find_dofs(p, 0, model.V)
	mon2 = f.MonitorReactions(index)
	ss.set_monitor_react(mon2)

	ss.solve(False)

	mon1.plot_monitor()
	solution.save_monitor(mon1)
	solution.save_monitor_r(mon2)


def calc_LG():
	model_pham = f3l.PlateDamageModel(LG_mat, cross, mesh_ref, "el", "pham", hs)
	init_bc(model_pham)
	solve("3L_Plate_test", model_pham)


def find_dofs(p, d, V):
	found_dofs = []
	V_dofs = V.tabulate_dof_coordinates()
	V0_dofs = V.sub(d).dofmap().dofs()
	for i in range(0, len(V0_dofs)):
		v_x = V_dofs[V0_dofs[i], 0]
		if fe.near(v_x, p.x()):
			found_dofs.append(V0_dofs[i])
	print("Found_dofs = ", found_dofs)
	return found_dofs


# Material parameters
E_glass = 70.0e9  # Young's modulus
nu_glass = 0.22  # Poisson ratio
G_EVA = 1.0e3*np.array([6933.9, 3898.6, 2289.2, 1672.7, 761.6, 2401.0, 65.2, 248.0, 575.6, 56.3, 188.6, 445.1, 300.1, 401.6, 348.1, 111.6, 127.2, 137.8, 50.5, 322.9, 100.0, 199.9])
theta = np.array([1.0e-9, 1.0e-8, 1.0e-7, 1.0e-6, 1.0e-5, 1.0e-4, 1.0e-3, 1.0e-2, 1.0e-1, 1.0, 1.0e1, 1.0e2, 1.0e3, 1.0e4, 1.0e5, 1.0e6, 1.0e7, 1.0e8, 1.0e9, 1.0e10, 1.0e11, 1.0e12])
nu_eva = 0.49
G_inf = 682.18e3
c1 = 339.102
c2 = 1185.816
T_ref = 20.0
T_act = 25.0

lc = 0.006
ft = 60.0e6
Gc_glass = lc*8.0/3.0*ft**2/E_glass

glass_mat = f.ElasticMaterial(E_glass, nu_glass, Gc_glass, lc, ft)
EVA_foil_mat = f.ViscoElasticMaterial(G_EVA, theta, G_inf, nu_eva, c1, c2, T_ref, T_act)

H1, H2, H3 = 0.00996, 0.00076, 0.00996

LG_mat = f.LG3LMaterial(glass_mat, EVA_foil_mat, [H1, H2, H3])

# Geometry parameters
l_x, l_y = 1.1, 0.36  # Length and thickness of glass beam
n_x, n_y = 275, 10  # Number of elements for uniform mesh
l_off = 0.05  # Support offset
l_0 = 0.2  # Pitch of load points

n_ni = 20
hs = np.linspace(-0.5*H1, 0.5*H1, n_ni)

cross = f.CrossSection(l_y, 0.0)

# Numerical parameters
max_iter = 10

# Time parameters - TODO: change time space
t_start = 0.1
t_middle = 8.0
t_end = 9.4
dt_1 = 0.5
dt_2 = 0.2
t_steps_1 = int((t_middle-t_start)/dt_1)
t_steps_2 = int((t_end-t_middle)/dt_2)
#t_steps = int((t_end-t_start)/dt_2)

time_space_1 = np.linspace(t_start, t_middle, t_steps_1)
time_space_2 = np.linspace(t_middle + dt_1, t_end, t_steps_2)

time_space = np.concatenate((time_space_1, time_space_2))
#time_space = np.linspace(t_start, t_end, t_steps)

# --------------------
# Define geometry
# --------------------
#mesh_unif = fe.Mesh("Meshes/QS_4PB_1D.xml")
#mesh_ref = fe.Mesh("Meshes/QS_4PB_1D_ref.xml")
mesh_ref = prepare_mesh()

#hmin = mesh_unif.hmin()
hmin_ref = mesh_ref.hmin()
print(hmin_ref)

fe.plot(mesh_ref)
plt.show()

calc_LG()
