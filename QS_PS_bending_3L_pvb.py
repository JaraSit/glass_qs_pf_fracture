import fenics as fe
import formulations as f
import matplotlib.pyplot as plt
import numpy as np
import solvers.solver as s


# --------------------
# Functions and classes
# --------------------
def left_point(x):
	return fe.near(x[0], l_off) and fe.near(x[1], 0.0)


def right_point(x):
	return fe.near(x[0], l_x - l_off) and fe.near(x[1], 0.0)


def left_load_point(x):
	return fe.near(x[0], l_x / 2 - l_0 / 2) and fe.near(x[1], l_y)


def right_load_point(x):
	return fe.near(x[0], l_x / 2 + l_0 / 2) and fe.near(x[1], l_y)


def symmetry_axis(x, on_boundary):
	return fe.near(x[0], l_x/2) and on_boundary


def init_bc(model):
	model.init_spaces(degr)
	V = model.V

	u_d = fe.Expression("-t/1000.0", t=0.0, degree=0)  # Prescribed displacement
	BC_u_1 = fe.DirichletBC(V.sub(1), 0.0, left_point, method="pointwise")
	BC_u_2 = fe.DirichletBC(V.sub(1), u_d, left_load_point, method="pointwise")
	BC_u_3 = fe.DirichletBC(V.sub(0), 0.0, symmetry_axis)
	BC_u = [BC_u_1, BC_u_2, BC_u_3]
	BC_d = []

	model.set_bc(BC_u, BC_d, u_d)


def solve(file, model, time_space_i):
	solution = f.PostProcess(file)
	ss = s.StaggeredScheme(model, time_space_i, solution)
	ss.max_iter = max_iter

	mon1 = f.Monitor(0.5 * l_x, 0.0)
	mon2 = f.Monitor(0.5 * l_x, l_y)
	ss.set_monitor([mon1, mon2])

	p = fe.Point((0.5*l_x - 0.5*l_0, l_y))
	index = find_dof(p, 1, model.V)
	mon_r = f.MonitorReaction(index)
	ss.set_monitor_react(mon_r)

	ss.solve(False)

	solution.save_monitors([mon1, mon2])
	solution.save_monitor_r(mon_r)
	mon1.plot_monitor()


def calc_LG():
	model_pham_min = f.PSDamageModel(LG_mat_PVB_min, mesh_ref, "el", "pham", "vd")
	model_pham_max = f.PSDamageModel(LG_mat_PVB_max, mesh_ref, "el", "pham", "vd")
	init_bc(model_pham_min)
	init_bc(model_pham_max)
	solve("Solutions/Solution_LG3_PS_el_pham_vd_pvb_hyb_min", model_pham_min, time_space_min)
	solve("Solutions/Solution_LG3_PS_el_pham_vd_pvb_hyb_max", model_pham_max, time_space_max)


def find_dof(p, d, V):
	found_dof = -1
	V_dofs = V.tabulate_dof_coordinates()
	V0_dofs = V.sub(d).dofmap().dofs()
	for i in range(0, len(V0_dofs)):
		v_x = V_dofs[V0_dofs[i], 0]
		v_y = V_dofs[V0_dofs[i], 1]
		if fe.near(v_x, p.x()) and fe.near(v_y, p.y()):
			found_dof = V0_dofs[i]
	print("Found_dof = ", found_dof)
	return found_dof


def make_time_space(t_array, t_dens_array):
	t_space = np.array([])
	for i in range(len(t_array) - 1):
		t_i_end = t_array[i+1] - t_dens_array[i]
		t_i_steps = int((t_i_end - t_array[i])/t_dens_array[i])
		t_i_space = np.linspace(t_array[i], t_i_end, t_i_steps)
		t_space = np.concatenate((t_space, t_i_space))
	return t_space


# Material parameters
E_glass = 70.0e9  # Young's modulus
nu_glass = 0.22  # Poisson ratio
G_PVB = 1.0e3*np.array([1782124.2, 519208.7, 546176.8, 216893.2, 13618.3, 4988.3, 1663.8, 587.2, 258.0, 63.8, 168.4])
theta_PVB = np.array([1.0e-5, 1.0e-4, 1.0e-3, 1.0e-2, 1.0e-1, 1.0, 1.0e1, 1.0e2, 1.0e3, 1.0e4, 1.0e5])
nu_pvb = 0.49
G_inf_pvb = 232.26e3
c1_pvb = 8.635
c2_pvb = 42.422
T_ref = 20.0
T_act = 25.0

lc = 0.0003
ft_glass_min = 29.0e6
ft_glass_max = 85.0e6

Gc_min = lc * 8.0 / 3.0 * ft_glass_min ** 2 / E_glass
Gc_max = lc * 8.0 / 3.0 * ft_glass_max ** 2 / E_glass
glass_mat_min = f.ElasticMaterial(E_glass, nu_glass, Gc_min, lc, ft_glass_min)
glass_mat_max = f.ElasticMaterial(E_glass, nu_glass, Gc_max, lc, ft_glass_max)
PVB_foil_mat = f.ViscoElasticMaterial(G_PVB, theta_PVB, G_inf_pvb, nu_pvb, c1_pvb, c2_pvb, T_ref, T_act)

H1, H2, H3 = 0.00996, 0.00078, 0.00996

LG_mat_PVB_min = f.LG3LMaterial(glass_mat_min, PVB_foil_mat, [H1, H2, H3])
LG_mat_PVB_max = f.LG3LMaterial(glass_mat_max, PVB_foil_mat, [H1, H2, H3])

# Geometry parameters
l_x, l_y = 1.1, H1+H2+H3  # Length and thickness of glass beam
l_off = 0.05  # Support offset
l_0 = 0.2  # Pitch of load points

# Numerical parameters
max_iter = 100
degr = 1

time_space_min = make_time_space([0.1, 6, 6.4, 6.51], [0.5, 0.1, 0.01])
time_space_max = make_time_space([0.1, 17.0, 19.0, 19.2], [0.5, 0.1, 0.01])


# --------------------
# Define geometry
# --------------------
mesh_ref = fe.Mesh("Meshes/QS_4PB_PS_gmsh_ref_pvb.xml")
hmin = mesh_ref.hmin()
print(hmin)

fe.plot(mesh_ref)
plt.show()

calc_LG()
