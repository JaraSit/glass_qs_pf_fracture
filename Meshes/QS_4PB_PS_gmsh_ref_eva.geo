//Inputs
L = 1.1;
L_sym = 0.5*L;
L_offset = 0.05;
L_load = 0.1;
H1 = 0.00996;
H2 = 0.00076;
H3 = 0.00996;
L_ref = 0.09;
L_ref2 = 0.12;
L_ref3 = 0.065;

gridsize = 0.003;
gridsize2 = 0.002;
gridsize3 = 0.0002; 

//Points
Point(1) = {0, 0, 0, gridsize};
Point(2) = {L_offset, 0, 0, gridsize};
Point(4) = {L_sym, 0, 0, gridsize3};
Point(5) = {0, H1, 0, gridsize};
Point(6) = {L_sym, H1, 0, gridsize2};
Point(7) = {0, H1+H2, 0, gridsize};
Point(8) = {L_sym, H1+H2, 0, gridsize2};
Point(9) = {0, H1+H2+H3, 0, gridsize};
Point(10) = {L_sym, H1+H2+H3, 0, gridsize2};
Point(11) = {L_sym - L_load, H1+H2+H3, 0, gridsize2};
Point(12) = {L_sym - L_ref, H1+H2+H3, 0, gridsize3};
Point(13) = {L_sym - L_ref, H1+H2, 0, gridsize3};
Point(14) = {L_sym - L_ref, H1, 0, gridsize3};
Point(15) = {L_sym - L_ref, 0.0, 0, gridsize3};
Point(16) = {L_sym - L_ref2, H1+H2+H3, 0, gridsize};
Point(17) = {L_sym - L_ref2, H1+H2, 0, gridsize};
Point(18) = {L_sym - L_ref2, H1, 0, gridsize};
Point(19) = {L_sym - L_ref2, 0.0, 0, gridsize};
Point(20) = {L_sym - L_ref3, 0.0, 0, gridsize3};
Point(21) = {L_sym - L_ref3, H1, 0, gridsize3};
Point(22) = {L_sym - L_ref3, H1+H2, 0, gridsize3};
Point(23) = {L_sym - L_ref3, H1+H2+H3, 0, gridsize3};

//Lines
Line(7) = {1,2};
Line(9) = {2,19};
Line(10) = {1,5};
Line(11) = {4,6};
Line(12) = {5,18};
Line(13) = {5,7};
Line(14) = {6,8};
Line(15) = {7,17};
Line(16) = {7,9};
Line(17) = {8,10};
Line(18) = {9,16};
Line(19) = {12,23};
Line(20) = {16,11};
Line(21) = {11,12};
Line(22) = {17,13};
Line(23) = {18,14};
Line(24) = {19,15};
Line(25) = {13,22};
Line(26) = {14,21};
Line(27) = {15,20};
Line(28) = {20,4};
Line(29) = {23,10};
Line(30) = {21,6};
Line(31) = {22, 8};

//Line loops
Line Loop(17) = {10, 12, 23, 26, 30, -11, -28, -27, -24, -9, -7};
Line Loop(18) = {13, 15, 22, 25, 31, -14, -30, -26, -23, -12};
Line Loop(19) = {16, 18, 20, 21, 19, 29, -17, -31, -25, -22, -15};

//Surfaces
Plane Surface(20) = 17;
Plane Surface(21) = 18;
Plane Surface(22) = 19;

