dens1 = 0.01;
dens2 = 0.0001;
//+
Point(1) = {0, 0, 0, dens1};
Point(2) = {0.05, 0, 0, dens1};
Point(3) = {0.40, 0, 0, dens1};
Point(4) = {0.43, 0, 0, dens2};
Point(5) = {0.45, 0, 0, dens2};
Point(6) = {0.55, 0, 0, dens2};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 5};
Line(5) = {5, 6};
Physical Line("bulk") = {1, 2, 3, 4, 5};
