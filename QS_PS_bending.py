import formulations as f
import solvers.solver as s
import fenics as fe
import numpy as np
import matplotlib.pyplot as plt


# --------------------
# Functions and classes
# --------------------
def left_point(x):
	return fe.near(x[0], l_off) and fe.near(x[1], 0.0)


def right_point(x):
	return fe.near(x[0], l_x - l_off) and fe.near(x[1], 0.0)


def left_load_point(x):
	return fe.near(x[0], l_x / 2 - l_0 / 2) and fe.near(x[1], l_y)


def right_load_point(x):
	return fe.near(x[0], l_x / 2 + l_0 / 2) and fe.near(x[1], l_y)


def symmetry_axis(x, on_boundary):
	return fe.near(x[0], l_x/2) and on_boundary


def prepare_mesh():
	nx = 110
	ny = 4

	l_sym = 0.5 * l_x
	mesh = fe.RectangleMesh(fe.Point(0., 0.), fe.Point(l_sym, l_y), nx, ny, diagonal="left/right")

	# fe.plot(mesh, "Mesh")
	# plt.show()

	# Refine
	markers = fe.MeshFunction("bool", mesh, 2)
	markers.set_all(False)
	for c in fe.cells(mesh):
		# Mark cells with facet midpoints near x == L/2
		for f in fe.facets(c):
			if fe.near(f.midpoint()[0], l_x/2., 0.5 * (l_0 + 4.0 * l_y)):
				markers[c] = True
	mesh = fe.refine(mesh, markers, redistribute=False)

	# fe.plot(mesh, "Mesh")
	# plt.show()

	# Refine again
	markers = fe.MeshFunction("bool", mesh, 2)
	markers.set_all(False)
	for c in fe.cells(mesh):
		# Mark cells with facet midpoints near x == L/2
		for f in fe.facets(c):
			if fe.near(f.midpoint()[0], l_x / 2., 0.5 * (l_0 + 2.0 * l_y)):
				markers[c] = True
	mesh = fe.refine(mesh, markers, redistribute=False)

	# Refine again
	markers = fe.MeshFunction("bool", mesh, 2)
	markers.set_all(False)
	for c in fe.cells(mesh):
		# Mark cells with facet midpoints near x == L/2
		for f in fe.facets(c):
			if fe.near(f.midpoint()[0], l_x / 2., 0.5 * (l_0 + 2.0 * l_y)):
				markers[c] = True
	mesh = fe.refine(mesh, markers, redistribute=False)

	# # Refine again
	# markers = fe.MeshFunction("bool", mesh, 2)
	# markers.set_all(False)
	# for c in fe.cells(mesh):
	# 	# Mark cells with facet midpoints near x == L/2
	# 	for f in fe.facets(c):
	# 		if fe.near(f.midpoint()[0], l_x / 2., 0.5 * (l_0 + 2.0 * l_y)):
	# 			markers[c] = True
	# mesh = fe.refine(mesh, markers, redistribute=False)

	return mesh


def init_bc(model):
	model.init_spaces(degr)
	V = model.V

	u_d = fe.Expression("-t/1000.0", t=0.0, degree=0)  # Prescribed displacement
	BC_u_1 = fe.DirichletBC(V.sub(1), 0.0, left_point, method="pointwise")
	BC_u_2 = fe.DirichletBC(V.sub(1), u_d, left_load_point, method="pointwise")
	BC_u_3 = fe.DirichletBC(V.sub(0), 0.0, symmetry_axis)
	BC_u = [BC_u_1, BC_u_2, BC_u_3]
	BC_d = []

	model.set_bc(BC_u, BC_d, u_d)


def solve(file, model):
	solution = f.PostProcess(file)
	ss = s.StaggeredScheme(model, time_space, solution)

	mon1 = f.Monitor(0.5*l_x, 0.0)
	ss.set_monitor([mon1])

	ss.solve(False)

	mon1.plot_monitor()
	solution.save_monitor(mon1)


def solve_imp(file, model):
	solution = f.PostProcess(file)
	ss = s.StaggeredScheme(model, time_space, solution)

	mon1 = f.Monitor(0.5*l_x, 0.0)
	ss.set_monitor(mon1)

	ss.set_d_init(d_init)
	ss.solve()

	mon1.plot_monitor()
	solution.save_monitor(mon1)


def calc_uniforms():
	model_pham = f.PSDamageModel(glass_p, mesh_unif, "el", "pham", "vd")
	model_bourdin = f.PSDamageModel(glass_b, mesh_unif, "el", "bourdin", "vd")
	model_stress = f.PSDamageModel(glass_p, mesh_unif, "el", "stress", "vd")

	init_bc(model_pham)
	init_bc(model_bourdin)
	init_bc(model_stress)

	solve("Solution_pham_PS_Unif", model_pham)
	solve("Solution_bourdin_PS_Unif", model_bourdin)
	solve("Solution_stress_PS_Unif", model_stress)


def calc_refined():
	model_pham = f.PSDamageModel(glass_p_ref, mesh_ref, "el", "pham", "vd")
	model_bourdin = f.PSDamageModel(glass_b_ref, mesh_ref, "sd", "bourdin", "sd")
	model_stress = f.PSDamageModel(glass_p_ref, mesh_ref, "el", "stress", "sd")

	init_bc(model_pham)
	init_bc(model_bourdin)
	init_bc(model_stress)

	solve("Solution_pham_PS_Ref", model_pham)
	#solve("Solution_bourdin_PS_Ref_sd_full", model_bourdin)
	#solve("Solution_stress_PS_Ref_sd_hyb", model_stress)


def calc_imp():
	model_pham = f.PSDamageModel(glass_p_ref, mesh_ref, "el", "pham", "sd")

	init_bc(model_pham)

	solve_imp("test", model_pham)


# Material parameters
E = 76.6e9  # Young's modulus
nu = 0.22  # Poisson ratio
lc = 0.005
lc_ref = 0.0015
ft = 60.0e6

Gc_b = lc * 256.0 / 27.0 * ft ** 2 / E
Gc_p = lc * 8.0 / 3.0 * ft ** 2 / E

Gc_b_ref = lc_ref * 256.0 / 27.0 * ft ** 2 / E
Gc_p_ref = lc_ref * 8.0 / 3.0 * ft ** 2 / E

glass_b = f.ElasticMaterial(E, nu, Gc_b, lc, ft)
glass_p = f.ElasticMaterial(E, nu, Gc_p, lc, ft)

glass_b_ref = f.ElasticMaterial(E, nu, Gc_b_ref, lc_ref, ft)
glass_p_ref = f.ElasticMaterial(E, nu, Gc_p_ref, lc_ref, ft)

# Geometry parameters
l_x, l_y = 1.1, 0.02  # Length and thickness of glass beam
n_x, n_y = 275, 10  # Number of elements for uniform mesh
l_off = 0.05  # Support offset
l_0 = 0.2  # Pitch of load points

degr = 1
d_init = fe.Expression("0.0 + 0.01*(near(x[0], 0.5, 0.0001) and near(x[1], 0.0, 0.005))", degree=1)

# Time parameters - TODO: change time space
t_start = 0.1
#t_middle = 7.2
t_middle = 7.0
t_end = 8.0
#t_end = 8.0
dt_1 = 0.1
dt_2 = 0.01
t_steps_1 = int((t_middle-t_start)/dt_1)
t_steps_2 = int((t_end-t_middle)/dt_2)

time_space_1 = np.linspace(t_start, t_middle, t_steps_1)
time_space_2 = np.linspace(t_middle + dt_1, t_end, t_steps_2)

time_space = np.concatenate((time_space_1, time_space_2))
#time_space = np.linspace(t_start, t_end, t_steps_1)

# --------------------
# Define geometry
# --------------------
mesh = fe.Mesh("Meshes/QS_4PB_testing.xml")
mesh_unif = fe.RectangleMesh(fe.Point(0., 0.), fe.Point(0.5*l_x, l_y), n_x, n_y, "left/right")
mesh_ref = prepare_mesh()

hmin = mesh_ref.hmin()
print(hmin)

#plt.style.use("classic")
fe.plot(mesh_ref)
plt.figure()

fe.plot(fe.project(d_init, fe.FunctionSpace(mesh_ref, "DG", 0)))
plt.show()

#xdmf_mesh_unif = fe.XDMFFile("Meshes/QS_4PB_PS_unif.xdmf")
#xdmf_mesh_ref = fe.XDMFFile("Meshes/QS_4PB_PS_ref.xdmf")

#xdmf_mesh_unif.write(mesh_unif)
#xdmf_mesh_ref.write(mesh_ref)

# calc_uniforms()
calc_refined()
#calc_imp()





# Create model with pham/bourdin/stress formulation with material glass_p
# 3rd argument: type of displacement equation
# 4th argument: type of damage model
# 5th argument: type of decomposition for damage calculation
#model_pham = f.PSDamageModel(glass_p, mesh, "el", "pham", "vd")
#model_bourdin = f.PSDamageModel(glass_p, mesh, "el", "bourdin", "vd")
#model_stress = f.PSDamageModel(glass_p, mesh, "el", "stress", "vd")

# Init spaces - for BC definition
#model_pham.init_spaces()
#V = model_pham.V

# --------------------
# Boundary conditions
# --------------------
#u_d = fe.Expression("-t/1000.0", t=0.0, degree=0)  # Prescribed displacement
#BC_u_1 = fe.DirichletBC(V.sub(1), 0.0, left_point, method="pointwise")
#BC_u_2 = fe.DirichletBC(V.sub(1), u_d, left_load_point, method="pointwise")
#BC_u_3 = fe.DirichletBC(V.sub(0), 0.0, symmetry_axis)
#BC_u = [BC_u_1, BC_u_2, BC_u_3]
#BC_d = []
#model_pham.set_bc(BC_u, BC_d, u_d)

# 1st argument of PostProcess object: Folder for solution
#solution_1 = f.PostProcess("Solution_pham_PS_Ref")
#ss = s.StaggeredScheme(model_pham, time_space, solution_1)
#solution_2 = f.PostProcess("Solution_bourdin_PS_Ref")
#ss = s.StaggeredScheme(model_pham, time_space, solution_2)

# One-point monitoring of sigma_x
#mon1 = f.Monitor(l_x/2, 0.0)
#ss.set_monitor(mon1)

# Staggered solver
#ss.solve()

# Plotting of monitor data
#mon1.plot_monitor()
#solution.save_monitor(mon1)
