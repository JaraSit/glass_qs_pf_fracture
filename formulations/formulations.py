import fenics as fe
import matplotlib.pyplot as plt
import numpy as np
import formulations.Functions as fu
import math


# --------------------------
# Material classes
# --------------------------
class ElasticMaterial:
	def __init__(self, E, nu, Gc, lc, ft, type="plane_stress", k_res=0.0):
		self.E = E  # Young's modulus
		self.nu = nu  # Poisson ration
		self.Gc = Gc  # Fracture toughness
		self.lc = lc  # Localization band width
		self.ft = ft  # Fracture strength
		self.lmbda = E*nu/(1 + nu)/(1 - 2*nu)  # Lame coefficients
		self.mu = E / 2 / (1 + nu)  # Lame coefficients
		if type == "plane_stress":
			self.lmbda = 2*self.mu*self.lmbda/(self.lmbda + 2*self.mu)  # Correction for plane stress
		self.kn = self.lmbda + self.mu  # Bulk modulus
		self.k = 5.0/6.0
		self.k_res = k_res

	def get_lame_cofs(self):
		return self.lmbda, self.mu

	def get_lame_cofs_d(self):
		return self.get_lame_cofs()

	def get_gc(self):
		return self.Gc

	def get_lc(self):
		return self.lc

	def get_ft(self):
		return self.ft

	def update(self, t_i):
		return 0


class ViscoElasticMaterial:
	def __init__(self, G_array, theta_array, G_inf, nu, c1, c2, T_ref, T_act, type="plane_stress"):
		self.G_array = G_array  # G_i series
		self.theta_array = theta_array  # Relaxation times series
		self.G_inf = G_inf  # long-term module
		self.nu = nu  # Poisson ration
		self.c1 = c1
		self.c2 = c2
		self.T_ref = T_ref
		self.T_act = T_act
		self.s_type = type

	def get_G(self, t_i):
		a_t = math.pow(10.0, -self.c1*(self.T_act-self.T_ref)/(self.c2 + self.T_act - self.T_ref))
		print(a_t)
		temp = self.G_inf
		n = len(self.G_array)
		temp += sum(self.G_array[k]*math.exp(-t_i/(a_t*self.theta_array[k])) for k in range(0, n))
		return temp


class LG3LMaterial:
	def __init__(self, glass_mat, foil_mat, hs, type="t/2", k_res=0.0):
		self.glass_mat = glass_mat
		self.foil_mat = foil_mat
		self.hs = hs
		self.init_lmbda_mu()
		self.type = type
		self.k_res = k_res

	def init_lmbda_mu(self):
		glass_lambda = self.glass_mat.lmbda
		foil_lambda = 0.0001*glass_lambda
		glass_mu = self.glass_mat.mu
		foil_mu = 0.0001*glass_mu
		self.lmbda = fe.Expression("abs(x[1]-H1-0.5*H2) < 0.5*H2 ? foil : glass", H1=self.hs[0], H2=self.hs[1], foil=foil_lambda, glass=glass_lambda, t=1.0, degree=0)
		self.mu = fe.Expression("abs(x[1]-H1-0.5*H2) < 0.5*H2 ? foil : glass", H1=self.hs[0], H2=self.hs[1], foil=foil_mu, glass=glass_mu, t=1.0, degree=0)
		self.lmbda_d = fe.Expression("abs(x[1]-H1-0.5*H2) < 0.5*H2 ? foil : glass", H1=self.hs[0], H2=self.hs[1], foil=0.0, glass=glass_lambda, t=1.0, degree=0)
		self.mu_d = fe.Expression("abs(x[1]-H1-0.5*H2) < 0.5*H2 ? foil : glass", H1=self.hs[0], H2=self.hs[1], foil=0.0, glass=glass_mu, t=1.0, degree=0)
		self.E_foil = fe.Expression("foil", foil=foil_mu, degree=0)
		self.G_foil = fe.Expression("foil", foil=foil_mu, degree=0)
		self.lmbda_foil = fe.Expression("foil", foil=foil_lambda, degree=0)

	def get_lame_cofs(self):
		return self.lmbda, self.mu

	def get_lame_cofs_d(self):
		return self.lmbda_d, self.mu_d

	def get_lmbdas(self):
		return [self.glass_mat.lmbda, self.lmbda_foil, self.glass_mat.lmbda]

	def get_gc(self):
		gc = self.glass_mat.Gc
		return fe.Expression("abs(x[1]-H1-0.5*H2) < 0.5*H2 ? foil : glass", H1=self.hs[0], H2=self.hs[1], foil=gc*1000.0, glass=gc, t=1.0, degree=0)

	def get_lc(self):
		return self.glass_mat.lc

	def get_ft(self):
		return self.glass_mat.ft

	def update(self, t_i):
		# lmbda_foil = 0.0
		# G_foil = 0.0
		if self.type == "t/2":
			G_foil = self.foil_mat.get_G(t_i*60.0/1.8/2)
			nu_foil = self.foil_mat.nu
			lmbda_foil = 2*G_foil*nu_foil/(1.0-2.0*nu_foil)
			if self.foil_mat.s_type == "plane_stress":
				lmbda_foil = 2*G_foil*lmbda_foil/(lmbda_foil + 2*G_foil)
			E_foil = 2*G_foil*(1.0 + nu_foil)
		elif self.type == "t":
			G_foil = self.foil_mat.get_G(t_i*60.0/1.8)
			nu_foil = self.foil_mat.nu
			lmbda_foil = 2*G_foil*nu_foil/(1.0-2.0*nu_foil)
			if self.foil_mat.s_type == "plane_stress":
				lmbda_foil = 2*G_foil*lmbda_foil/(lmbda_foil + 2*G_foil)
			E_foil = 2*G_foil*(1.0 + nu_foil)
		else:
			raise Exception("Type of pseudo-viscoelasticity must be t/2!")
		self.lmbda.foil = lmbda_foil
		self.mu.foil = G_foil
		self.G_foil.foil = G_foil
		self.E_foil.foil = E_foil
		self.lmbda_foil.foil = lmbda_foil

	def get_hs(self):
		return self.hs

	def get_Es(self):
		return [self.glass_mat.E, self.E_foil, self.glass_mat.E]

	def get_Gs(self):
		return [self.glass_mat.mu, self.G_foil, self.glass_mat.mu]

	def get_nus(self):
		return [self.glass_mat.nu, self.foil_mat.nu, self.glass_mat.nu]


# --------------------------
# Cross section class
# --------------------------
class CrossSection:
	def __init__(self, b, h):
		self.b = b
		self.h = h
		self.A = b*h
		self.I = 1.0/12.0*b*h**3


# --------------------------
# Class for post-processing
# --------------------------
class PostProcess:
	def __init__(self, folder):
		self.folder = folder
		self.files = []

	def init_files(self, file_names):
		for i in range(len(file_names)):
			self.files.append(fe.XDMFFile(self.folder + "/" + file_names[i]))
			self.files[i].parameters["flush_output"] = True
		# self.file_u = fe.XDMFFile(self.folder + "/displacements.xdmf")  # XDMF file for displacements
		# self.file_d = fe.XDMFFile(self.folder + "/damage.xdmf")  # XDMF file for damage variable

	# WILL BE DEPRECIATED
	def save_u_d_xdmf(self, u, d, t):
		print("Function save_u_d_xdmf will be depreciated.")
		self.file_u.write(u, t)
		self.file_d.write(d, t)

	def save_to(self, func, ind, t):
		self.files[ind].write(func, t)

	def close(self):
		for files_i in self.files:
			files_i.close()

	# WILL BE DEPRECIATED
	# def close(self):
	# 	self.file_u.close()
	# 	self.file_d.close()

	def save_monitor(self, mon):
		np.savetxt(self.folder + "/stress_data.txt", np.column_stack((mon.mon_u_d, mon.mon_strain_x, mon.mon_strain_y, mon.mon_stress_x, mon.mon_stress_y)))

	def save_monitors(self, mons):
		print(mons[0].mon_u_d)
		temp = np.vstack(mons[0].mon_u_d)
		print(temp)
		for mons_i in mons:
			temp = np.concatenate((temp, np.array([mons_i.mon_strain_x]).T), axis=1)
			temp = np.concatenate((temp, np.array([mons_i.mon_strain_y]).T), axis=1)
			temp = np.concatenate((temp, np.array([mons_i.mon_stress_x]).T), axis=1)
			temp = np.concatenate((temp, np.array([mons_i.mon_stress_x]).T), axis=1)
		np.savetxt(self.folder + "/stress_data.txt", temp)

	def save_monitor_r(self, mon_r):
		np.savetxt(self.folder + "/reaction_data.txt", np.column_stack((mon_r.mon_u_d, mon_r.mon_reaction)))


# --------------------------
# Auxiliary structure for monitoring
# --------------------------
class Monitor:
	def __init__(self, m_x, m_y):
		self.m_x = m_x
		self.m_y = m_y
		self.mon_u_d = []
		self.mon_stress_x = []
		self.mon_stress_y = []
		self.mon_strain_x = []
		self.mon_strain_y = []

	def append_data(self, form, u_i, d_i):
		self.mon_u_d.append(form.u_d(self.m_x, self.m_y))
		sr_x, sr_y, str_x, str_y = form.get_strain_and_stress(self.m_x, self.m_y, u_i, d_i)
		#self.mon_stress.append(form.get_stress(self.m_x, self.m_y, u_i, d_i))
		self.mon_strain_x.append(sr_x)
		self.mon_strain_y.append(sr_y)
		self.mon_stress_x.append(str_x)
		self.mon_stress_y.append(str_y)

	# def append_data_2(self, u_d_i, stress_i):
	# 	self.mon_u_d.append(u_d_i(self.m_x, self.m_y))
	# 	self.mon_stress.append(stress_i(self.m_x, self.m_y)[0])

	def plot_monitor(self):
		xx = np.array(self.mon_u_d)
		yy = np.array(self.mon_stress_x)
		plt.style.use("classic")
		plt.plot(abs(xx*1.0e3), yy/1.0e6)
		plt.xlabel("Prescribed displacement [mm]")
		plt.ylabel("Stress [MPa]")
		plt.show()


# --------------------------
# Auxiliary structure for reactions monitoring (ugly version)
# TODO: Somehow upgrade or change Monitor and Monitor_reaction classes
# --------------------------
class MonitorReaction:
	def __init__(self, selected_dof):
		self.s_dof = selected_dof
		self.mon_u_d = []
		self.mon_reaction = []

	def append_data(self, form, u_form, u_i, d_i):
		self.mon_u_d.append(form.u_d(0.0, 0.0))
		forces = fe.assemble(u_form)
		self.mon_reaction.append(forces[self.s_dof])


class MonitorReactions:
	def __init__(self, selected_dofs):
		self.s_dofs = selected_dofs
		self.mon_u_d = []
		self.mon_reaction = []

	def append_data(self, form, u_form, u_i, d_i):
		self.mon_u_d.append(form.u_d(0.0, 0.0))
		forces = fe.assemble(u_form)
		react = 0.0
		for dofs in self.s_dofs:
			react += forces[dofs]
		self.mon_reaction.append(react)


# --------------------------
# Plane stress damage model
# --------------------------
class PSDamageModel:
	# PSDamageModel Constructor
	def __init__(self, material, mesh, u_type, d_type, dec_type):
		self.mat = material
		self.mesh = mesh
		self.V = None
		self.V0 = None
		self.DS = None
		self.W = None
		self.bc_u = []
		self.bc_d = []
		self.u_d = None
		self.u_type = u_type  # Type of form for displacement calculation
		self.d_type = d_type  # Type of form for damage calculation
		self.dec_type = dec_type  # Type of stress decomposition for damage calculation

	# Define spaces
	def init_spaces(self, degr):
		self.V = fe.VectorFunctionSpace(self.mesh, "CG", degr)  # Function space for displacements
		self.V0 = fe.TensorFunctionSpace(self.mesh, "DG", degr-1)  # Function space for stress components
		self.DS = fe.FunctionSpace(self.mesh, "DG", degr-1)  # Function space for history variable
		#self.W = fe.FunctionSpace(self.mesh, "CG", degr)  # Function space for damage variable
		self.W = fe.FunctionSpace(self.mesh, "CG", degr)

	# Set Dirichlet boundary conditions and parameter function u_d
	def set_bc(self, bc_u, bc_d, u_d):
		self.bc_u = bc_u
		self.bc_d = bc_d
		self.u_d = u_d

	# Update time-like parameter in u_d function
	def time_step_update(self, t):
		self.u_d.t = t
		self.mat.update(t)

	# Return corresponding bilinear form for displacements
	def get_u_form(self, u, d):
		u_tr = fe.TrialFunction(self.V)
		u_test = fe.TestFunction(self.V)
		lmbda, mu = self.mat.get_lame_cofs()
		if self.u_type == "el":
			return (1-d)**2*fe.inner(fu.sigma_el(u, lmbda, mu), fu.eps(u_test))*fe.dx + fe.dot(fe.Constant((0.0, 0.0)), u_test)*fe.dx, "nlin"
		elif self.u_type == "vd":
			return (1-d)**2*fe.inner(fu.sigma_p_vd(u, lmbda, mu), fu.eps(u_test))*fe.dx + fe.inner(fu.sigma_n_vd(u, lmbda, mu), fu.eps(u_test))*fe.dx, "nlin"
		elif self.u_type == "sd":
			return (1-d)**2*fe.inner(fu.sigma_p_sd(u, lmbda, mu), fu.eps(u_test))*fe.dx + fe.inner(fu.sigma_n_sd(u, lmbda, mu), fu.eps(u_test))*fe.dx, "nlin"
		else:
			raise Exception("Type of displacement form must be el/vd/sd!")

	def get_stress_tensor(self, u_i, d_i):
		lmbda, mu = self.mat.get_lame_cofs()
		return fu.local_project((1 - d_i)**2*self.get_sigma_active(u_i, lmbda, mu) + self.get_sigma_pasive(u_i, lmbda, mu), self.V0)

	# Return active part of stress tensor
	def get_sigma_active(self, u_i, lmbda_i, mu_i):
		if self.dec_type == "el":
			return fu.sigma_el(u_i, lmbda_i, mu_i)
		elif self.dec_type == "vd":
			return fu.sigma_p_vd(u_i, lmbda_i, mu_i)
		elif self.dec_type == "sd":
			return fu.sigma_p_sd(u_i, lmbda_i, mu_i)
		else:
			raise Exception("Type of decomposition must be el/vd!")

	# Return pasive part of stress tensor
	def get_sigma_pasive(self, u_i, lmbda_i, mu_i):
		if self.dec_type == "el":
			return 0
		elif self.dec_type == "vd":
			return fu.sigma_n_vd(u_i, lmbda_i, mu_i)
		elif self.dec_type == "sd":
			return fu.sigma_n_sd(u_i, lmbda_i, mu_i)
		else:
			raise Exception("Type of decomposition must be el/vd!")

	# Return bilinear formulation for damage
	def get_d_form(self, u, d):
		d_tr = fe.TrialFunction(self.W)
		d_test = fe.TestFunction(self.W)
		#gc = self.mat.Gc
		#lc = self.mat.lc
		#ft = self.mat.ft
		gc = self.mat.get_gc()
		lc = self.mat.get_lc()
		ft = self.mat.get_ft()
		lmbda, mu = self.mat.get_lame_cofs()
		if self.d_type == "pham":
			E_ds = -fe.inner(self.get_sigma_active(u, lmbda, mu), fu.eps(u))*fe.inner(1.0 - d, d_test)*fe.dx
			E_ds += 3.0/8.0*gc*(1.0/lc*d_test + 2*lc*fe.inner(fe.grad(d), fe.grad(d_test)))*fe.dx
		elif self.d_type == "bourdin":
			E_ds = -fe.inner(self.get_sigma_active(u, lmbda, mu), fu.eps(u))*fe.inner(1.0 - d, d_test)*fe.dx
			E_ds += gc*(1.0/lc*fe.inner(d, d_test) + lc*fe.inner(fe.grad(d), fe.grad(d_test)))*fe.dx
		elif self.d_type == "stress":
			stress = fu.sigma_el(u, self.mat)
			sg00, sg01, sg10, sg11 = fu.eig_v(stress)
			D = fu.mc_bracket((fu.mc_bracket(sg00) /ft)**2 + (fu.mc_bracket(sg11)/ft)**2 - 1)
			E_ds = -D*fe.inner(1.0 - d, d_test)*fe.dx
			E_ds += (fe.inner(d, d_test) + lc**2*fe.inner(fe.grad(d), fe.grad(d_test))) * fe.dx
		else:
			raise Exception("Damage type must be pham/bourdin/stress!")
		return E_ds

	def init_files(self, files):
		files.init_files(["displ_u.xdmf", "damage.xdmf"])

	def save_u(self, files, x, t):
		files.save_to(x, 0, t)

	def save_d(self, files, d, t):
		files.save_to(d, 1, t)

	# Unused?
	def get_stress(self, m_x, m_y, u_i, d_i):
		stress = self.get_stress_tensor(u_i, d_i)
		return stress(m_x, m_y)[0]

	def get_strain_and_stress(self, m_x, m_y, u_i, d_i):
		strain = fu.local_project(fu.eps(u_i), self.V0)
		stress = self.get_stress_tensor(u_i, d_i)
		return strain(m_x, m_y)[0], 0.0, stress(m_x, m_y)[0], 0.0

	def get_d_max(self):
		return fe.interpolate(fe.Constant(1.0), self.W)


# --------------------------
# Beam damage model
# --------------------------
class BeamDamageModel:
	# PSDamageModel Constructor
	def __init__(self, material, cross, mesh, u_type, d_type, dec_type, num_int):
		self.mat = material
		self.cross = cross
		self.mesh = mesh
		self.V = None
		self.V0 = None
		self.W = None
		self.bc_u = []
		self.bc_d = []
		self.u_d = None
		self.u_type = u_type  # Type of form for displacement calculation
		self.d_type = d_type  # Type of form for damage calculation
		self.dec_type = dec_type  # Type of active energy
		self.num_int = num_int
		self.dh = abs(num_int[1] - num_int[0])

	# Define spaces
	def init_spaces(self):
		# p1 = fe.FiniteElement('P', fe.interval, 2)  # Space of quadratic polynomial functions (to overcome locking)
		p1 = fe.FiniteElement('P', fe.interval, 1)  # Space of linear polynomial functions
		element = fe.MixedElement([p1, p1, p1])  # Element of mixed space
		self.V = fe.FunctionSpace(self.mesh, element)  # Mixed space
		self.V0 = fe.FunctionSpace(self.mesh, 'DG', 0)
		self.W = fe.FunctionSpace(self.mesh, 'P', 1)  # Space of linear polynomial functions

	# Set Dirichlet boundary conditions and parameter function u_d
	def set_bc(self, bc_u, bc_d, u_d):
		self.bc_u = bc_u
		self.bc_d = bc_d
		self.u_d = u_d

	# Update time-like parameter in u_d function
	def time_step_update(self, t):
		self.u_d.t = t

	# Return corresponding bilinear form for displacements using numerical integration
	def get_u_form(self, x, d):
		# Selective integration for shearlock reduction
		dx_shear = fe.dx(metadata={"quadrature_degree": 0})
		u_test, w_test, phi_test = fe.TestFunctions(self.V)

		u_form = 0.0
		type_sol = "lin"

		b = self.cross.b

		if self.u_type == "num_int":
			u_, w_, phi_ = fe.split(x)
			for i in range(len(self.num_int) - 1):
				gauss = 0.5*(self.num_int[i] + self.num_int[i + 1])
				eps_z = u_.dx(0) + phi_.dx(0)*gauss
				d_eps_z = u_test.dx(0) + phi_test.dx(0)*gauss
				eps_z_p = fu.mc_bracket(eps_z)
				eps_z_n = -fu.mc_bracket(-eps_z)
				u_form += fe.inner((1.0-d)**2*self.mat.E*eps_z_p + self.mat.E*eps_z_n, d_eps_z)*self.dh*b*fe.dx
			u_form += w_test.dx(0)*self.mat.mu*self.cross.A*(w_.dx(0) + phi_)*dx_shear
			u_form += phi_test*self.mat.mu*self.cross.A*(phi_ + w_.dx(0))*dx_shear
			u_form -= fe.Constant(0.0)*w_test*fe.dx
			type_sol = "nlin"
		elif self.u_type == "analytical":
			u_, w_, phi_ = fe.TrialFunctions(self.V)

			E = self.mat.E
			h = self.cross.h
			zN = -0.5 * h * (d / (d - 2.0))
			gm = (1 - d) ** 2
			gp = fe.Constant(1.0)

			t1 = gm * (zN + 0.5 * h) + gp * (0.5 * h - zN)
			t2 = gm * (0.5 * zN ** 2 - 0.125 * h ** 2) + gp * (0.125 * h ** 2 - 0.5 * zN ** 2)
			t3 = gm * (zN ** 3 / 3.0 + h ** 3 / 24.0) + gp * (h ** 3 / 24.0 - zN ** 3 / 3.0)

			u_form += u_test.dx(0) * E * t1 * b * u_.dx(0) * fe.dx + u_test.dx(0) * E * t2 * b * phi_.dx(0) * fe.dx
			u_form += phi_test.dx(0) * E * t2 * b * u_.dx(0) * fe.dx
			u_form += phi_test.dx(0) * E * t3 * b * phi_.dx(0) * fe.dx
			# u_form += phi_test.dx(0)*E*self.cross.I*phi_.dx(0)*fe.dx

			u_form += w_test.dx(0) * self.mat.mu * self.cross.A * (w_.dx(0) + phi_) * dx_shear
			u_form += phi_test * self.mat.mu * self.cross.A * (phi_ + w_.dx(0)) * dx_shear
			u_form -= fe.Constant(0.0) * w_test * fe.dx
			type_sol = "lin"
		else:
			raise Exception("Type of beam model must be num_int/analytical!")
		return u_form, type_sol

	# Return bilinear formulation for damage
	def get_d_form(self, u, d):
		d_tr = fe.TrialFunction(self.W)
		d_test = fe.TestFunction(self.W)
		gc = self.mat.Gc
		lc = self.mat.lc
		ft = self.mat.ft
		if self.d_type == "pham":
			print("pham")
			d_form = -2*self.get_energy_active(u, d)*fe.inner(1.0 - d, d_test)*fe.dx
			d_form += self.cross.A*3.0/8.0*gc*(1.0/lc*d_test + 2*lc*fe.inner(fe.grad(d), fe.grad(d_test)))*fe.dx
		elif self.d_type == "bourdin":
			d_form = -2*self.get_energy_active(u, d)*fe.inner(1.0 - d, d_test)*fe.dx
			d_form += self.cross.A*gc*(1.0/lc*fe.inner(d, d_test) + lc*fe.inner(fe.grad(d), fe.grad(d_test)))*fe.dx
		elif self.d_type == "stress":
			u_, w_, phi_ = fe.split(u)
			eps_i_1 = u_.dx(0) + phi_.dx(0)*0.5*self.cross.h
			eps_i_2 = u_.dx(0) - phi_.dx(0)*0.5*self.cross.h
			stress = fu.max_fce(self.mat.E*eps_i_1, self.mat.E*eps_i_2)
			# stress = sigma_el(u, self.mat)
			# sg00, sg01, sg10, sg11 = eig_v(stress)
			D = fu.mc_bracket((fu.mc_bracket(stress)/ft)**2 - 1)
			d_form = -D*fe.inner(1.0 - d, d_test)*fe.dx
			d_form += (fe.inner(d, d_test) + lc**2*fe.inner(fe.grad(d), fe.grad(d_test))) * fe.dx
		else:
			raise Exception("Damage type must be pham/bourdin/stress!")
		return d_form

	# Testing function - not used now
	def get_energy_active_2(self, x, d):
		u_, w_, phi_ = fe.split(x)
		eps_i_1 = u_.dx(0) + phi_.dx(0)*0.5*self.cross.h
		eps_i_2 = u_.dx(0) - phi_.dx(0)*0.5*self.cross.h
		eps_max = fu.max_fce(eps_i_1, eps_i_2)
		en = 0.5*fu.mc_bracket(eps_max)**2*self.mat.E*self.cross.h
		return en

	# Testing function - not used now
	def get_energy_active_3(self, x, d):
		u_, w_, phi_ = fe.split(x)
		en = 0.0
		for i in range(len(self.num_int) - 1):
			gauss = 0.5*(self.num_int[i] + self.num_int[i + 1])
			eps_z = u_.dx(0) + phi_.dx(0)*gauss
			eps_z_p = fu.mc_bracket(eps_z)
			temp = 0.5*self.mat.E*eps_z_p**2*self.dh
			en += temp
		return en

	def get_energy_active(self, x, d):
		u_, w_, phi_ = fe.split(x)
		en = 0.0

		if self.dec_type == "num_int":
			for i in range(len(self.num_int) - 1):
				gauss = 0.5 * (self.num_int[i] + self.num_int[i + 1])
				eps_z = u_.dx(0) + phi_.dx(0) * gauss
				eps_z_p = fu.mc_bracket(eps_z)
				temp = 0.5 * self.mat.E * eps_z_p ** 2 * self.dh
				en += temp
		elif self.dec_type == "surface":
			eps_i_1 = u_.dx(0) + phi_.dx(0) * 0.5 * self.cross.h
			eps_i_2 = u_.dx(0) - phi_.dx(0) * 0.5 * self.cross.h
			eps_max = fu.max_fce(eps_i_1, eps_i_2)
			en += 0.5 * fu.mc_bracket(eps_max) ** 2 * self.mat.E * self.cross.h * self.cross.b
		elif self.dec_type == "analytical":
			h = self.cross.h
			b = self.cross.b
			E = self.mat.E
			zN = -0.5*h*(d/(d - 2.0))

			t1 = zN + 0.5*h
			t2 = 0.5*zN**2 - 0.125*h**2
			t3 = zN**3/3.0 + h**3/24.0

			en += 0.5*E*t1*b*(u_.dx(0))**2
			en += E*t2*b*u_.dx(0)*phi_.dx(0)
			en += 0.5*E*t3*b*(phi_.dx(0))**2
		return en

	def init_files(self, files):
		files.init_files(["displ_u.xdmf", "displ_w.xdmf", "displ_phi.xdmf", "damage.xdmf"])

	def save_u(self, files, x, t):
		u_, w_, phi_ = x.split(deepcopy=True)
		files.save_to(u_, 0, t)
		files.save_to(w_, 1, t)
		files.save_to(phi_, 2, t)

	def save_d(self, files, d, t):
		files.save_to(d, 3, t)

	def get_stress(self, m_x, m_y, u_i, d_i):
		u_, w_, phi_ = u_i.split(deepcopy=True)
		eps_i = u_.dx(0) - phi_.dx(0)*0.5*self.cross.h
		stress = fu.local_project((1.0 - d_i)**2*self.mat.E*fu.mc_bracket(eps_i) - self.mat.E*fu.mc_bracket(-eps_i), self.V0)
		return stress(m_x)

	def get_strain_and_stress(self, m_x, m_y, u_i, d_i):
		u_, w_, phi_ = u_i.split(deepcopy=True)
		eps_i = u_.dx(0) - phi_.dx(0)*0.5*self.cross.h
		strain = fu.local_project(eps_i, self.V0)
		stress = fu.local_project((1.0 - d_i)**2*self.mat.E*fu.mc_bracket(eps_i) - self.mat.E*fu.mc_bracket(-eps_i), self.V0)
		return strain(m_x), 0.0, stress(m_x), 0.0

	def get_d_max(self):
		return fe.interpolate(fe.Constant(1.0), self.W)


# --------------------------
# Plate damage model
# --------------------------
# TODO: Rozmyslet znovu a poradne smyk
class PlateDamageModel:
	# PSDamageModel Constructor
	def __init__(self, material, cross, mesh, u_type, d_type, num_int):
		self.mat = material
		self.cross = cross
		self.mesh = mesh
		self.V = None
		self.V0 = None
		self.W = None
		self.bc_u = []
		self.bc_d = []
		self.u_d = None
		self.u_type = u_type  # Type of form for displacement calculation
		self.d_type = d_type  # Type of form for damage calculation
		self.num_int = num_int
		self.dh = abs(num_int[1] - num_int[0])

	# Define spaces
	def init_spaces(self):
		deg = 2
		element = fe.MixedElement([fe.VectorElement('P', self.mesh.ufl_cell(), deg),
								   fe.FiniteElement('P', self.mesh.ufl_cell(), deg),
								   fe.VectorElement('P', self.mesh.ufl_cell(), deg)])
		self.V = fe.FunctionSpace(self.mesh, element)
		self.V0 = fe.TensorFunctionSpace(self.mesh, 'DG', 0)
		self.W = fe.FunctionSpace(self.mesh, "CG", 1)

	# Set Dirichlet boundary conditions and parameter function u_d
	def set_bc(self, bc_u, bc_d, u_d):
		self.bc_u = bc_u
		self.bc_d = bc_d
		self.u_d = u_d

	# Update time-like parameter in u_d function
	def time_step_update(self, t):
		self.u_d.t = t

	# Return corresponding bilinear form for displacements using numerical integration
	def get_u_form(self, x, d):
		# Selective integration for shearlock reduction
		dx_shear = fe.dx(metadata={"quadrature_degree": 2})
		u_, w_, theta_ = fe.split(x)
		u_test, w_test, theta_test = fe.TestFunctions(self.V)
		S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])
		u_form = 0.0
		for i in range(len(self.num_int) - 1):
			gauss = 0.5*(self.num_int[i] + self.num_int[i + 1])
			u_z = u_ + S*theta_*gauss
			u_z_test = u_test + S*theta_test*gauss
			u_form += fe.inner(fu.sigma_vd(u_z, d, self.mat), fu.eps(u_z_test))*self.dh*fe.dx
		D2 = (self.mat.E*self.mat.k*self.cross.h)/(2.0*(1.0 + self.mat.nu))
		u_form += D2*fe.inner(fe.grad(w_) + S*theta_, fe.grad(w_test) + S*theta_test)*dx_shear
		u_form -= fe.Constant(0.0)*w_test*fe.dx
		return u_form, "nlin"

	# Return bilinear formulation for damage
	def get_d_form(self, u, d):
		d_tr = fe.TrialFunction(self.W)
		d_test = fe.TestFunction(self.W)
		gc = self.mat.Gc
		lc = self.mat.lc
		ft = self.mat.ft
		if self.d_type == "pham":
			print("pham")
			d_form = -2*self.get_energy_active(u, d)*fe.inner(1.0 - d, d_test)*fe.dx
			d_form += self.cross.h*3.0/8.0*gc*(1.0/lc*d_test + 2*lc*fe.inner(fe.grad(d), fe.grad(d_test)))*fe.dx
		elif self.d_type == "bourdin":
			d_form = -2*self.get_energy_active(u, d)*fe.inner(1.0 - d, d_test)*fe.dx
			d_form += self.cross.h*gc*(1.0/lc*fe.inner(d, d_test) + lc*fe.inner(fe.grad(d), fe.grad(d_test)))*fe.dx
		elif self.d_type == "stress":
			# TODO: This section is not right
			u_, w_, phi_ = fe.split(u)
			eps_i_1 = u_.dx(0) + phi_.dx(0)*0.5*self.cross.h
			eps_i_2 = u_.dx(0) - phi_.dx(0)*0.5*self.cross.h
			stress = fu.max_fce(self.mat.E*eps_i_1, self.mat.E*eps_i_2)
			# stress = sigma_el(u, self.mat)
			# sg00, sg01, sg10, sg11 = eig_v(stress)
			D = fu.mc_bracket((fu.mc_bracket(stress)/ft)**2 - 1)
			d_form = -D*fe.inner(1.0 - d, d_test)*fe.dx
			d_form += (fe.inner(d, d_test) + lc**2*fe.inner(fe.grad(d), fe.grad(d_test))) * fe.dx
		else:
			raise Exception("Damage type must be pham/bourdin/stress!")
		return d_form

	def get_energy_active(self, x, d):
		u_, w_, theta_ = fe.split(x)
		S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

		u_z_l = u_ + S*theta_*0.5*self.cross.h
		u_z_u = u_ - S*theta_*0.5*self.cross.h
		en = fu.max_fce(fu.psi_p_vd(u_z_l, self.mat)*self.cross.h, fu.psi_p_vd(u_z_u, self.mat)*self.cross.h)

		return en

	def get_energy_active_avrg(self, x, d):
		u_, w_, theta_ = fe.split(x)
		S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])
		en = 0.0
		for i in range(len(self.num_int) - 1):
			gauss = 0.5*(self.num_int[i] + self.num_int[i + 1])
			u_z = u_ + S*theta_*gauss
			en += fu.psi_p_vd(u_z, self.mat)*self.dh
			#eps_z = u_.dx(0) + phi_.dx(0)*gauss
			#eps_z_p = fu.mc_bracket(eps_z)
			#temp = 0.5*self.mat.E*eps_z_p**2*self.dh
			#en += temp
		return en

	def init_files(self, files):
		files.init_files(["displ_u.xdmf", "displ_w.xdmf", "displ_phi.xdmf", "damage.xdmf"])

	def save_u(self, files, x, t):
		u_, w_, theta_ = x.split(deepcopy=True)
		files.save_to(u_, 0, t)
		files.save_to(w_, 1, t)
		files.save_to(theta_, 2, t)

	def save_d(self, files, d, t):
		files.save_to(d, 3, t)

	def get_strain_and_stress(self, m_x, m_y, u_i, d_i):
		S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])
		u_, w_, theta_ = u_i.split(deepcopy=True)
		eps_i = fe.grad(u_) - S*fe.grad(theta_)*0.5*self.cross.h
		u_local_i = u_ - S*theta_*0.5*self.cross.h
		strain = fu.local_project(eps_i, self.V0)
		stress = fu.local_project(fu.sigma_vd(u_local_i, d_i, self.mat), self.V0)
		return strain(m_x, m_y)[0], 0.0, stress(m_x, m_y)[0], 0.0
