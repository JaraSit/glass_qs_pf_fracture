import fenics as fe
import matplotlib.pyplot as plt
import numpy as np
import formulations.Functions as fu
import math


# --------------------------------------
# Layered Mindlin beam model with damage
# --------------------------------------
class BeamDamageModel:
	# Layered beam DamageModel Constructor
	def __init__(self, material, cross, mesh, u_type, d_type, num_int, lays_num, glass_num):
		self.mat = material
		self.cross = cross
		self.mesh = mesh
		self.V = None
		self.V0 = None
		self.W = None
		self.bc_u = []
		self.bc_d = []
		self.u_d = None
		self.u_type = u_type  # Type of form for displacement calculation
		self.d_type = d_type  # Type of form for damage calculation
		self.num_int = num_int
		self.n_ni = len(num_int)
		self.dh = abs(num_int[1] - num_int[0])
		self.lays_num = lays_num
		self.glass_num = glass_num

	# Define spaces
	def init_spaces(self):
		p1 = fe.FiniteElement('P', fe.interval, 1)  # Space of linear polynomial functions
		elems_num = 5*self.lays_num - 2
		element = fe.MixedElement(elems_num*[p1])  # Element of mixed space
		self.V = fe.FunctionSpace(self.mesh, element)  # Mixed space
		self.V0 = fe.FunctionSpace(self.mesh, 'DG', 0)
		element_d = fe.MixedElement(self.glass_num*[p1])
		self.W = fe.FunctionSpace(self.mesh, element_d)
		#self.W = fe.FunctionSpace(self.mesh, 'P', 1)  # Space of linear polynomial functions

	# Set Dirichlet boundary conditions and parameter function u_d
	def set_bc(self, bc_u, bc_d, u_d):
		self.bc_u = bc_u
		self.bc_d = bc_d
		self.u_d = u_d

	# Update time-like parameter in u_d function
	def time_step_update(self, t):
		self.u_d.t = t
		self.mat.update(t)

	# Test version of analytical u_form (without damage)
	def get_u_form_a(self, x, d):
		# Selective integration for shearlock reduction
		dx_shear = fe.dx(metadata={"quadrature_degree": 0})

		lg_trial = fe.TrialFunctions(self.V)
		lg_test = fe.TestFunctions(self.V)

		u_form = 0.0

		E = self.mat.get_Es()
		G = self.mat.get_Gs()
		h = self.mat.get_hs()
		b = self.cross.b
		A = [b * hi for hi in h]
		I = [1.0/12.0*b*hi**3 for hi in h]

		for i in range(self.lays_num):

			ind = 3*i
			ind_max = 3*self.lays_num

			u_tr, w_tr, phi_tr = lg_trial[ind], lg_trial[ind + 1], lg_trial[ind + 2]
			u_test, w_test, phi_test = lg_test[ind], lg_test[ind + 1], lg_test[ind + 2]

			u_form += u_test.dx(0)*E[i]*A[i]*u_tr.dx(0)*fe.dx
			u_form += phi_test.dx(0)*E[i]*I[i]*phi_tr.dx(0)*fe.dx

			u_form += (w_test.dx(0) + phi_test)*G[i]*A[i]*(w_tr.dx(0) + phi_tr)*dx_shear
			u_form -= fe.Constant(0.0)*w_test*fe.dx

			if i < (self.lays_num - 1):
				lmbd_1_tr, lmbd_2_tr = lg_trial[ind_max + 2*i], lg_trial[ind_max + 2*i + 1]
				lmbd_1_test, lmbd_2_test = lg_test[ind_max + 2*i], lg_test[ind_max + 2*i + 1]
				ind_next = 3*(i + 1)
				u_tr_next, w_tr_next, phi_tr_next = lg_trial[ind_next], lg_trial[ind_next + 1], lg_trial[ind_next + 2]
				u_test_next, w_test_next, phi_test_next = lg_test[ind_next], lg_test[ind_next + 1], lg_test[ind_next + 2]

				u_form += lmbd_1_tr*(u_test + 0.5*h[i]*phi_test - u_test_next + 0.5*h[i + 1]*phi_test_next)*fe.dx
				u_form += lmbd_1_test*(u_tr + 0.5*h[i]*phi_tr - u_tr_next + 0.5*h[i + 1]*phi_tr_next)*fe.dx
				u_form += lmbd_2_tr*(w_test - w_test_next)*fe.dx + lmbd_2_test*(w_tr - w_tr_next)*fe.dx

		return u_form, "lin"

	# u_form with numerical integration
	def get_u_form(self, x, d):
		# Selective integration for shearlock reduction
		dx_shear = fe.dx(metadata={"quadrature_degree": 0})

		lg_trial = fe.split(x)
		lg_test = fe.TestFunctions(self.V)

		u_form = 0.0

		E = self.mat.get_Es()
		G = self.mat.get_Gs()
		h = self.mat.get_hs()
		b = self.cross.b
		A = [b*hi for hi in h]
		I = [1.0/12.0*b*hi**3 for hi in h]

		glass_ind = 0

		for i in range(self.lays_num):

			ind = 3 * i
			ind_max = 3 * self.lays_num

			u_tr, w_tr, phi_tr = lg_trial[ind], lg_trial[ind + 1], lg_trial[ind + 2]
			u_test, w_test, phi_test = lg_test[ind], lg_test[ind + 1], lg_test[ind + 2]

			if i%2 == 0:
				hs = np.linspace(-0.5*h[i], 0.5*h[i], self.n_ni)
				dh = abs(hs[1] - hs[0])
				for j in range(len(hs) - 1):
					gauss = 0.5*(hs[j] + hs[j + 1])
					eps_z = u_tr.dx(0) + phi_tr.dx(0)*gauss
					d_eps_z = u_test.dx(0) + phi_test.dx(0)*gauss
					eps_z_p = fu.mc_bracket(eps_z)
					eps_z_n = -fu.mc_bracket(-eps_z)
					u_form += fe.inner((1.0-d[glass_ind])**2*E[i]*eps_z_p + E[i]*eps_z_n, d_eps_z)*dh*b*fe.dx
				glass_ind += 1
			else:
				u_form += u_test.dx(0)*E[i]*A[i]*u_tr.dx(0)*fe.dx
				u_form += phi_test.dx(0)*E[i]*I[i]*phi_tr.dx(0)*fe.dx

			u_form += (w_test.dx(0) + phi_test)*G[i]*A[i]*(w_tr.dx(0) + phi_tr)*dx_shear
			u_form -= fe.Constant(0.0)*w_test*fe.dx

			if i < (self.lays_num - 1):
				lmbd_1_tr, lmbd_2_tr = lg_trial[ind_max + 2*i], lg_trial[ind_max + 2*i + 1]
				lmbd_1_test, lmbd_2_test = lg_test[ind_max + 2*i], lg_test[ind_max + 2*i + 1]
				ind_next = 3*(i + 1)
				u_tr_next, w_tr_next, phi_tr_next = lg_trial[ind_next], lg_trial[ind_next + 1], lg_trial[ind_next + 2]
				u_test_next, w_test_next, phi_test_next = lg_test[ind_next], lg_test[ind_next + 1], lg_test[ind_next + 2]

				u_form += lmbd_1_tr*(u_test + 0.5*h[i]*phi_test - u_test_next + 0.5*h[i + 1]*phi_test_next)*fe.dx
				u_form += lmbd_1_test*(u_tr + 0.5*h[i]*phi_tr - u_tr_next + 0.5*h[i + 1]*phi_tr_next)*fe.dx
				u_form += lmbd_2_tr*(w_test - w_test_next)*fe.dx + lmbd_2_test*(w_tr - w_tr_next)*fe.dx

		return u_form, "nlin"

	# Return formulation for damage
	def get_d_form(self, u, d):
		d_i = fe.split(d)
		#d_tr = fe.TrialFunctions(self.W)
		d_test = fe.TestFunctions(self.W)
		gc = self.mat.glass_mat.Gc
		lc = self.mat.glass_mat.lc
		ft = self.mat.glass_mat.ft
		# TODO: make it better
		h = [self.mat.get_hs()[0], self.mat.get_hs()[2]]
		b = self.cross.b

		d_form = 0.0
		for i in range(self.glass_num):
			d_form += -2*self.get_energy_active(u, d, 2*i)*fe.inner(1.0 - d_i[i], d_test[i])*fe.dx
			d_form += b*h[i]*3.0/8.0*gc*(1.0/lc*d_test[i] + 2*lc*fe.inner(fe.grad(d_i[i]), fe.grad(d_test[i])))*fe.dx

		return d_form

	def get_energy_active(self, x, d, l_num):
		lg_fce = fe.split(x)
		u_, w_, phi_ = lg_fce[3*l_num], lg_fce[3*l_num + 1], lg_fce[3*l_num + 2]
		h = self.mat.get_hs()[l_num]
		E = self.mat.get_Es()[l_num]
		b = self.cross.b
		eps_i_1 = u_.dx(0) + phi_.dx(0)*0.5*h
		eps_i_2 = u_.dx(0) - phi_.dx(0)*0.5*h
		eps_max = fu.max_fce(eps_i_1, eps_i_2)
		en = 0.5*fu.mc_bracket(eps_max)**2*E*h*b
		return en

	def init_files(self, files):
		files_i = []
		for i in range(self.lays_num):
			files_i.append("displ_u_" + str(i) + ".xdmf")
			files_i.append("displ_w_" + str(i) + ".xdmf")
			files_i.append("displ_phi_" + str(i) + ".xdmf")
			files_i.append("damage_" + str(i) + ".xdmf")
		files.init_files(files_i)

	def save_u(self, files, x, t):
		lg_displ = x.split(deepcopy=True)
		for i in range(self.lays_num):
			ind = 3*i
			ind_file = 4*i
			files.save_to(lg_displ[ind], ind_file, t)
			files.save_to(lg_displ[ind + 1], ind_file + 1, t)
			files.save_to(lg_displ[ind + 2], ind_file + 2, t)

	def save_d(self, files, d, t):
		lg_d = d.split(deepcopy=True)
		for i in range(self.glass_num):
			ind_file = 3 + 4*i
			files.save_to(lg_d[i], ind_file, t)

	# TODO: make it better. Temporary!!
	def get_strain_and_stress(self, m_x, m_y, u_i, d_i):
		E = self.mat.get_Es()
		hs = self.mat.get_hs()
		lg_displ = u_i.split(deepcopy=True)
		u_ = lg_displ[0]
		phi_ = lg_displ[2]
		eps_i = u_.dx(0) - phi_.dx(0)*0.5*hs[0]
		strain = fu.local_project(eps_i, self.V0)
		stress = fu.local_project(E[0]*eps_i, self.V0)
		#stress = fu.local_project((1.0 - d_i)**2*E[0]*fu.mc_bracket(eps_i) - E[0]*fu.mc_bracket(-eps_i), self.V0)
		return strain(m_x), 0.0, stress(m_x), 0.0

	def get_d_max(self):
		# TODO: make it better
		return fe.interpolate(fe.Constant((1.0, 1.0)), self.W)


def map_to_layers(fce_i, h_i):
	# w1, u1, phi1, u3, phi3, ...
	u_i = [None]*3
	w_i = [None]*3
	phi_i = [None]*3

	S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])
	S1 = fe.as_tensor([[0.0, -1.0], [1.0, 0.0]])
	w1_m, u1_m, p1_m, u3_m, p3_m = fe.split(fce_i)

	u_i[0] = u1_m
	u_i[2] = u3_m
	w_i[0] = w1_m
	w_i[1] = w1_m
	w_i[2] = w1_m
	phi_i[0] = p1_m
	phi_i[2] = p3_m
	u_i[1] = 0.25*h_i[0]*S*phi_i[0] - 0.25*h_i[2]*S*phi_i[2] + 0.5*u_i[2] + 0.5*u_i[0]
	phi_i[1] = S1*(-0.5*S*phi_i[0]*h_i[0]/h_i[1] - 0.5*S*phi_i[2]*h_i[2]/h_i[1] + u_i[2]/h_i[1] - u_i[0]/h_i[1])

	return w_i, u_i, phi_i


class PlateDamageModel:
	# PlateDamageModel Constructor
	# Only for three layered plates!!!
	def __init__(self, material, cross, mesh, u_type, d_type, num_int):
		self.mat = material
		self.cross = cross
		self.mesh = mesh
		self.V = None
		self.V0 = None
		self.W = None
		self.bc_u = []
		self.bc_d = []
		self.u_d = None
		self.u_type = u_type  # Type of form for displacement calculation
		self.d_type = d_type  # Type of form for damage calculation
		self.num_int = num_int
		self.n_ni = len(num_int)
		self.dh = abs(num_int[1] - num_int[0])

	# Define spaces
	def init_spaces(self):
		p1 = fe.FiniteElement("P", self.mesh.ufl_cell(), 1)
		v1 = fe.VectorElement("P", self.mesh.ufl_cell(), 1)
		element = fe.MixedElement([p1, v1, v1, v1, v1])
		self.V = fe.FunctionSpace(self.mesh, element)  # Mixed space
		self.V0 = fe.TensorFunctionSpace(self.mesh, 'DG', 0)
		element_d = fe.MixedElement(2*[p1])
		self.W = fe.FunctionSpace(self.mesh, element_d)

	# Set Dirichlet boundary conditions and parameter function u_d
	def set_bc(self, bc_u, bc_d, u_d):
		self.bc_u = bc_u
		self.bc_d = bc_d
		self.u_d = u_d

	# Update time-like parameter in u_d function
	def time_step_update(self, t):
		self.u_d.t = t
		self.mat.update(t)

	# u_form with numerical integration
	def get_u_form(self, x, d):
		# Selective integration for shearlock reduction
		dx_shear = fe.dx(metadata={"quadrature_degree": 0})

		lg_trial = fe.split(x)
		lg_test = fe.TestFunction(self.V)
		S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])
		d_ar = [d.sub(0), 0.0, d.sub(1)]

		#E = self.mat.get_Es()
		mu = self.mat.get_Gs()
		nu = self.mat.get_nus()
		h = self.mat.get_hs()
		lmbda = self.mat.get_lmbdas()

		#TODO: Check stiffnesses!!!
		E = [lmbda[i]*(1.0+nu[i])*(1.0-2.0*nu[i])/nu[i] for i in range(0,3)]

		w_tr, u_tr, phi_tr = map_to_layers(x, h)
		w_test, u_test, phi_test = map_to_layers(lg_test, h)

		u_form = 0.0

		for i in range(3):
			if i%2 == 0:
				hs = np.linspace(-0.5*h[i], 0.5*h[i], self.n_ni)
				dh = abs(hs[1] - hs[0])
				for j in range(len(hs) - 1):
					gauss = 0.5*(hs[j] + hs[j + 1])
					u_z = u_tr[i] + S*phi_tr[i]*gauss
					u_z_test = u_test[i] + S*phi_test[i]*gauss
					u_form += fe.inner(fu.sigma_vd(u_z, d_ar[i], lmbda[i], mu[i]), fu.eps(u_z_test))*dh*fe.dx
			else:
				D1 = (E[i]*h[i]**3)/(12.0*(1.0 - nu[i]**2))
				D3 = (E[i]*h[i])/(1.0 - nu[i]**2)
				kappa, kappa_test = fu.eps(S*phi_tr[i]), fu.eps(S*phi_test[i])
				eps, eps_test = fu.eps(u_tr[i]), fu.eps(u_test[i])
				u_form += fe.inner(D1*(nu[i]*fe.div(S*phi_tr[i])*fe.Identity(2) + (1.0-nu[i])*kappa), kappa_test)*fe.dx
				u_form += fe.inner(D3*(nu[i]*fe.div(u_tr[i])*fe.Identity(2) + (1.0-nu[i])*eps), eps_test)*fe.dx

			D2 = (mu[i]*h[i]*5.0)/6.0
			u_form += D2*fe.inner(fe.grad(w_tr[i]) + S*phi_tr[i], fe.grad(w_test[i]) + S*phi_test[i])*dx_shear

		return u_form, "nlin"

	def get_d_form(self, x, d):
		d_i = fe.split(d)
		d_test = fe.TestFunctions(self.W)
		gc = self.mat.glass_mat.Gc
		lc = self.mat.glass_mat.lc
		ft = self.mat.glass_mat.ft
		# TODO: make it better
		h = [self.mat.get_hs()[0], self.mat.get_hs()[2]]
		b = self.cross.b

		d_form = 0.0
		for i in range(2):
			d_form += -2*self.get_energy_active(x, d, 2*i)*fe.inner(1.0 - d_i[i], d_test[i])*fe.dx
			d_form += h[i]*3.0/8.0*gc*(1.0/lc*d_test[i] + 2*lc*fe.inner(fe.grad(d_i[i]), fe.grad(d_test[i])))*fe.dx

		return d_form

	def get_energy_active(self, x, d, l_num):
		lg_fce = fe.split(x)
		u_, phi_ = lg_fce[l_num+1], lg_fce[l_num+2]
		S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])
		h = self.mat.get_hs()[l_num]
		mu = self.mat.get_Gs()[l_num]
		lmbda = self.mat.get_lmbdas()[l_num]

		u_z_l = u_ + S*phi_*0.5*h
		u_z_u = u_ - S*phi_*0.5*h
		en = fu.max_fce(fu.psi_p_vd(u_z_l, lmbda, mu), fu.psi_p_vd(u_z_u, lmbda, mu))*h

		return en

	def get_d_max(self):
		# TODO: make it better
		return fe.interpolate(fe.Constant((1.0, 1.0)), self.W)

	def init_files(self, files):
		files_i = []
		for i in range(3):
			files_i.append("displ_u_" + str(i) + ".xdmf")
			files_i.append("displ_w_" + str(i) + ".xdmf")
			files_i.append("displ_phi_" + str(i) + ".xdmf")
			files_i.append("damage_" + str(i) + ".xdmf")
		files.init_files(files_i)

	def save_u(self, files, x, t):
		w_sol, u1_sol, phi1_sol, u3_sol, phi3_sol = x.split(deepcopy=True)
		files.save_to(u1_sol, 0, t)
		files.save_to(w_sol, 1, t)
		files.save_to(phi1_sol, 2, t)
		files.save_to(u3_sol, 8, t)
		files.save_to(phi3_sol, 10, t)

	def save_d(self, files, d, t):
		d_sol_1, d_sol_3 = d.split(deepcopy=True)
		files.save_to(d_sol_1, 3, t)
		files.save_to(d_sol_3, 11, t)

	def get_strain_and_stress(self, m_x, m_y, u_i, d_i):
		S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])
		w_sol, u1_sol, phi1_sol, u3_sol, phi3_sol = u_i.split(deepcopy=True)
		d_sol_1, d_sol_3 = d_i.split(deepcopy=True)
		eps_i = fe.grad(u1_sol) - fe.grad(S*phi1_sol)*0.5*self.mat.get_hs()[0]
		u_local_i = u1_sol - S*phi1_sol*0.5*self.mat.get_hs()[0]
		strain = fu.local_project(eps_i, self.V0)
		stress = fu.local_project(fu.sigma_vd(u_local_i, d_sol_1, self.mat.glass_mat.lmbda, self.mat.glass_mat.mu), self.V0)
		return strain(m_x, m_y)[0], 0.0, stress(m_x, m_y)[0], 0.0


# --------------------------
# Plane stress damage model
# --------------------------
class PSDamageModel:
	# PSDamageModel Constructor
	def __init__(self, material, mesh, mesh_d, u_type, d_type, dec_type):
		self.mat = material
		self.mesh = mesh
		self.mesh_d = mesh_d
		self.V = None
		self.V0 = None
		self.DS = None
		self.W = None
		self.bc_u = []
		self.bc_d = []
		self.u_d = None
		self.u_type = u_type  # Type of form for displacement calculation
		self.d_type = d_type  # Type of form for damage calculation
		self.dec_type = dec_type  # Type of stress decomposition for damage calculation

	# Define spaces
	def init_spaces(self, degr):
		self.V = fe.VectorFunctionSpace(self.mesh, "CG", degr)  # Function space for displacements
		self.V0 = fe.TensorFunctionSpace(self.mesh, "DG", degr-1)  # Function space for stress components
		self.DS = fe.FunctionSpace(self.mesh, "DG", degr-1)  # Function space for history variable
		#self.W = fe.FunctionSpace(self.mesh, "CG", degr)  # Function space for damage variable
		self.W = fe.FunctionSpace(self.mesh_d, "CG", degr)
		self.W0 = fe.FunctionSpace(self.mesh, "DG", degr)

	# Set Dirichlet boundary conditions and parameter function u_d
	def set_bc(self, bc_u, bc_d, u_d):
		self.bc_u = bc_u
		self.bc_d = bc_d
		self.u_d = u_d

	# Update time-like parameter in u_d function
	def time_step_update(self, t):
		self.u_d.t = t
		self.mat.update(t)

	# Return corresponding bilinear form for displacements
	def get_u_form(self, u, d):
		u_tr = fe.TrialFunction(self.V)
		u_test = fe.TestFunction(self.V)
		lmbda, mu = self.mat.get_lame_cofs()
		if self.u_type == "el":
			return (1-d)**2*fe.inner(fu.sigma_el(u, lmbda, mu), fu.eps(u_test))*fe.dx + fe.dot(fe.Constant((0.0, 0.0)), u_test)*fe.dx, "nlin"
		elif self.u_type == "vd":
			return (1-d)**2*fe.inner(fu.sigma_p_vd(u, lmbda, mu), fu.eps(u_test))*fe.dx + fe.inner(fu.sigma_n_vd(u, lmbda, mu), fu.eps(u_test))*fe.dx, "nlin"
		elif self.u_type == "sd":
			return (1-d)**2*fe.inner(fu.sigma_p_sd(u, lmbda, mu), fu.eps(u_test))*fe.dx + fe.inner(fu.sigma_n_sd(u, lmbda, mu), fu.eps(u_test))*fe.dx, "nlin"
		else:
			raise Exception("Type of displacement form must be el/vd/sd!")

	def get_stress_tensor(self, u_i, d_i):
		lmbda, mu = self.mat.get_lame_cofs()
		return fu.local_project((1 - d_i)**2*self.get_sigma_active(u_i, lmbda, mu) + self.get_sigma_pasive(u_i, lmbda, mu), self.V0)

	# Return active part of stress tensor
	def get_sigma_active(self, u_i, lmbda_i, mu_i):
		if self.dec_type == "el":
			return fu.sigma_el(u_i, lmbda_i, mu_i)
		elif self.dec_type == "vd":
			return fu.sigma_p_vd(u_i, lmbda_i, mu_i)
		elif self.dec_type == "sd":
			return fu.sigma_p_sd(u_i, lmbda_i, mu_i)
		else:
			raise Exception("Type of decomposition must be el/vd!")

	# Return pasive part of stress tensor
	def get_sigma_pasive(self, u_i, lmbda_i, mu_i):
		if self.dec_type == "el":
			return 0
		elif self.dec_type == "vd":
			return fu.sigma_n_vd(u_i, lmbda_i, mu_i)
		elif self.dec_type == "sd":
			return fu.sigma_n_sd(u_i, lmbda_i, mu_i)
		else:
			raise Exception("Type of decomposition must be el/vd!")

	# Return bilinear formulation for damage
	def get_d_form(self, u, d):
		d_tr = fe.TrialFunction(self.W)
		d_test = fe.TestFunction(self.W)
		#gc = self.mat.Gc
		#lc = self.mat.lc
		#ft = self.mat.ft
		gc = self.mat.get_gc()
		lc = self.mat.get_lc()
		ft = self.mat.get_ft()
		lmbda, mu = self.mat.get_lame_cofs_d()
		if self.d_type == "pham":
			E_ds = -fe.inner(self.get_sigma_active(u, lmbda, mu), fu.eps(u))*fe.inner(1.0 - d, d_test)*fe.dx
			E_ds += 3.0/8.0*gc*(1.0/lc*d_test + 2*lc*fe.inner(fe.grad(d), fe.grad(d_test)))*fe.dx
		elif self.d_type == "bourdin":
			E_ds = -fe.inner(self.get_sigma_active(u, lmbda, mu), fu.eps(u))*fe.inner(1.0 - d, d_test)*fe.dx
			E_ds += gc*(1.0/lc*fe.inner(d, d_test) + lc*fe.inner(fe.grad(d), fe.grad(d_test)))*fe.dx
		elif self.d_type == "stress":
			stress = fu.sigma_el(u, self.mat)
			sg00, sg01, sg10, sg11 = fu.eig_v(stress)
			D = fu.mc_bracket((fu.mc_bracket(sg00) /ft)**2 + (fu.mc_bracket(sg11)/ft)**2 - 1)
			E_ds = -D*fe.inner(1.0 - d, d_test)*fe.dx
			E_ds += (fe.inner(d, d_test) + lc**2*fe.inner(fe.grad(d), fe.grad(d_test))) * fe.dx
		else:
			raise Exception("Damage type must be pham/bourdin/stress!")
		return E_ds

	def init_files(self, files):
		files.init_files(["displ_u.xdmf", "damage.xdmf"])

	def save_u(self, files, x, t):
		files.save_to(x, 0, t)

	def save_d(self, files, d, t):
		files.save_to(d, 1, t)

	# Unused?
	def get_stress(self, m_x, m_y, u_i, d_i):
		stress = self.get_stress_tensor(u_i, d_i)
		return stress(m_x, m_y)[0]

	def get_strain_and_stress(self, m_x, m_y, u_i, d_i):
		strain = fu.local_project(fu.eps(u_i), self.V0)
		stress = self.get_stress_tensor(u_i, d_i)
		return strain(m_x, m_y)[0], 0.0, stress(m_x, m_y)[0], 0.0

	def get_d_max(self):
		return fe.interpolate(fe.Constant(1.0), self.W)