import fenics as fe


# --------------------------
# Class of static functions
# --------------------------
# Macauley bracket
def mc_bracket(v):
	return 0.5 * (v + abs(v))


# Maximum of two function with UFL
def max_fce(a, b):
	return 0.5*(a+b+abs(a-b))


# Projection on each element
def local_project(fce, space):
	lp_trial, lp_test = fe.TrialFunction(space), fe.TestFunction(space)
	lp_a = fe.inner(lp_trial, lp_test)*fe.dx
	lp_L = fe.inner(fce, lp_test)*fe.dx
	local_solver = fe.LocalSolver(lp_a, lp_L)
	local_solver.factorize()
	lp_f = fe.Function(space)
	local_solver.solve_local_rhs(lp_f)
	return lp_f


# Symmetry gradient of displacements
def eps(v):
	return fe.sym(fe.grad(v))


# Elastic stress tensor
def sigma_el(u_i, lmbda, mu):
	return lmbda*(fe.tr(eps(u_i)))*fe.Identity(2) + 2.0*mu*(eps(u_i))


# Positive stress tensor - spectral decomposition
def sigma_p_sd(u_i, lmbda, mu):
	return lmbda*(fe.tr(eps_p(u_i)))*fe.Identity(2) + 2.0*mu*(eps_p(u_i))


# Negative stress tensor - spectral decomposition
def sigma_n_sd(u_i, lmbda, mu):
	return lmbda*(fe.tr(eps_n(u_i)))*fe.Identity(2) + 2.0*mu*(eps_n(u_i))


# Positive stress tensor - volumetric-deviatoric
def sigma_p_vd(u_i, lmbda, mu):
	kn = lmbda + mu
	return kn*mc_bracket(fe.tr(eps(u_i)))*fe.Identity(2) + 2*mu*fe.dev(eps(u_i))


# Negative stress tensor - volumetric-deviatoric
def sigma_n_vd(u_i, lmbda, mu):
	kn = lmbda + mu
	return -kn*mc_bracket(-fe.tr(eps(u_i)))*fe.Identity(2)


def sigma_vd(u_i, d_i, lmbda, mu):
	k_res = fe.Constant(0.001)
	return ((1-d_i)**2 + k_res)*sigma_p_vd(u_i, lmbda, mu) + sigma_n_vd(u_i, lmbda, mu)
	#return sigma_p_vd(u_i, lmbda, mu) + sigma_n_vd(u_i, lmbda, mu)


def psi_p_vd(u_i, lmbda, mu):
	kn = lmbda + mu
	return 0.5*kn*mc_bracket(fe.tr(eps(u_i)))**2 + mu*fe.tr(fe.dev(eps(u_i))*fe.dev(eps(u_i)))


def psi_n_vd(u_i, lmbda, mu):
	kn = lmbda + mu
	return 0.5*kn*mc_bracket(-fe.tr(eps(u_i)))**2


def psi_p_sd(u_i, lmbda, mu):
	return (0.5*lmbda*mc_bracket(fe.tr(eps(u_i)))**2) + mu*fe.tr(eps_p(u_i)*eps_p(u_i))


def psi_n_sd(u_i, lmbda, mu):
	return (0.5*lmbda*mc_bracket(-fe.tr(eps(u_i)))**2) + mu*fe.tr(eps_n(u_i)*eps_n(u_i))


# eigenvalues for 2x2 matrix
def eig_v(a):
	v00 = a[0, 0]/2 + a[1, 1]/2 - fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2
	v01 = 0.0
	v10 = 0.0
	v11 = a[0, 0]/2 + a[1, 1]/2 + fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2
	return v00, v01, v10, v11


# eigenvectors for 2x2 matrix
def eig_w(a):
	w00 = -a[0, 1]/(a[0, 0]/2 - a[1, 1]/2 + fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2)
	w01 = -a[0, 1]/(a[0, 0]/2 - a[1, 1]/2 - fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2)
	w10 = 1.0
	w11 = 1.0
	return w00, w01, w10, w11


# positive strain tensor
def eps_p(v):
	v00, v01, v10, v11 = eig_v(eps(v))
	v00 = fe.conditional(fe.gt(v00, 0.0), v00, 0.0)
	v11 = fe.conditional(fe.gt(v11, 0.0), v11, 0.0)
	w00, w01, w10, w11 = eig_w(eps(v))
	wp = ([w00, w01], [w10, w11])
	wp = fe.as_tensor(wp)
	vp = ([v00, v01], [v10, v11])
	vp = fe.as_tensor(vp)
	return wp*vp*fe.inv(wp)


# negative strain tensor
def eps_n(v):
	v00, v01, v10, v11 = eig_v(eps(v))
	v00 = fe.conditional(fe.lt(v00, 0.0), v00, 0.0)
	v11 = fe.conditional(fe.lt(v11, 0.0), v11, 0.0)
	w00, w01, w10, w11 = eig_w(eps(v))
	wn = ([w00, w01], [w10, w11])
	wn = fe.as_tensor(wn)
	vn = ([v00, v01], [v10, v11])
	vn = fe.as_tensor(vn)
	return wn*vn*fe.inv(wn)
