import fenics as fe
import numpy as np
import matplotlib.pyplot as plt
import formulations.Functions as fu
import os


# --------------------------
# Staggered scheme class
# --------------------------
class StaggeredScheme:
	def __init__(self, formulation, time, files):
		self.max_iter = 10
		self.tol = 1.0e-5
		self.form = formulation
		self.time_set = time
		self.u = fe.Function(self.form.V)
		self.d = fe.Function(self.form.W)
		self.du = fe.Function(self.form.V)
		self.dd = fe.Function(self.form.W)
		self.u_old = fe.Function(self.form.V)
		self.d_old = fe.Function(self.form.W)
		self.d_min = fe.Function(self.form.W)
		self.d_max = fe.Function(self.form.W)
		self.u_form = None
		self.u_form_type = "nlin"
		self.d_form = None
		self.files = files
		self.mon = None
		self.mon_r = None

	# Set initial damage profile
	def set_d_init(self, d_init):
		self.d_min = fe.project(d_init, self.form.W)

	# Set stress and strain monitoring
	def set_monitor(self, mon):
		self.mon = mon

	# Set reaction monitoring
	def set_monitor_react(self, mon_r):
		self.mon_r = mon_r

	# Get 'u' and 'd' formulation for given spatial model
	def init_forms(self):
		self.u_form, self.u_form_type = self.form.get_u_form(self.u, self.d)
		self.d_form = self.form.get_d_form(self.u, self.d)
		self.d_max = self.form.get_d_max()

	# Displacement solver
	def solve_displacement(self):
		if self.u_form_type == "nlin":
			self.u.vector()[:] = np.random.random(self.u.vector().size())
			# self.form.update_u_form(self.u, self.d)
			a_form = self.u_form
			fe.solve(a_form == 0, self.u, self.form.bc_u, solver_parameters={"newton_solver": {"error_on_nonconvergence": False}})
		elif self.u_form_type == "lin":
			a_form = fe.lhs(self.u_form)
			l_form = fe.rhs(self.u_form)
			fe.solve(a_form == l_form, self.u, self.form.bc_u)
		else:
			print("Error in displacement solver.")

	# Damage snes variational inequalities solver based on Newton's method
	def solve_damage(self):
		lower = self.d_min
		upper = self.d_max

		# Solution of damage formulation
		H = fe.derivative(self.d_form, self.d, fe.TrialFunction(self.form.W))

		snes_solver_parameters = {"nonlinear_solver": "snes",
								  "snes_solver": {"linear_solver": "lu",
												  "relative_tolerance": 1.0e-6,
												  "absolute_tolerance": 1.0e-6,
												  "maximum_iterations": 50,
												  "report": True,
												  "error_on_nonconvergence": False,
												  "line_search": "basic"}}

		# prm['snes_solver']['line_search'] = 'basic'

		problem = fe.NonlinearVariationalProblem(self.d_form, self.d, self.form.bc_d, H)
		problem.set_bounds(lower, upper)

		solver = fe.NonlinearVariationalSolver(problem)
		solver.parameters.update(snes_solver_parameters)
		solver.solve()

	def update_d_variable(self):
		lmbda, mu = self.form.mat.get_lame_cofs()
		d_new = fe.conditional(fe.gt(1.1*fu.psi_p_vd(self.u, lmbda, mu), fu.psi_n_vd(self.u, lmbda, mu)), self.d, 0.0)
		self.d.assign(fe.project(d_new, self.form.W))

	# Staggered solver
	def solve(self, update_d):
		# Initialization of bilinear forms
		self.init_forms()
		self.form.init_files(self.files)

		if not os.path.exists(self.files.folder):
			os.makedirs(self.files.folder)

		file_object = open(self.files.folder + "/" + "log_file.txt", "w+")
		file_object.write("Log_file.txt\n")
		file_object.write("eps_staggered = " + str(self.tol) + ", max_iters_staggered = " + str(self.max_iter) + ", True = converges\n")
		file_object.write("time\titer_numbers\tconverges\n")
		file_object.close()

		# Time loop
		for i in range(0, len(self.time_set)):
			t = self.time_set[i]

			# Informative print of time instant
			print("Time instant: ", t)
			self.form.time_step_update(t)

			ite = 1
			err = 50.0

			converges = False

			# Staggered loop
			while err > self.tol:

				# Damage and displacement solutions
				self.solve_displacement()
				self.solve_damage()

				if update_d:
					print("hej")
					self.update_d_variable()

				# Damage and displacement increments
				self.du.assign(self.u - self.u_old)
				self.dd.assign(self.d - self.d_old)

				# Errors - damage is not standardized
				err_u = fe.norm(self.du)/fe.norm(self.u)
				err_d = fe.norm(self.dd)
				err = max(err_u, err_d)

				print("iter", ite, "errors", err_u, err_d)

				self.u_old.assign(self.u)
				self.d_old.assign(self.d)

				ite += 1

				# Max iterations condition
				if ite > self.max_iter:
					print("max iterations reached")
					break

				if err <= self.tol:
					converges = True
				#if t > 17:
				#	en = self.form.get_energy_active(self.u, self.d)
				#	plot_en = fe.plot(en)
				#	#plt.colorbar(plot_en)
				#	plt.show()

			#en = self.form.get_energy_active(self.u, self.d)
			#fe.plot(fe.project(16*en, fe.FunctionSpace(self.form.mesh, "DG", 0)))
			#plt.title("time t=" + str(t))
			#plt.show()

			self.d_min.assign(self.d)
			self.form.save_u(self.files, self.u, t)
			self.form.save_d(self.files, self.d, t)

			file_object = open(self.files.folder + "/" + "log_file.txt", "a")
			file_object.write(str(round(t, 5)) + "\t" + str(ite) + "\t" + str(converges) + "\n")
			file_object.close()

			if self.mon is not None:
				# stress_tens = self.form.get_stress_tensor(self.u, self.d)
				# self.mon.append_data(self.form.u_d, stress_tens)
				for mon_i in self.mon:
					mon_i.append_data(self.form, self.u, self.d)

			if self.mon_r is not None:
				self.mon_r.append_data(self.form, self.u_form, self.u, self.d)

		self.files.close()


# ----------------------------------
# Displacement solver without damage
# ----------------------------------
class TimeDisplacementScheme:
	def __init__(self, formulation, time, files):
		self.max_iter = 50
		self.tol = 1.0e-5
		self.form = formulation
		self.time_set = time
		self.u = fe.Function(self.form.V)
		self.d = fe.Function(self.form.W)
		self.du = fe.Function(self.form.V)
		self.u_old = fe.Function(self.form.V)
		self.u_form = None
		self.u_form_type = "nlin"
		self.files = files

	def set_monitor(self, mon):
		self.mon = mon

	def init_forms(self):
		self.u_form, self.u_form_type = self.form.get_u_form(self.u, self.d)

	def solve_displacement(self):
		if self.u_form_type == "nlin":
			self.u.vector()[:] = np.random.random(self.u.vector().size())
			# self.form.update_u_form(self.u, self.d)
			a_form = self.u_form
			fe.solve(a_form == 0, self.u, self.form.bc_u, solver_parameters={"newton_solver": {"error_on_nonconvergence": False}})
		elif self.u_form_type == "lin":
			a_form = fe.lhs(self.u_form)
			l_form = fe.rhs(self.u_form)
			fe.solve(a_form == l_form, self.u, self.form.bc_u)
		else:
			print("Error in displacement solver.")

	def solve(self):
		# Initialization of bilinear forms
		self.init_forms()
		self.form.init_files(self.files)

		# Time loop
		for i in range(0, len(self.time_set)):
			t = self.time_set[i]

			# Informative print of time instant
			print("Time instant: ", t)
			self.form.time_step_update(t)

			self.solve_displacement()

			self.form.save_u(self.files, self.u, t)

			if self.mon is not None:
				self.mon.append_data(self.form, self.u, self.d)

		self.files.close()
