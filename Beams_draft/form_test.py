import fenics as fe
import numpy as np
import matplotlib.pyplot as plt


# --------------------
# Functions and classes
# --------------------
def left_point(x):
	return fe.near(x[0], l_off)


def left_load_point(x):
	return fe.near(x[0], l_x / 2 - l_0 / 2)


def symmetry_axis(x, on_boundary):
	return fe.near(x[0], l_x/2) and on_boundary


def prepare_mesh():
	return fe.IntervalMesh(n_x, 0.0, l_x/2)


def mc_bracket(v):
	return 0.5 * (v + abs(v))


def solve_displacement():
	# Selective integration for shearlock reduction
	u.vector()[:] = np.random.random(u.vector().size())
	dx_shear = fe.dx(metadata={"quadrature_degree": 1})
	u_tr, w_tr, phi_tr = fe.split(u)
	u_test, w_test, phi_test = fe.TestFunctions(V)
	u_form = 0.0
	for i in range(len(hs) - 1):
		gauss = 0.5 * (hs[i] + hs[i + 1])
		eps_z = u_tr.dx(0) + phi_tr.dx(0) * gauss
		d_eps_z = u_test.dx(0) + phi_test.dx(0) * gauss
		eps_z_p = mc_bracket(eps_z)
		eps_z_n = -mc_bracket(-eps_z)
		u_form += fe.inner((1.0 - d) ** 2 * E * eps_z_p + E * eps_z_n, d_eps_z) * dh * fe.dx
	u_form += w_test.dx(0) * mu * A * (w_tr.dx(0) + phi_tr) * dx_shear
	u_form += phi_test * mu * A * (phi_tr + w_tr.dx(0)) * dx_shear
	u_form -= fe.Constant(0.0) * w_test * fe.dx

	fe.solve(u_form == 0, u, bc_u)


def get_energy_active(u, d):
	u_, w_, phi_ = u.split(deepcopy=True)
	en = 0.0
	for i in range(len(hs) - 1):
		gauss = 0.5 * (hs[i] + hs[i + 1])
		eps_z = u_.dx(0) + phi_.dx(0) * gauss
		eps_z_p = mc_bracket(eps_z)
		temp = 0.5 * E * eps_z_p ** 2 * dh
		en += temp
	print(en)
	return en


def solve_damage():
	d_test = fe.TestFunction(W)
	d_form = -20*get_energy_active(u, d) * fe.inner(1.0 - d, d_test) * fe.dx
	d_form += h*3.0 / 8.0 *Gc_b*(1.0 / lc * d_test + 2 * lc * fe.inner(fe.grad(d), fe.grad(d_test))) * fe.dx

	lower = d_min
	upper = fe.interpolate(fe.Constant(1.00), W)

	# Solution of damage formulation
	H = fe.derivative(d_form, d, fe.TrialFunction(W))

	snes_solver_parameters = {"nonlinear_solver": "snes",
							  "snes_solver": {"linear_solver": "lu",
											  # "relative_tolerance": 1.0e-4,
											  # "absolute_tolerance": 1.0e-2,
											  "maximum_iterations": 20,
											  "report": True,
											  "error_on_nonconvergence": False,
											  "line_search": "basic"}}

	# prm['snes_solver']['line_search'] = 'basic'

	problem = fe.NonlinearVariationalProblem(d_form, d, bc_d, H)
	problem.set_bounds(lower, upper)

	solver = fe.NonlinearVariationalSolver(problem)
	solver.parameters.update(snes_solver_parameters)
	solver.solve()


# Material parameters
E = 76.6e9  # Young's modulus
nu = 0.22  # Poisson ratio
lc = 0.005
ft = 60.0e6

mu = E / 2 / (1 + nu)

Gc_b = lc * 256.0 / 27.0 * ft ** 2 / E
Gc_p = lc * 8.0 / 3.0 * ft ** 2 / E

# Geometry parameters
l_x = 1.0  # Length of glass beam
l_off = 0.05  # Support offset
l_0 = 0.2  # Pitch of load points
b = 1.0
h = 0.02

n_x = 400
n_ni = 40

A = b*h

max_iter = 50
tol = 1.0e-5

# Time parameters
t_start = 0.1
# t_end = 10.59
# t_end = 2.0
t_end = 9.0
dt = 0.01
t_steps = int((t_end-t_start)/dt)

time_space = np.linspace(t_start, t_end, t_steps)

# --------------------
# Define geometry
# --------------------
mesh = prepare_mesh()

fe.plot(mesh)
plt.show()

hs = np.linspace(-0.5*h, 0.5*h, n_ni)
dh = abs(hs[1] - hs[0])

# model_pham = f.BeamDamageModel(glass_p, cross, mesh, "el", "pham", hs)

# Init spaces - for BC definition
# model_pham.init_spaces()
# V = model_pham.V
p1 = fe.FiniteElement('P', fe.interval, 1)  # Space of linear polynomial functions
element = fe.MixedElement([p1, p1, p1])  # Element of mixed space
V = fe.FunctionSpace(mesh, element)  # Mixed space
V0 = fe.FunctionSpace(mesh, 'DG', 0)
W = fe.FunctionSpace(mesh, 'P', 1)  # Space of linear polynomial functions

# --------------------
# Boundary conditions
# --------------------
u_d = fe.Expression("-t/1000.0", t=0.0, degree=0)  # Prescribed displacement
BC_u_1 = fe.DirichletBC(V.sub(1), 0.0, left_point, method="pointwise")
BC_u_2 = fe.DirichletBC(V.sub(1), u_d, left_load_point, method="pointwise")
BC_u_3 = fe.DirichletBC(V.sub(0), 0.0, symmetry_axis)
BC_u_4 = fe.DirichletBC(V.sub(2), 0.0, symmetry_axis)
bc_u = [BC_u_1, BC_u_2, BC_u_3, BC_u_4]
bc_d = []

u = fe.Function(V)
d = fe.Function(W)
du = fe.Function(V)
dd = fe.Function(W)
u_old = fe.Function(V)
d_old = fe.Function(W)
d_min = fe.Function(W)


# Time loop
for i in range(0, len(time_space)):
	t = time_space[i]

	# Informative print of time instant
	print("Time instant: ", t)
	u_d.t = t

	ite = 1
	err = 10.0

	# Staggered loop
	while err > tol:

		solve_displacement()

		solve_damage()

		du.assign(u - u_old)
		dd.assign(d - d_old)

		err_u = fe.norm(du)/fe.norm(u)
		err_d = fe.norm(dd)
		err = max(err_u, err_d)

		print("iter", ite, "errors", err_u, err_d)

		u_old.assign(u)
		d_old.assign(d)

		ite += 1

		if ite > max_iter:
			print("max iterations reached")
			break

	d_min.assign(d)

	print(np.amax(d.vector()[:]))