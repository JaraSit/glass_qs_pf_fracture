import formulations as f
import solver as s
import fenics as fe
import numpy as np
import matplotlib.pyplot as plt


# --------------------
# Functions and classes
# --------------------
def left_point(x):
	return fe.near(x[0], l_off)


def left_load_point(x):
	return fe.near(x[0], l_x / 2 - l_0 / 2)


def symmetry_axis(x, on_boundary):
	return fe.near(x[0], l_x/2) and on_boundary


def prepare_mesh():
	return fe.IntervalMesh(n_x, 0.0, l_x/2)


# Material parameters
E = 76.6e9  # Young's modulus
nu = 0.22  # Poisson ratio
lc = 0.005
ft = 60.0e6

Gc_b = lc * 256.0 / 27.0 * ft ** 2 / E
Gc_p = lc * 8.0 / 3.0 * ft ** 2 / E

glass_b = f.ElasticMaterial(E, nu, Gc_b, lc, ft)
glass_p = f.ElasticMaterial(E, nu, Gc_p, lc, ft)

# Geometry parameters
l_x = 1.0  # Length of glass beam
l_off = 0.05  # Support offset
l_0 = 0.2  # Pitch of load points
b = 1.0
h = 0.02

n_x = 400
n_ni = 40

cross = f.CrossSection(b, h)

# Time parameters
t_start = 0.1
# t_end = 10.59
# t_end = 2.0
t_end = 9.0
dt = 0.01
t_steps = int((t_end-t_start)/dt)

time_space = np.linspace(t_start, t_end, t_steps)

# --------------------
# Define geometry
# --------------------
mesh = prepare_mesh()

fe.plot(mesh)
plt.show()

hs = np.linspace(-0.5*h, 0.5*h, n_ni)

# Create model with pham/bourdin/stress formulation with material glass_p
# 3rd argument: type of displacement equation
# 4th argument: type of damage model
# 5th argument: type of decomposition for damage calculation
model_pham = f.BeamDamageModel(glass_p, cross, mesh, "el", "pham", hs)
# model_bourdin = f.BeamDamageModel(glass_b, cross, mesh, "el", "bourdin", hs)
# model_stress = f.BeamDamageModel(glass_p, cross, mesh, "el", "stress", hs)

# Init spaces - for BC definition
model_pham.init_spaces()
V = model_pham.V

# --------------------
# Boundary conditions
# --------------------
u_d = fe.Expression("-t/1000.0", t=0.0, degree=0)  # Prescribed displacement
BC_u_1 = fe.DirichletBC(V.sub(1), 0.0, left_point, method="pointwise")
BC_u_2 = fe.DirichletBC(V.sub(1), u_d, left_load_point, method="pointwise")
BC_u_3 = fe.DirichletBC(V.sub(0), 0.0, symmetry_axis)
BC_u_4 = fe.DirichletBC(V.sub(2), 0.0, symmetry_axis)
BC_u = [BC_u_1, BC_u_2, BC_u_3, BC_u_4]
BC_d = []
model_pham.set_bc(BC_u, BC_d, u_d)

# 1st argument of PostProcess object: Folder for solution
solution = f.PostProcess("Solution_pham_beam_01")
ss = s.StaggeredScheme(model_pham, time_space, solution)

# One-point monitoring of sigma_x
mon1 = f.Monitor(l_x/2, 0.0)
ss.set_monitor(mon1)

# Staggered solver
ss.solve()

# Plotting of monitor data
mon1.plot_monitor()
solution.save_monitor(mon1)
