import fenics as fe
import matplotlib.pyplot as plt
import numpy as np


# --------------------------
# Auxiliary functions
# --------------------------
# Macauley bracket
def mc_bracket(v):
	return 0.5 * (v + abs(v))


# Maximum of two function with UFL
def max_fce(a, b):
	return 0.5*(a+b+abs(a-b))


# Projection on each element
def local_project(fce, space):
	lp_trial, lp_test = fe.TrialFunction(space), fe.TestFunction(space)
	lp_a = fe.inner(lp_trial, lp_test)*fe.dx
	lp_L = fe.inner(fce, lp_test)*fe.dx
	local_solver = fe.LocalSolver(lp_a, lp_L)
	local_solver.factorize()
	lp_f = fe.Function(space)
	local_solver.solve_local_rhs(lp_f)
	return lp_f


# Symmetry gradient of displacements
def eps(v):
	return fe.sym(fe.grad(v))


# Elastic stress tensor
def sigma_el(u_i, mat_i):
	return mat_i.lmbda*(fe.tr(eps(u_i)))*fe.Identity(2) + 2.0*mat_i.mu*(eps(u_i))


# Positive stress tensor - volumetric-deviatoric
def sigma_p_vd(u_i, mat_i):
	return mat_i.kn*mc_bracket(fe.tr(eps(u_i)))*fe.Identity(2) + 2*mat_i.mu*fe.dev(eps(u_i))


# Negative stress tensor - volumetric-deviatoric
def sigma_n_vd(u_i, mat_i):
	return -mat_i.kn*mc_bracket(-fe.tr(eps(u_i)))*fe.Identity(2)


# eigenvalues for 2x2 matrix
def eig_v(a):
	v00 = a[0, 0]/2 + a[1, 1]/2 - fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2
	v01 = 0.0
	v10 = 0.0
	v11 = a[0, 0]/2 + a[1, 1]/2 + fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2
	return v00, v01, v10, v11


# eigenvectors for 2x2 matrix
def eig_w(a):
	w00 = -a[0, 1]/(a[0, 0]/2 - a[1, 1]/2 + fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2)
	w01 = -a[0, 1]/(a[0, 0]/2 - a[1, 1]/2 - fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2)
	w10 = 1.0
	w11 = 1.0
	return w00, w01, w10, w11


# --------------------------
# Elastic material class
# --------------------------
class ElasticMaterial:
	def __init__(self, E, nu, Gc, lc, ft, type="plane_strain"):
		self.E = E  # Young's modulus
		self.nu = nu  # Poisson ration
		self.Gc = Gc  # Fracture toughness
		self.lc = lc  # Localization band width
		self.ft = ft  # Fracture strength
		self.lmbda = E*nu/(1 + nu)/(1 - 2*nu)  # Lame coefficients
		self.mu = E / 2 / (1 + nu)  # Lame coefficients
		if type == "plane_stress":
			self.lmbda = 2*self.mu*self.lmbda/(self.lmbda + 2*self.mu)  # Correction for plane stress
		self.kn = self.lmbda + self.mu  # Bulk modulus


# --------------------------
# Cross section class
# --------------------------
class CrossSection:
	def __init__(self, b, h):
		self.b = b
		self.h = h
		self.A = b*h
		self.I = 1.0/12.0*b*h**3


# --------------------------
# Class for post-processing
# --------------------------
class PostProcess:
	def __init__(self, folder):
		self.folder = folder
		self.files = []

	def init_files(self, file_names):
		for i in range(len(file_names)):
			self.files.append(fe.XDMFFile(self.folder + "/" + file_names[i]))
		# self.file_u = fe.XDMFFile(self.folder + "/displacements.xdmf")  # XDMF file for displacements
		# self.file_d = fe.XDMFFile(self.folder + "/damage.xdmf")  # XDMF file for damage variable

	# WILL BE DEPRECIATED
	def save_u_d_xdmf(self, u, d, t):
		print("Function save_u_d_xdmf will be depreciated.")
		self.file_u.write(u, t)
		self.file_d.write(d, t)

	def save_to(self, func, ind, t):
		self.files[ind].write(func, t)

	def close(self):
		for files_i in self.files:
			files_i.close()

	# WILL BE DEPRECIATED
	# def close(self):
	# 	self.file_u.close()
	# 	self.file_d.close()

	def save_monitor(self, mon):
		np.savetxt(self.folder + "/stress_data.txt", np.column_stack((mon.mon_u_d, mon.mon_stress)))


# --------------------------
# Auxiliary structure for monitoring
# --------------------------
class Monitor:
	def __init__(self, m_x, m_y):
		self.m_x = m_x
		self.m_y = m_y
		self.mon_u_d = []
		self.mon_stress = []

	def append_data(self, form, u_i, d_i):
		self.mon_u_d.append(form.u_d(self.m_x, self.m_y))
		self.mon_stress.append(form.get_stress(self.m_x, self.m_y, u_i, d_i))

	# def append_data_2(self, u_d_i, stress_i):
	# 	self.mon_u_d.append(u_d_i(self.m_x, self.m_y))
	# 	self.mon_stress.append(stress_i(self.m_x, self.m_y)[0])

	def plot_monitor(self):
		xx = np.array(self.mon_u_d)
		yy = np.array(self.mon_stress)
		plt.style.use("classic")
		plt.plot(abs(xx*1.0e3), yy/1.0e6)
		plt.xlabel("Prescribed displacement [mm]")
		plt.ylabel("Stress [MPa]")
		plt.show()


# --------------------------
# Plane stress damage model
# --------------------------
class PSDamageModel:
	# PSDamageModel Constructor
	def __init__(self, material, mesh, u_type, d_type, dec_type):
		self.mat = material
		self.mesh = mesh
		self.V = None
		self.V0 = None
		self.DS = None
		self.W = None
		self.bc_u = []
		self.bc_d = []
		self.u_d = None
		self.u_type = u_type  # Type of form for displacement calculation
		self.d_type = d_type  # Type of form for damage calculation
		self.dec_type = dec_type  # Type of stress decomposition for damage calculation

	# Define spaces
	def init_spaces(self):
		self.V = fe.VectorFunctionSpace(self.mesh, "CG", 1)  # Function space for displacements
		self.V0 = fe.TensorFunctionSpace(self.mesh, "DG", 0)  # Function space for stress components
		self.DS = fe.FunctionSpace(self.mesh, "DG", 0)  # Function space for history variable
		self.W = fe.FunctionSpace(self.mesh, "CG", 1)  # Function space for damage variable

	# Set Dirichlet boundary conditions and parameter function u_d
	def set_bc(self, bc_u, bc_d, u_d):
		self.bc_u = bc_u
		self.bc_d = bc_d
		self.u_d = u_d

	# Update time-like parameter in u_d function
	def time_step_update(self, t):
		self.u_d.t = t

	# Return corresponding bilinear form for displacements
	def get_u_form(self, u, d):
		u_tr = fe.TrialFunction(self.V)
		u_test = fe.TestFunction(self.V)
		if self.u_type == "el":
			return (1-d)**2*fe.inner(sigma_el(u, self.mat), eps(u_test))*fe.dx + fe.dot(fe.Constant((0.0, 0.0)), u_test)*fe.dx
		elif self.u_type == "vd":
			return (1-d)**2*fe.inner(sigma_p_vd(u, self.mat), eps(u_test))*fe.dx + fe.inner(sigma_n_vd(u, self.mat), eps(u_test))*fe.dx
		else:
			raise Exception("Type of displacement form must be el/vd!")

	def get_stress_tensor(self, u_i, d_i):
		return local_project((1 - d_i)**2*self.get_sigma_active(u_i) + self.get_sigma_pasive(u_i), self.V0)

	# Return active part of stress tensor
	def get_sigma_active(self, u_i):
		if self.dec_type == "el":
			return sigma_el(u_i, self.mat)
		elif self.dec_type == "vd":
			return sigma_p_vd(u_i, self.mat)
		else:
			raise Exception("Type of decomposition must be el/vd!")

	# Return pasive part of stress tensor
	def get_sigma_pasive(self, u_i):
		if self.dec_type == "el":
			return 0
		elif self.dec_type == "vd":
			return sigma_n_vd(u_i, self.mat)
		else:
			raise Exception("Type of decomposition must be el/vd!")

	# Return bilinear formulation for damage
	def get_d_form(self, u, d):
		d_tr = fe.TrialFunction(self.W)
		d_test = fe.TestFunction(self.W)
		gc = self.mat.Gc
		lc = self.mat.lc
		ft = self.mat.ft
		if self.d_type == "pham":
			E_ds = -fe.inner(self.get_sigma_active(u), eps(u))*fe.inner(1.0 - d, d_test)*fe.dx
			E_ds += 3.0/8.0*gc*(1.0/lc*d_test + 2*lc*fe.inner(fe.grad(d), fe.grad(d_test)))*fe.dx
		elif self.d_type == "bourdin":
			E_ds = -fe.inner(self.get_sigma_active(u), eps(u))*fe.inner(1.0 - d, d_test)*fe.dx
			E_ds += gc*(1.0/lc*fe.inner(d, d_test) + lc*fe.inner(fe.grad(d), fe.grad(d_test)))*fe.dx
		elif self.d_type == "stress":
			stress = sigma_el(u, self.mat)
			sg00, sg01, sg10, sg11 = eig_v(stress)
			D = mc_bracket((mc_bracket(sg00) /ft)**2 + (mc_bracket(sg11)/ft)**2 - 1)
			E_ds = -D*fe.inner(1.0 - d, d_test)*fe.dx
			E_ds += (fe.inner(d, d_test) + lc**2*fe.inner(fe.grad(d), fe.grad(d_test))) * fe.dx
		else:
			raise Exception("Damage type must be pham/bourdin/stress!")
		return E_ds


# --------------------------
# Beam damage model
# --------------------------
# TODO: Rozmyslet znovu a poradne smyk
class BeamDamageModel:
	# PSDamageModel Constructor
	def __init__(self, material, cross, mesh, u_type, d_type, num_int):
		self.mat = material
		self.cross = cross
		self.mesh = mesh
		self.V = None
		self.V0 = None
		self.W = None
		self.bc_u = []
		self.bc_d = []
		self.u_d = None
		self.u_type = u_type  # Type of form for displacement calculation
		self.d_type = d_type  # Type of form for damage calculation
		self.num_int = num_int
		self.dh = abs(num_int[1] - num_int[0])

	# Define spaces
	def init_spaces(self):
		# p1 = fe.FiniteElement('P', fe.interval, 2)  # Space of quadratic polynomial functions (to overcome locking)
		p1 = fe.FiniteElement('P', fe.interval, 1)  # Space of linear polynomial functions
		element = fe.MixedElement([p1, p1, p1])  # Element of mixed space
		self.V = fe.FunctionSpace(self.mesh, element)  # Mixed space
		self.V0 = fe.FunctionSpace(self.mesh, 'DG', 0)
		self.W = fe.FunctionSpace(self.mesh, 'P', 1)  # Space of linear polynomial functions

	# Set Dirichlet boundary conditions and parameter function u_d
	def set_bc(self, bc_u, bc_d, u_d):
		self.bc_u = bc_u
		self.bc_d = bc_d
		self.u_d = u_d

	# Update time-like parameter in u_d function
	def time_step_update(self, t):
		self.u_d.t = t

	# Return corresponding bilinear form for displacements using numerical integration
	def get_u_form(self, x, d):
		# Selective integration for shearlock reduction
		dx_shear = fe.dx(metadata={"quadrature_degree": 1})
		u_, w_, phi_ = fe.split(x)
		u_test, w_test, phi_test = fe.TestFunctions(self.V)
		u_form = 0.0
		for i in range(len(self.num_int) - 1):
			gauss = 0.5*(self.num_int[i] + self.num_int[i + 1])
			eps_z = u_.dx(0) + phi_.dx(0)*gauss
			d_eps_z = u_test.dx(0) + phi_test.dx(0)*gauss
			eps_z_p = mc_bracket(eps_z)
			eps_z_n = -mc_bracket(-eps_z)
			u_form += fe.inner((1.0-d)**2*self.mat.E*eps_z_p + self.mat.E*eps_z_n, d_eps_z)*self.dh*fe.dx
		u_form += w_test.dx(0)*self.mat.mu*self.cross.A*(w_.dx(0) + phi_)*dx_shear
		u_form += phi_test*self.mat.mu*self.cross.A*(phi_ + w_.dx(0))*dx_shear
		u_form -= fe.Constant(0.0)*w_test*fe.dx
		return u_form

	# Return bilinear formulation for damage
	def get_d_form(self, u, d):
		d_tr = fe.TrialFunction(self.W)
		d_test = fe.TestFunction(self.W)
		gc = self.mat.Gc
		lc = self.mat.lc
		ft = self.mat.ft
		if self.d_type == "pham":
			print("pham")
			d_form = -2*self.get_energy_active(u, d)*fe.inner(1.0 - d, d_test)*fe.dx
			d_form += self.cross.h*3.0/8.0*gc*(1.0/lc*d_test + 2*lc*fe.inner(fe.grad(d), fe.grad(d_test)))*fe.dx
		elif self.d_type == "bourdin":
			d_form = -2*self.get_energy_active(u, d)*fe.inner(1.0 - d, d_test)*fe.dx
			d_form += self.cross.h*gc*(1.0/lc*fe.inner(d, d_test) + lc*fe.inner(fe.grad(d), fe.grad(d_test)))*fe.dx
		elif self.d_type == "stress":
			u_, w_, phi_ = fe.split(u)
			eps_i_1 = u_.dx(0) + phi_.dx(0)*0.5*self.cross.h
			eps_i_2 = u_.dx(0) - phi_.dx(0)*0.5*self.cross.h
			stress = max_fce(self.mat.E*eps_i_1, self.mat.E*eps_i_2)
			# stress = sigma_el(u, self.mat)
			# sg00, sg01, sg10, sg11 = eig_v(stress)
			D = mc_bracket((mc_bracket(stress)/ft)**2 - 1)
			d_form = -D*fe.inner(1.0 - d, d_test)*fe.dx
			d_form += (fe.inner(d, d_test) + lc**2*fe.inner(fe.grad(d), fe.grad(d_test))) * fe.dx
		else:
			raise Exception("Damage type must be pham/bourdin/stress!")
		return d_form

	def get_energy_active(self, x, d):
		u_, w_, phi_ = fe.split(x)
		en = 0.0
		for i in range(len(self.num_int) - 1):
			gauss = 0.5*(self.num_int[i] + self.num_int[i + 1])
			eps_z = u_.dx(0) + phi_.dx(0)*gauss
			eps_z_p = mc_bracket(eps_z)
			temp = 0.5*self.mat.E*eps_z_p**2*self.dh
			en += temp
		print(en)
		return en

	def init_files(self, files):
		files.init_files(["displ_u.xdmf", "displ_w.xdmf", "displ_phi.xdmf", "damage.xdmf"])

	def save_u(self, files, x, t):
		u_, w_, phi_ = x.split(deepcopy=True)
		files.save_to(u_, 0, t)
		files.save_to(w_, 1, t)
		files.save_to(phi_, 2, t)

	def save_d(self, files, d, t):
		files.save_to(d, 3, t)

	def get_stress(self, m_x, m_y, u_i, d_i):
		u_, w_, phi_ = u_i.split(deepcopy=True)
		eps_i = u_.dx(0) - phi_.dx(0)*0.5*self.cross.h
		stress = local_project((1.0 - d_i)**2*self.mat.E*mc_bracket(eps_i) - self.mat.E*mc_bracket(-eps_i), self.V0)
		return stress(m_x)
