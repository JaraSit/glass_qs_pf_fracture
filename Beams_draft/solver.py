import fenics as fe
import numpy as np
import matplotlib.pyplot as plt


# --------------------------
# Staggered scheme class
# --------------------------
class StaggeredScheme:
	def __init__(self, formulation, time, files):
		self.max_iter = 50
		self.tol = 1.0e-5
		self.form = formulation
		self.time_set = time
		self.u = fe.Function(self.form.V)
		self.d = fe.Function(self.form.W)
		self.du = fe.Function(self.form.V)
		self.dd = fe.Function(self.form.W)
		self.u_old = fe.Function(self.form.V)
		self.d_old = fe.Function(self.form.W)
		self.d_min = fe.Function(self.form.W)
		self.u_form = None
		self.d_form = None
		self.files = files

	def set_monitor(self, mon):
		self.mon = mon

	def init_forms(self):
		self.u_form = self.form.get_u_form(self.u, self.d)
		self.d_form = self.form.get_d_form(self.u, self.d)

	def solve_displacement(self):
		self.u.vector()[:] = np.random.random(self.u.vector().size())
		# self.form.update_u_form(self.u, self.d)
		a_form = self.u_form
		fe.solve(a_form == 0, self.u, self.form.bc_u)

	def solve_damage(self):
		lower = self.d_min
		upper = fe.interpolate(fe.Constant(1.00), self.form.W)

		# Solution of damage formulation
		H = fe.derivative(self.d_form, self.d, fe.TrialFunction(self.form.W))

		snes_solver_parameters = {"nonlinear_solver": "snes",
								  "snes_solver": {"linear_solver": "lu",
												  # "relative_tolerance": 1.0e-4,
												  # "absolute_tolerance": 1.0e-2,
												  "maximum_iterations": 20,
												  "report": True,
												  "error_on_nonconvergence": False,
												  "line_search": "basic"}}

		# prm['snes_solver']['line_search'] = 'basic'

		problem = fe.NonlinearVariationalProblem(self.d_form, self.d, self.form.bc_d, H)
		problem.set_bounds(lower, upper)

		solver = fe.NonlinearVariationalSolver(problem)
		solver.parameters.update(snes_solver_parameters)
		solver.solve()

	def solve(self):
		# Initialization of bilinear forms
		self.init_forms()
		self.form.init_files(self.files)

		# Time loop
		for i in range(0, len(self.time_set)):
			t = self.time_set[i]

			# Informative print of time instant
			print("Time instant: ", t)
			self.form.time_step_update(t)

			ite = 1
			err = 10.0

			# Staggered loop
			while err > self.tol:

				self.solve_displacement()

				self.solve_damage()

				self.du.assign(self.u - self.u_old)
				self.dd.assign(self.d - self.d_old)

				err_u = fe.norm(self.du)/fe.norm(self.u)
				err_d = fe.norm(self.dd)
				err = max(err_u, err_d)

				print("iter", ite, "errors", err_u, err_d)

				self.u_old.assign(self.u)
				self.d_old.assign(self.d)

				ite += 1

				if ite > self.max_iter:
					print("max iterations reached")
					break

			self.d_min.assign(self.d)
			self.form.save_u(self.files, self.u, t)
			self.form.save_d(self.files, self.d, t)

			if self.mon is not None:
				# stress_tens = self.form.get_stress_tensor(self.u, self.d)
				# self.mon.append_data(self.form.u_d, stress_tens)
				self.mon.append_data(self.form, self.u, self.d)

		self.files.close()
