import fenics as fe
import formulations as f
import matplotlib.pyplot as plt
import numpy as np
import solvers.solver as s


# --------------------
# Functions and classes
# --------------------
def left_point(x):
	return fe.near(x[0], l_off) and fe.near(x[1], 0.0)


def right_point(x):
	return fe.near(x[0], l_x - l_off) and fe.near(x[1], 0.0)


def left_load_point(x):
	return fe.near(x[0], l_x / 2 - l_0 / 2) and fe.near(x[1], l_y)


def right_load_point(x):
	return fe.near(x[0], l_x / 2 + l_0 / 2) and fe.near(x[1], l_y)


def symmetry_axis(x, on_boundary):
	return fe.near(x[0], l_x/2) and on_boundary


def init_bc(model):
	model.init_spaces(degr)
	V = model.V

	u_d = fe.Expression("-t/1000.0", t=0.0, degree=0)  # Prescribed displacement
	BC_u_1 = fe.DirichletBC(V.sub(1), 0.0, left_point, method="pointwise")
	BC_u_2 = fe.DirichletBC(V.sub(1), u_d, left_load_point, method="pointwise")
	BC_u_3 = fe.DirichletBC(V.sub(0), 0.0, symmetry_axis)
	BC_u = [BC_u_1, BC_u_2, BC_u_3]
	BC_d = []

	model.set_bc(BC_u, BC_d, u_d)


def solve(file, model):
	solution = f.PostProcess(file)
	ss = s.StaggeredScheme(model, time_space, solution)
	ss.max_iter = max_iter

	mon1 = f.Monitor(0.5*l_x, 0.0)
	ss.set_monitor(mon1)

	p = fe.Point((0.5*l_x - 0.5*l_0, l_y))
	index = find_dof(p, 1, model.V)
	mon2 = f.MonitorReaction(index)
	ss.set_monitor_react(mon2)

	ss.solve()

	mon1.plot_monitor()
	solution.save_monitor(mon1)
	solution.save_monitor_r(mon2)


def calc_LG():
	model_pham = f.PSDamageModel(LG_mat, mesh_ref, "vd", "pham", "vd")
	init_bc(model_pham)
	solve("Solutions/Solution_LG3_PS_vd_pham_vd", model_pham)


class indicator(fe.UserExpression):
	def __init__(self, subdomain, val1, val2, **kwargs):
		self.subdomains = subdomain
		self.val1 = val1
		self.val2 = val2
		super().__init__(**kwargs)

	def eval_cell(self, values, x, cell):
		if self.subdomains[cell.index] == 1:
			values[0] = self.val1
		else:
			values[0] = self.val2

	def value_shape(self):
		return ()


def find_dof(p, d, V):
	found_dof = -1
	V_dofs = V.tabulate_dof_coordinates()
	V0_dofs = V.sub(d).dofmap().dofs()
	for i in range(0, len(V0_dofs)):
		v_x = V_dofs[V0_dofs[i], 0]
		v_y = V_dofs[V0_dofs[i], 1]
		if fe.near(v_x, p.x()) and fe.near(v_y, p.y()):
			found_dof = V0_dofs[i]
	print("Found_dof = ", found_dof)
	return found_dof


# Material parameters
E_glass = 70.0e9  # Young's modulus
nu_glass = 0.22  # Poisson ratio
G_EVA = 1.0e3*np.array([6933.9, 3898.6, 2289.2, 1672.7, 761.6, 2401.0, 65.2, 248.0, 575.6, 56.3, 188.6, 445.1, 300.1, 401.6, 348.1, 111.6, 127.2, 137.8, 50.5, 322.9, 100.0, 199.9])
G_PVB = 1.0e3*np.array([1782124.2, 519208.7, 546176.8, 216893.2, 13618.3, 4988.3, 1663.8, 587.2, 258.0, 63.8, 168.4])
theta_EVA = np.array([1.0e-9, 1.0e-8, 1.0e-7, 1.0e-6, 1.0e-5, 1.0e-4, 1.0e-3, 1.0e-2, 1.0e-1, 1.0, 1.0e1, 1.0e2, 1.0e3, 1.0e4, 1.0e5, 1.0e6, 1.0e7, 1.0e8, 1.0e9, 1.0e10, 1.0e11, 1.0e12])
theta_PVB = np.array([1.0e-5, 1.0e-4, 1.0e-3, 1.0e-2, 1.0e-1, 1.0, 1.0e1, 1.0e2, 1.0e3, 1.0e4, 1.0e5])
nu_eva = 0.49
nu_pvb = 0.49
G_inf_eva = 682.18e3
G_inf_pvb = 232.26e3
c1_eva = 339.102
c1_pvb = 8.635
c2_eva = 1185.816
c2_pvb = 42.422
T_ref = 20.0
T_act = 25.0

lc_ref = 0.001
ft_glass = 54.0e6

Gc_p_ref = lc_ref * 8.0 / 3.0 * ft_glass ** 2 / E_glass
glass_mat = f.ElasticMaterial(E_glass, nu_glass, Gc_p_ref, lc_ref, ft_glass)
EVA_foil_mat = f.ViscoElasticMaterial(G_EVA, theta_EVA, G_inf_eva, nu_eva, c1_eva, c2_eva, T_ref, T_act)
PVB_foil_mat = f.ViscoElasticMaterial(G_PVB, theta_PVB, G_inf_pvb, nu_pvb, c1_pvb, c2_pvb, T_ref, T_act)

H1, H2, H3 = 0.00996, 0.00076, 0.00996
H2_pvb = 0.00078

LG_mat_EVA = f.LG3LMaterial(glass_mat, EVA_foil_mat, [H1, H2, H3])
LG_mat_PVB = f.LG3LMaterial(glass_mat, PVB_foil_mat, [H1, H2_pvb, H3])

# Geometry parameters
l_x, l_y = 1.1, H1+H2+H3  # Length and thickness of glass beam
n_x, n_y = 275, 10  # Number of elements for uniform mesh
l_off = 0.05  # Support offset
l_0 = 0.2  # Pitch of load points

#E = fe.Expression("abs(x[1]-H1-0.5*H2) < 0.5*H2 ? E_eva : E_glass", H1=H1, H2=H2, E_glass=E_glass, E_eva=E_eva, degree=0)
#nu = fe.Expression("abs(x[1]-H1-0.5*H2) < 0.5*H2 ? nu_eva : nu_glass", H1=H1, H2=H2, nu_glass=nu_glass, nu_eva=nu_eva, degree=0)

degr = 1

# Numerical parameters
max_iter = 50

# Time parameters
t_start = 0.1
t_middle = 9.1
t_middle_2 = 10.0
t_end = 10.2
dt_1 = 0.5
dt_2 = 0.05
dt_3 = 0.01
t_steps_1 = int((t_middle-t_start)/dt_1)
t_steps_2 = int((t_middle_2-t_middle)/dt_2)
t_steps_3 = int((t_end-t_middle_2)/dt_3)

time_space_1 = np.linspace(t_start, t_middle, t_steps_1)
time_space_2 = np.linspace(t_middle + dt_1, t_middle_2, t_steps_2)
time_space_3 = np.linspace(t_middle_2 + dt_2, t_end, t_steps_3)

time_space = np.concatenate((time_space_1, time_space_2, time_space_3))


# --------------------
# Define geometry
# --------------------
mesh_ref = fe.Mesh("Meshes/QS_4PB_PS_gmsh_ref_3.xml")
hmin = mesh_ref.hmin()
print(hmin)

fe.plot(mesh_ref)
plt.show()

calc_LG()
