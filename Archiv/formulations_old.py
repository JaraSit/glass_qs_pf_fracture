import fenics as fe
import matplotlib.pyplot as plt
import numpy as np


# --------------------------
# Auxiliary functions
# --------------------------
# Macauley bracket
def mc_bracket(v):
	return 0.5 * (v + abs(v))


# Projection on each element
def local_project(fce, space):
	lp_trial, lp_test = fe.TrialFunction(space), fe.TestFunction(space)
	lp_a = fe.inner(lp_trial, lp_test)*fe.dx
	lp_L = fe.inner(fce, lp_test)*fe.dx
	local_solver = fe.LocalSolver(lp_a, lp_L)
	local_solver.factorize()
	lp_f = fe.Function(space)
	local_solver.solve_local_rhs(lp_f)
	return lp_f


# Symmetry gradient of displacements
def eps(v):
	return fe.sym(fe.grad(v))


# Elastic stress tensor
def sigma_el(u_i, mat_i):
	return mat_i.lmbda*(fe.tr(eps(u_i)))*fe.Identity(2) + 2.0*mat_i.mu*(eps(u_i))


# Positive stress tensor - volumetric-deviatoric
def sigma_p_vd(u_i, mat_i):
	return mat_i.kn*mc_bracket(fe.tr(eps(u_i)))*fe.Identity(2) + 2*mat_i.mu*fe.dev(eps(u_i))


# Negative stress tensor - volumetric-deviatoric
def sigma_n_vd(u_i, mat_i):
	return -mat_i.kn*mc_bracket(-fe.tr(eps(u_i)))*fe.Identity(2)


# eigenvalues for 2x2 matrix
def eig_v(a):
	v00 = a[0, 0]/2 + a[1, 1]/2 - fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2
	v01 = 0.0
	v10 = 0.0
	v11 = a[0, 0]/2 + a[1, 1]/2 + fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2
	return v00, v01, v10, v11


# eigenvectors for 2x2 matrix
def eig_w(a):
	w00 = -a[0, 1]/(a[0, 0]/2 - a[1, 1]/2 + fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2)
	w01 = -a[0, 1]/(a[0, 0]/2 - a[1, 1]/2 - fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2)
	w10 = 1.0
	w11 = 1.0
	return w00, w01, w10, w11


# --------------------------
# Elastic material class
# --------------------------
class ElasticMaterial:
	def __init__(self, E, nu, Gc, lc, ft):
		self.E = E  # Young's modulus
		self.nu = nu  # Poisson ration
		self.Gc = Gc  # Fracture toughness
		self.lc = lc  # Localization band width
		self.ft = ft  # Fracture strength
		self.lmbda = E*nu/(1 + nu)/(1 - 2*nu)  # Lame coefficients
		self.mu = E / 2 / (1 + nu)  # Lame coefficients
		self.lmbda = 2*self.mu*self.lmbda/(self.lmbda + 2*self.mu)  # Correction for plane stress
		self.kn = self.lmbda + self.mu  # Bulk modulus


# --------------------------
# Class for post-processing
# --------------------------
class PostProcess:
	def __init__(self, folder):
		self.folder = folder

	def init_files(self):
		self.file_u = fe.XDMFFile(self.folder + "/displacements.xdmf")  # XDMF file for displacements
		self.file_d = fe.XDMFFile(self.folder + "/damage.xdmf")  # XDMF file for damage variable

	def save_u_d_xdmf(self, u, d, t):
		self.file_u.write(u, t)
		self.file_d.write(d, t)

	def close(self):
		self.file_u.close()
		self.file_d.close()

	def save_monitor(self, mon):
		np.savetxt(self.folder + "/stress_data.txt", np.column_stack((mon.mon_u_d, mon.mon_stress)))


# --------------------------
# Auxiliary structure for monitoring
# --------------------------
class Monitor:
	def __init__(self, m_x, m_y):
		self.m_x = m_x
		self.m_y = m_y
		self.mon_u_d = []
		self.mon_stress = []

	def append_data(self, u_d_i, stress_i):
		self.mon_u_d.append(u_d_i(self.m_x, self.m_y))
		self.mon_stress.append(stress_i(self.m_x, self.m_y)[0])

	def plot_monitor(self):
		xx = np.array(self.mon_u_d)
		yy = np.array(self.mon_stress)
		plt.style.use("classic")
		plt.plot(abs(xx*1.0e3), yy/1.0e6)
		plt.xlabel("Prescribed displacement [mm]")
		plt.ylabel("Stress [MPa]")
		plt.show()


# --------------------------
# Class with damage model
# --------------------------
class PSDamageModel:
	# PSDamageModel Constructor
	def __init__(self, material, mesh, u_type, d_type, dec_type):
		self.mat = material
		self.mesh = mesh
		self.V = None
		self.V0 = None
		self.DS = None
		self.W = None
		self.bc_u = []
		self.bc_d = []
		self.u_d = None
		self.u_type = u_type  # Type of form for displacement calculation
		self.d_type = d_type  # Type of form for damage calculation
		self.dec_type = dec_type  # Type of stress decomposition for damage calculation

	# Define spaces
	def init_spaces(self):
		self.V = fe.VectorFunctionSpace(self.mesh, "CG", 1)  # Function space for displacements
		self.V0 = fe.TensorFunctionSpace(self.mesh, "DG", 0)  # Function space for stress components
		self.DS = fe.FunctionSpace(self.mesh, "DG", 0)  # Function space for history variable
		self.W = fe.FunctionSpace(self.mesh, "CG", 1)  # Function space for damage variable

	# Set Dirichlet boundary conditions and parameter function u_d
	def set_bc(self, bc_u, bc_d, u_d):
		self.bc_u = bc_u
		self.bc_d = bc_d
		self.u_d = u_d

	# Update time-like parameter in u_d function
	def time_step_update(self, t):
		self.u_d.t = t

	# Return corresponding bilinear form for displacements
	def get_u_form(self, u, d):
		u_tr = fe.TrialFunction(self.V)
		u_test = fe.TestFunction(self.V)
		if self.u_type == "el":
			return (1-d)**2*fe.inner(sigma_el(u, self.mat), eps(u_test))*fe.dx + fe.dot(fe.Constant((0.0, 0.0)), u_test)*fe.dx
		elif self.u_type == "vd":
			return (1-d)**2*fe.inner(sigma_p_vd(u, self.mat), eps(u_test))*fe.dx + fe.inner(sigma_n_vd(u, self.mat), eps(u_test))*fe.dx
		else:
			raise Exception("Type of displacement form must be el/vd!")

	# Return active part of stress tensor
	def get_sigma_active(self, u_i):
		if self.dec_type == "el":
			return sigma_el(u_i, self.mat)
		elif self.dec_type == "vd":
			return sigma_p_vd(u_i, self.mat)
		else:
			raise Exception("Type of decomposition must be el/vd!")

	# Return pasive part of stress tensor
	def get_sigma_pasive(self, u_i):
		if self.dec_type == "el":
			return 0
		elif self.dec_type == "vd":
			return sigma_n_vd(u_i, self.mat)
		else:
			raise Exception("Type of decomposition must be el/vd!")

	# Return bilinear formulation for damage
	def get_d_form(self, u, d):
		d_tr = fe.TrialFunction(self.W)
		d_test = fe.TestFunction(self.W)
		gc = self.mat.Gc
		lc = self.mat.lc
		ft = self.mat.ft
		if self.d_type == "pham":
			E_ds = -fe.inner(self.get_sigma_active(u), eps(u))*fe.inner(1.0 - d, d_test)*fe.dx
			E_ds += 3.0/8.0*gc*(1.0/lc*d_test + 2*lc*fe.inner(fe.grad(d), fe.grad(d_test)))*fe.dx
		elif self.d_type == "bourdin":
			E_ds = -fe.inner(self.get_sigma_active(u), eps(u))*fe.inner(1.0 - d, d_test)*fe.dx
			E_ds += gc*(1.0/lc*fe.inner(d, d_test) + lc*fe.inner(fe.grad(d), fe.grad(d_test)))*fe.dx
		elif self.d_type == "stress":
			stress = sigma_el(u, self.mat)
			sg00, sg01, sg10, sg11 = eig_v(stress)
			D = mc_bracket((mc_bracket(sg00) /ft)**2 + (mc_bracket(sg11)/ft)**2 - 1)
			E_ds = -D*fe.inner(1.0 - d, d_test)*fe.dx
			E_ds += (fe.inner(d, d_test) + lc**2*fe.inner(fe.grad(d), fe.grad(d_test))) * fe.dx
		else:
			raise Exception("Damage type must be pham/bourdin/stress!")
		return E_ds


# --------------------------
# Staggered scheme class
# --------------------------
class StaggeredScheme:
	def __init__(self, formulation, time, files):
		self.max_iter = 50
		self.tol = 1.0e-5
		self.form = formulation
		self.time_set = time
		self.u = fe.Function(self.form.V)
		self.d = fe.Function(self.form.W)
		self.du = fe.Function(self.form.V)
		self.dd = fe.Function(self.form.W)
		self.u_old = fe.Function(self.form.V)
		self.d_old = fe.Function(self.form.W)
		self.d_min = fe.Function(self.form.W)
		self.u_form = None
		self.d_form = None
		self.files = files

	def set_monitor(self, mon):
		self.mon = mon

	def init_forms(self):
		self.u_form = self.form.get_u_form(self.u, self.d)
		self.d_form = self.form.get_d_form(self.u, self.d)

	def solve_displacement(self):
		self.u.vector()[:] = np.random.random(self.u.vector().size())
		a_form = self.u_form
		fe.solve(a_form == 0, self.u, self.form.bc_u)

	def solve_damage(self):
		lower = self.d_min
		upper = fe.interpolate(fe.Constant(1.00), self.form.W)

		# Solution of damage formulation
		H = fe.derivative(self.d_form, self.d, fe.TrialFunction(self.form.W))

		snes_solver_parameters = {"nonlinear_solver": "snes",
								  "snes_solver": {"linear_solver": "lu",
												  "relative_tolerance": 1.0e-4,
												  "absolute_tolerance": 1.0e-2,
												  "maximum_iterations": 20,
												  "report": True,
												  "error_on_nonconvergence": False,
												  "line_search": "basic"}}

		# prm['snes_solver']['line_search'] = 'basic'

		problem = fe.NonlinearVariationalProblem(self.d_form, self.d, self.form.bc_d, H)
		problem.set_bounds(lower, upper)

		solver = fe.NonlinearVariationalSolver(problem)
		solver.parameters.update(snes_solver_parameters)
		solver.solve()

	def solve(self):
		# Initialization of bilinear forms
		self.init_forms()
		self.files.init_files()

		# Time loop
		for i in range(0, len(self.time_set)):
			t = self.time_set[i]

			# Informative print of time instant
			print("Time instant: ", t)
			self.form.time_step_update(t)

			ite = 1
			err = 10.0

			# Staggered loop
			while err > self.tol:

				self.solve_displacement()

				self.solve_damage()

				self.du.assign(self.u - self.u_old)
				self.dd.assign(self.d - self.d_old)

				err_u = fe.norm(self.du)/fe.norm(self.u)
				err_d = fe.norm(self.dd)
				err = max(err_u, err_d)

				print("iter", ite, "errors", err_u, err_d)

				self.u_old.assign(self.u)
				self.d_old.assign(self.d)

				ite += 1

				if ite > self.max_iter:
					print("max iterations reached")
					break

			self.d_min.assign(self.d)
			self.files.save_u_d_xdmf(self.u, self.d, t)

			if self.mon is not None:
				stress_tens = local_project((1-self.d)**2*self.form.get_sigma_active(self.u) + self.form.get_sigma_pasive(self.u),self.form.V0)
				self.mon.append_data(self.form.u_d, stress_tens)

		self.files.close()
