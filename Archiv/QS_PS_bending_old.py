import formulations as f
import fenics as fe
import numpy as np
import matplotlib.pyplot as plt


# --------------------
# Functions and classes
# --------------------
def left_point(x):
	return fe.near(x[0], l_off) and fe.near(x[1], 0.0)


def right_point(x):
	return fe.near(x[0], l_x - l_off) and fe.near(x[1], 0.0)


def left_load_point(x):
	return fe.near(x[0], l_x / 2 - l_0 / 2) and fe.near(x[1], l_y)


def right_load_point(x):
	return fe.near(x[0], l_x / 2 + l_0 / 2) and fe.near(x[1], l_y)


def prepare_mesh():
	nx = 110
	ny = 4

	l_sym = 0.5 * l_x
	mesh = fe.RectangleMesh(fe.Point(0., 0.), fe.Point(l_sym, l_y), nx, ny, diagonal="left/right")

	# fe.plot(mesh, "Mesh")
	# plt.show()

	# Refine
	markers = fe.MeshFunction("bool", mesh, 2)
	markers.set_all(False)
	for c in fe.cells(mesh):
		# Mark cells with facet midpoints near x == L/2
		for f in fe.facets(c):
			if fe.near(f.midpoint()[0], l_x/2., 0.5 * (l_0 + 4.0 * l_y)):
				markers[c] = True
	mesh = fe.refine(mesh, markers, redistribute=False)

	# fe.plot(mesh, "Mesh")
	# plt.show()

	# Refine again
	markers = fe.MeshFunction("bool", mesh, 2)
	markers.set_all(False)
	for c in fe.cells(mesh):
		# Mark cells with facet midpoints near x == L/2
		for f in fe.facets(c):
			if fe.near(f.midpoint()[0], l_x / 2., 0.5 * (l_0 + 2.0 * l_y)):
				markers[c] = True
	mesh = fe.refine(mesh, markers, redistribute=False)

	return mesh

# fe.plot(mesh, "Mesh")
# plt.show()


# Material parameters
E = 76.6e9  # Young's modulus
nu = 0.22  # Poisson ratio
lc = 0.005
ft = 60.0e6

Gc_b = lc * 256.0 / 27.0 * ft ** 2 / E
Gc_p = lc * 8.0 / 3.0 * ft ** 2 / E

glass_b = f.ElasticMaterial(E, nu, Gc_b, lc, ft)
glass_p = f.ElasticMaterial(E, nu, Gc_p, lc, ft)

# Geometry parameters
l_x, l_y = 1.1, 0.02  # Length and thickness of glass beam
l_off = 0.05  # Support offset
l_0 = 0.2  # Pitch of load points

# Time parameters
t_start = 0.1
t_end = 10.59
dt = 0.05
t_steps = int((t_end-t_start)/dt)

time_space = np.linspace(t_start, t_end, t_steps)

# --------------------
# Define geometry
# --------------------
mesh = fe.Mesh("Drafts/Meshes/QS_4PB_testing.xml")
# mesh = prepare_mesh()

fe.plot(mesh)
plt.plot()

# Create model with pham/bourdin/stress formulation with material glass_p
# 3rd argument: type of displacement equation
# 4th argument: type of damage model
# 5th argument: type of decomposition for damage calculation
model_pham = f.PSDamageModel(glass_p, mesh, "el", "pham", "vd")

# Init spaces - for BC definition
model_pham.init_spaces()
V = model_pham.get_u_space()

# --------------------
# Boundary conditions
# --------------------
u_d = fe.Expression("-t/1000.0", t=0.0, degree=0)  # Prescribed displacement
BC_u_1 = fe.DirichletBC(V, fe.Constant((0.0, 0.0)), left_point, method="pointwise")
BC_u_2 = fe.DirichletBC(V.sub(1), 0.0, right_point, method="pointwise")
BC_u_3 = fe.DirichletBC(V.sub(1), u_d, left_load_point, method="pointwise")
BC_u_4 = fe.DirichletBC(V.sub(1), u_d, right_load_point, method="pointwise")
BC_u = [BC_u_1, BC_u_2, BC_u_3, BC_u_4]
BC_d = []
model_pham.set_bc(BC_u, BC_d, u_d)

# 1st argument of PostProcess object: Folder for solution
solution = f.PostProcess("Solution_pham_01")
ss = f.StaggeredScheme(model_pham, time_space, solution)

# One-point monitoring of sigma_x
mon1 = f.Monitor(l_x/2, 0.0)
ss.set_monitor(mon1)

# Staggered solver
ss.solve()

# Plotting of monitor data
mon1.plot_monitor()
