import numpy as np
import matplotlib.pyplot as plt
import fenics as fe
import dolfin as d
import mshr


def plot_ps(data, label, ax):
    ax.plot(abs(-data[:, 0])*1.0e3, data[:, 3]/1.0e6, label=label)


def plot_diag_and_damage(file1, file2, file3, title):
    pham_unif_ps = np.loadtxt(file1 + "/stress_data.txt")
    bourdin_unif_ps = np.loadtxt(file2 + "/stress_data.txt")
    stress_unif_ps = np.loadtxt(file3 + "/stress_data.txt")

    fig, axs = plt.subplots(1, 1)

    axs.set_title(title)
    plot_ps(pham_unif_ps, "pham", axs)
    plot_ps(bourdin_unif_ps, "bourdin", axs)
    plot_ps(stress_unif_ps, "stress", axs)
    plt.xlabel("Prescribed displacement [mm]")
    plt.ylabel("Stress [MPa]")
    plt.legend(loc="best")
    plt.show()

plt.style.use("classic")
#fig, axs = plt.subplots(1, 1)
plot_diag_and_damage("Solution_pham_PS_Unif", "Solution_bourdin_PS_Unif", "Solution_stress_PS_Unif", "Plane stress - uniform")
plot_diag_and_damage("Solution_pham_PS_Ref", "Solution_bourdin_PS_Ref", "Solution_stress_PS_Ref", "Plane stress - refined")
plot_diag_and_damage("Solution_pham_beam_Unif", "Solution_bourdin_beam_Unif", "Solution_stress_beam_Unif", "Beam - uniform")
plot_diag_and_damage("Solution_pham_beam_Ref", "Solution_bourdin_beam_Ref", "Solution_stress_beam_Ref", "Plane stress - refined")
#plot_one("Solution_pham_beam_Ref")
#plot_diag_and_damage("Solution_pham_plate_Ref", "Solution_pham_plate_Ref", "Solution_pham_plate_Ref", "Plate - refined")

plt.figure()
pham_unif_beam = np.loadtxt("Solution_pham_beam_Ref/stress_data.txt")
pham_unif_beam_2 = np.loadtxt("Solution_pham_beam_Ref_avrg/stress_data.txt")
plt.plot(-pham_unif_beam[:, 0], pham_unif_beam[:, 3], label="Avrg")
plt.plot(-pham_unif_beam_2[:, 0], pham_unif_beam_2[:, 3], label="Mod")
#axs.plot(-pham_unif_plate[:, 0], pham_unif_plate[:, 3], "x")
plt.legend()
plt.show()

plt.figure()
pham_unif_beam = np.loadtxt("Solution_pham_beam_Ref/stress_data.txt")
pham_unif_beam_2 = np.loadtxt("Solution_pham_PS_Ref/stress_data.txt")
pham_unif_beam_3 = np.loadtxt("Solution_pham_plate_Ref_2/stress_data.txt")
plt.plot(-pham_unif_beam[:, 0], pham_unif_beam[:, 3], label="Beam")
plt.plot(-pham_unif_beam_2[:, 0], pham_unif_beam_2[:, 3], label="PS")
plt.plot(-pham_unif_beam_3[:, 0], pham_unif_beam_3[:, 3], label="Plate")
#axs.plot(-pham_unif_plate[:, 0], pham_unif_plate[:, 3], "x")
plt.legend(loc="best")
plt.show()


# Geometry
# num_lay = 3
L = 0.2
L0 = 0.0  # 0.025
H1, H2, H3 = 0.01, 0.00228, 0.01
H = H1 + H2 + H3
# width w = 1 [m]

# Define mesh and subdomains
L1 = mshr.Rectangle(fe.Point(0.0, 0.0), fe.Point(L, H1))
L2 = mshr.Rectangle(fe.Point(0.0, H1), fe.Point(L, H1 + H2))
L3 = mshr.Rectangle(fe.Point(0.0, H1+H2), fe.Point(L, H1 + H2 + H3))
domain = L1 + L2 + L3
domain.set_subdomain(1, L1)
domain.set_subdomain(2, L2)
domain.set_subdomain(3, L3)
mesh = mshr.generate_mesh(domain, 200)
fe.plot(mesh)
plt.show()